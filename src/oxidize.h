#ifndef OXIDIZE_H
#define OXIDIZE_H
#include <optional>
#include <variant>
#include <cstdint>
#include <tl/expected.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/zip.hpp>
#include <range/v3/view/enumerate.hpp>
#include <range/v3/view/filter.hpp>
#include <range/v3/view/take.hpp>
#include <range/v3/view/drop.hpp>

using namespace ranges::views;

#define let const auto
#define fn auto
using f32 = float;
using f64 = double;
using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;
using usize = std::uintptr_t;
using i8 = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;
using isize = std::intptr_t;

template<typename R, typename E>
using Result = tl::expected<R, E>;

template<typename T>
using Option = std::optional<T>;
#if _MSC_VER
inline constexpr std::nullopt_t None { std::nullopt_t::_Tag{} };
#else
inline constexpr std::nullopt_t None { std::nullopt_t::_Construct::_Token };
#endif

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

#define NO_COPY(clazz) \
    clazz(const clazz& o) = delete; \
    clazz(clazz& o) = delete; \
    clazz& operator=(const clazz& o) = delete; \
    clazz& operator=(clazz& o) = delete

template<typename Fn>
concept DeferFn = requires(Fn f) {
    { f() } -> std::same_as<void>;
};

template<DeferFn Fn>
class Defer{
public:
    inline explicit Defer(Fn&& f) : f{std::move(f)} {};
    inline ~Defer(){
        f();
    };
private:
    Fn f;
};

#define DEFER(expr) Defer deferName##__LINE__{[&](){expr;}}

#define TRY(expr) if (auto&& try_name_##__LINE__{ expr }) (try_name_##__LINE__)

#define GET_MACRO3(_1, _2, _3, NAME, ...) NAME
#define MAP_(ref_type, ident, fun) transform([ref_type](let& (ident)){ return (fun); })
#define MAP_IDENT(ident, fun) MAP_(&, ident, fun)
#define MAP_IT(fun) MAP_IDENT(it, fun)
#define MAP(...) GET_MACRO3(__VA_ARGS__, MAP_, MAP_IDENT, MAP_IT)(__VA_ARGS__)

#define FILTER_(ref_type, ident, fun) filter([ref_type](let& (ident)){ return (fun); })
#define FILTER_IDENT(ident, fun) FILTER_(&, ident, fun)
#define FILTER_IT(fun) FILTER_IDENT(it, fun)
#define FILTER(...) GET_MACRO3(__VA_ARGS__, FILTER_, FILTER_IDENT, FILTER_IT)(__VA_ARGS__)


#endif //OXIDIZE_H
