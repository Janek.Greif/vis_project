//
// Created by janek on 24.02.22.
//

#ifndef VIS_PROJECT_RENDERERINDEX_H
#define VIS_PROJECT_RENDERERINDEX_H

#include <QDebug>
#include "../oxidize.h"

struct RendererIndex {
    RendererIndex();
    RendererIndex(const RendererIndex& other) = default;
    RendererIndex& operator=(const RendererIndex& other) = default;

    bool operator==(const RendererIndex& other) const = default;
    bool operator!=(const RendererIndex& other) const = default;
    inline constexpr explicit operator bool() const noexcept {
        return index <= 6;
    }

    friend QDebug operator<<(QDebug debug, const RendererIndex& rIdx);
private:
    RendererIndex(u8 index, u8 generation);
    u8 index;
    u8 generation;

    friend class DataRendererStore;
    friend struct TransFnIndex;
};

struct TransFnIndex {
    TransFnIndex();
    TransFnIndex(const TransFnIndex& other) = default;
    TransFnIndex& operator=(const TransFnIndex& other) = default;

    bool operator==(const TransFnIndex& other) const = default;
    bool operator!=(const TransFnIndex& other) const = default;

    friend QDebug operator<<(QDebug debug, const TransFnIndex& tIdx);

    /// For all basic (non shared) transfer-functions, the TransFnIndex and the RendererIndex have the same internal value
    /// and thus a converted to each other. Since they are potentially more transfer-functions than renderers,
    /// this method returns None in such a case.
    fn convertToCorrespondingRendererIndex() -> Option<RendererIndex>;

private:
    TransFnIndex(u8 index, u8 generation);
    u8 index;
    u8 generation;

    friend class DataRendererStore;
};


QDebug operator<<(QDebug debug, const RendererIndex& rIdx);
QDebug operator<<(QDebug debug, const TransFnIndex& tIdx);

#endif //VIS_PROJECT_RENDERERINDEX_H
