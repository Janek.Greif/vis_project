//
// Created by janek on 22.02.22.
//

#ifndef VIS_PROJECT_DATARENDERERSTORE_H
#define VIS_PROJECT_DATARENDERERSTORE_H

#include "DataRenderer.h"
#include "RendererIndex.h"
#include "TransferFunction.h"
#include "../oxidize.h"


class DataRendererStore {

    enum Free : u8 {
        F0 = 1 << 0,
        F1 = 1 << 1,
        F2 = 1 << 2,
        F3 = 1 << 3,
        F4 = 1 << 4,
        F5 = 1 << 5,
        ALL = 0b00111111,
        NONE = 0,
    };

public:
    class DataRendererStoreIter {
        friend class DataRendererStore;
    private:
        explicit DataRendererStoreIter(DataRendererStore* store, u8 idx);

    public:
        DataRendererStoreIter& operator++();
        DataRenderer* operator*();
        bool operator==(const DataRendererStoreIter& iter) const;
        bool operator!=(const DataRendererStoreIter& iter) const;

    private:
        DataRendererStore* store;
        u8 idx;
    };

public:
    DataRendererStore(std::shared_ptr<VolumeTexture> volTex, std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<ShaderEnvironment> shaderEnv);

    fn removeRenderer(RendererIndex rIdx) -> bool;
    fn remove(TransFnIndex tIdx) -> bool;
    fn requestNew(VolumeWidget* volWidget) -> Option<RendererIndex>;
    fn requestNewTransFn() -> TransFnIndex;
    fn get(RendererIndex idx) -> DataRenderer*;
    fn get(RendererIndex idx) const -> const DataRenderer*;

    fn getTransFn(TransFnIndex idx) -> TransferFunction*;

    fn getTransFn(TransFnIndex idx) const -> const TransferFunction*;

    fn begin() -> DataRendererStoreIter;
    fn end() -> DataRendererStoreIter;

    fn deleteTextures() -> void;

    fn operator[](RendererIndex idx) -> DataRenderer*;
    fn operator[](RendererIndex idx) const -> const DataRenderer*;

private:
    fn findFree() const -> Option<u8>;
    fn getRendererByIdx(u8) -> DataRenderer*;
    fn getRendererByIdx(u8) const -> const DataRenderer*;
    fn getGenerationByIdx(u8) const -> u8;
    fn getFreeByIdx(u8) const -> u8;

private:
    struct TransFnGenEntry {
        u8 generation;
        bool free;
    };

private:
    std::shared_ptr<VolumeTexture> vol_tex;
    std::shared_ptr<GeometryEnvironment> geo_env;
    std::shared_ptr<ShaderEnvironment> shader_env;

    DataRenderer r0;
    DataRenderer r1;
    DataRenderer r2;
    DataRenderer r3;
    DataRenderer r4;
    DataRenderer r5;
    std::vector<TransferFunction> trans_fns;
    std::vector<TransFnGenEntry> trans_fn_gens;
    u8 g0;
    u8 g1;
    u8 g2;
    u8 g3;
    u8 g4;
    u8 g5;
    u8 free;
};


#endif //VIS_PROJECT_DATARENDERERSTORE_H
