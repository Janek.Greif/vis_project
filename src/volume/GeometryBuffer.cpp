//
// Created by janek on 04.02.22.
//

#include "GeometryBuffer.h"

constinit QVector3D QUAD_VERTEX_DATA[4] {
        QVector3D{-1.0f, -1.0f,  0.0f}, // bottom left corner
        QVector3D{ 1.0f, -1.0f,  0.0f}, // bottom right corner
        QVector3D{ 1.0f,  1.0f,  0.0f}, // top right corner
        QVector3D{-1.0f,  1.0f,  0.0f}, // top left corner
};

constinit u16 QUAD_INDEX_DATA[6] {
        0, 1, 2, // first triangle (bottom left - top left - top right)
        0, 2, 3, // second triangle (bottom left - top right - bottom right)
};

constinit u16 QUAD_LINE_INDEX_DATA[8] {
        0, 1,
        1, 2,
        2, 3,
        3, 0,
};

constinit QVector3D CUBE_VERTEX_DATA[8] {
        // front
        QVector3D{-1.0f, -1.0f,  1.0f},
        QVector3D{ 1.0f, -1.0f,  1.0f},
        QVector3D{ 1.0f,  1.0f,  1.0f},
        QVector3D{-1.0f,  1.0f,  1.0f},

        // back
        QVector3D{-1.0f, -1.0f, -1.0f},
        QVector3D{ 1.0f, -1.0f, -1.0f},
        QVector3D{ 1.0f,  1.0f, -1.0f},
        QVector3D{-1.0f,  1.0f, -1.0f},
};

constinit u16 CUBE_INDEX_DATA[36] {
        // front
        0, 1, 2,
        2, 3, 0,
        // right
        1, 5, 6,
        6, 2, 1,
        // back
        7, 6, 5,
        5, 4, 7,
        // left
        4, 0, 3,
        3, 7, 4,
        // bottom
        4, 5, 1,
        1, 0, 4,
        // top
        3, 2, 6,
        6, 7, 3
};

constinit u16 CUBE_LINE_INDEX_DATA[24] {
        // front
        0, 1,
        1, 2,
        2, 3,
        3, 0,

        // back
        4, 5,
        5, 6,
        6, 7,
        7, 4,

        // connection
        0, 4,
        1, 5,
        2, 6,
        3, 7,
};

fn GeometryBuffer::bind() -> void {
    vertex_buffer->bind();
    index_buffer->bind();
}

fn GeometryBuffer::unbind() -> void {
    vertex_buffer->release();
    index_buffer->release();
}

fn GeometryBuffer::bindScaffold() -> void {
    vertex_buffer->bind();
    line_index_buffer->bind();
}

fn GeometryBuffer::unbindScaffold() -> void {
    vertex_buffer->release();
    line_index_buffer->release();
}

fn GeometryBuffer::draw() -> void {
    glDrawElements(GL_TRIANGLES, static_cast<i32>(indices_count), GL_UNSIGNED_SHORT, nullptr);
}

fn GeometryBuffer::draw(u32 count) -> void {
    if (count == 0) {
        return;
    }
    glDrawElementsInstanced(GL_TRIANGLES, static_cast<i32>(indices_count), GL_UNSIGNED_SHORT, nullptr, static_cast<i32>(count));
}

fn GeometryBuffer::drawScaffold() -> void {
    glDrawElements(GL_LINES, static_cast<i32>(line_indices_count), GL_UNSIGNED_SHORT, nullptr);
}

fn GeometryBuffer::drawScaffold(u32 count) -> void {
    if (count == 0) {
        return;
    }
    glDrawElementsInstanced(GL_LINES, static_cast<i32>(line_indices_count), GL_UNSIGNED_SHORT, nullptr, static_cast<i32>(count));
}

fn GeometryBuffer::create() -> void {
    if (!created) {
        initializeOpenGLFunctions();
        index_buffer->create();
        index_buffer->bind();
        index_buffer->allocate(indices, static_cast<i32>(indices_count * sizeof(u16)));

        line_index_buffer->create();
        line_index_buffer->bind();
        line_index_buffer->allocate(line_indices, static_cast<i32>(line_indices_count * sizeof(u16)));

        vertex_buffer->create();
        vertex_buffer->bind();
        vertex_buffer->allocate(vertices, static_cast<i32>(vertices_count * sizeof(QVector3D)));

        created = true;
    }
}

