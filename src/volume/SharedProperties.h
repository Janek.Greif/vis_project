//
// Created by janek on 25.02.22.
//

#ifndef VIS_PROJECT_SHAREDPROPERTIES_H
#define VIS_PROJECT_SHAREDPROPERTIES_H

#include <vector>
#include <array>

#include "property_link.h"
#include "LinkToken.h"
#include "RendererIndex.h"
#include "property_link_count.h"
#include "../oxidize.h"


class SharedProperties {
    NO_COPY(SharedProperties);

    template<Property Data>
    class PropertyElement {
    public:
        inline PropertyElement(
                std::shared_ptr<Data> data,
                u8 generation,
                std::shared_ptr<std::vector<RendererIndex>> linkedRenderers,
                bool free
        ) :
                data{std::move(data)},
                generation{generation},
                linked_renderers{std::move(linkedRenderers)},
                free{free}
        {}

    private:

        std::shared_ptr<Data> data;
        u8 generation{};
        std::shared_ptr<std::vector<RendererIndex>> linked_renderers;
        bool free{ true };

        friend class SharedProperties;
    };

public:
    SharedProperties();

    fn requestToken(PropertyLinkType linkType) -> LinkToken;
    fn removeToken(LinkToken token) -> bool;
    fn tokenInUse(LinkToken token) -> bool;
    fn getProperty(LinkToken token) -> Option<LinkProperty>;
    fn getLinkedRenderers(LinkToken token) -> Option<std::shared_ptr<std::vector<RendererIndex>>>;
    fn addLink(LinkToken token, RendererIndex index) -> Option<std::pair<LinkProperty, bool>>;
    fn removeLink(LinkToken token, RendererIndex index) -> bool;
    fn isLinked(PropertyLinkType linkType, RendererIndex index) -> bool;
    fn removeAllLinks(RendererIndex idx) -> void;

private:
    template<Property P>
    fn getCheckedElement(std::vector<PropertyElement<P>>& properties, LinkToken token) -> PropertyElement<P>*;

    template<Property P>
    fn requestLinkToken(std::vector<PropertyElement<P>>& properties) -> LinkToken;

    template<Property P>
    fn removeLinkToken(std::vector<PropertyElement<P>>& properties, LinkToken token) -> bool;

    template<Property P>
    fn addLink(std::vector<PropertyElement<P>>& properties, LinkToken token, RendererIndex rIdx) -> Option<std::pair<LinkProperty, bool>>;

    template<Property P>
    fn removeLink(std::vector<PropertyElement<P>>& properties, LinkToken token, RendererIndex rIdx) -> bool;

    template<Property P>
    fn isLinked(std::vector<PropertyElement<P>>& properties, RendererIndex rIdx) -> bool;

    template<Property P>
    fn removeAllLinks(std::vector<PropertyElement<P>>& properties, RendererIndex rIdx) -> void;

private:
    static_assert(PROPERTY_LINK_COUNT == 5, "include other properties");
    std::vector<PropertyElement<CameraPosition>> camera_position_properties;
    std::vector<PropertyElement<CameraZoom>> camera_zoom_properties;
    std::vector<PropertyElement<TransFn>> trans_fn_properties;
    std::vector<PropertyElement<SlicePositions>> slice_positions_properties;
    std::vector<PropertyElement<Light>> light_properties;
};



#endif //VIS_PROJECT_SHAREDPROPERTIES_H
