//
// Created by janek on 24.02.22.
//

#include "RendererIndex.h"

RendererIndex::RendererIndex(u8 index, u8 generation): index{index}, generation{generation} {

}

RendererIndex::RendererIndex(): index{7}, generation{255} {

}

fn TransFnIndex::convertToCorrespondingRendererIndex() -> Option<RendererIndex> {
    if (index < 6) {
        return RendererIndex{ index, generation };
    } else {
        return None;
    }
}

TransFnIndex::TransFnIndex(): index{255}, generation{255} {

}

TransFnIndex::TransFnIndex(u8 index, u8 generation) : index{index}, generation{generation}{

}

QDebug operator<<(QDebug debug, const RendererIndex& rIdx) {
    QDebugStateSaver saver{debug};
    debug.nospace() << "RendererIndex{" << rIdx.index << ", " << rIdx.generation << "}";

    return debug;
}
QDebug operator<<(QDebug debug, const TransFnIndex& tIdx) {
    QDebugStateSaver saver{debug};
    debug.nospace() << "TransFnIndex{" << tIdx.index << ", " << tIdx.generation << "}";

    return debug;
}
