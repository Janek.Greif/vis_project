//
// Created by janek on 19.03.22.
//

#ifndef VIS_PROJECT_GLTEX_H
#define VIS_PROJECT_GLTEX_H


#include <QOpenGLTexture>

#include "../oxidize.h"

class GLTex : public QOpenGLTexture{
public:
    inline GLTex(QOpenGLTexture::Target target) : QOpenGLTexture{target} {}
    inline GLTex(const QImage& image, QOpenGLTexture::MipMapGeneration genMipMaps) : QOpenGLTexture{image, genMipMaps} {}

    inline explicit operator bool() noexcept {
        return isCreated();
    }


    fn create1DImage(i32 size, QOpenGLTexture::TextureFormat format = QOpenGLTexture::R32F) -> void;
    fn create3DImage(i32 width, i32 height, i32 depth, QOpenGLTexture::TextureFormat format = QOpenGLTexture::R32F) -> void;

    static fn create1DImage(QOpenGLTexture& t, i32 size, QOpenGLTexture::TextureFormat format = QOpenGLTexture::R32F) -> void;
    static fn create3DImage(QOpenGLTexture& t, i32 width, i32 height, i32 depth, QOpenGLTexture::TextureFormat format = QOpenGLTexture::R32F) -> void;
};


#endif //VIS_PROJECT_GLTEX_H
