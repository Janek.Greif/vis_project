//
// Created by janek on 08.03.22.
//

#ifndef VIS_PROJECT_SHADERENVIRONMENT_H
#define VIS_PROJECT_SHADERENVIRONMENT_H

#include <QObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLExtraFunctions>
#include "../oxidize.h"

class ShaderEnvironment : public QObject, protected QOpenGLExtraFunctions{
    Q_OBJECT
public:
    NO_COPY(ShaderEnvironment);
    ShaderEnvironment();

    fn init() -> void;

private:
    fn createShaders() -> void;

#ifdef QT_DEBUG
public slots:
    fn reload() -> void;
#endif

public:
    QOpenGLShaderProgram xray_average;
    QOpenGLShaderProgram xray_average_shaded;
    QOpenGLShaderProgram xray_maximum;
    QOpenGLShaderProgram volume_renderer;
    QOpenGLShaderProgram volume_renderer_shaded;
    QOpenGLShaderProgram slice_renderer;
    QOpenGLShaderProgram slice_renderer_shaded;
    QOpenGLShaderProgram cube;

    QOpenGLShaderProgram low_pass;

    QOpenGLShaderProgram histogram;
    QOpenGLShaderProgram gradient;

    QOpenGLShaderProgram depth_texture;
    QOpenGLShaderProgram line_renderer;
    QOpenGLShaderProgram block;

private:
    bool initialized;
};


#endif //VIS_PROJECT_SHADERENVIRONMENT_H
