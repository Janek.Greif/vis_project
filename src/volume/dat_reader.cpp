//
// Created by janek on 04.02.22.
//
#include <QFile>
#include <QFileDialog>
#include <QSettings>
#include <QtConcurrent/QtConcurrent>
#include "dat_reader.h"

fn read_volume_dat(const std::filesystem::path& name) -> Result<Volume, ReadVolumeDatError> {
    QFile file{name};
    if (!file.open(QIODevice::ReadOnly)){
        return tl::make_unexpected(ReadVolumeDatError::DAT_FILE_NOT_FOUND);
    }

    QDataStream stream{&file};
    stream.setByteOrder(QDataStream::LittleEndian);
    u16 w{};
    u16 h{};
    u16 d{};
    stream >> w >> h >> d;
    let width{ static_cast<i32>(w) };
    let height{ static_cast<i32>(h) };
    let depth{ static_cast<i32>(d) };

    let size{ width * height * depth };
    let byte_size{ size * static_cast<i32>(sizeof(u16)) };
    QList<u16> data{};
    data.resize(size);
    let read_bytes{ stream.readRawData(reinterpret_cast<char*>(data.data()), byte_size) };
    if (read_bytes == -1) {
        return tl::make_unexpected(ReadVolumeDatError::ERROR_READING_FILE);
    }
    if (read_bytes != byte_size) {
        return tl::make_unexpected(ReadVolumeDatError::FILE_TOO_BIG);
    }
    for (auto& voxel : data) {
        voxel *= 16;
    }

    auto iniName{ name };
    iniName.replace_extension("ini");
    let iniNameStr{ QString::fromStdString(iniName.string()) };

    QSettings ini{iniNameStr, QSettings::IniFormat};

    return Volume {
        QString::fromStdString(name.stem().string()),
        std::move(data),
        w,
        h,
        d,
        ini.value("DatFile/oldDat Spacing X", 1.0).toFloat(),
        ini.value("DatFile/oldDat Spacing Y", 1.0).toFloat(),
        ini.value("DatFile/oldDat Spacing Z", 1.0).toFloat()
    };
}

fn read_volume_dat_with_dialog(QFutureWatcher<Result<MoveCell<Volume>, ReadVolumeDatError>>& watcher, QWidget* parent) -> bool {
    let fileName{ QFileDialog::getOpenFileName(parent, "Open Volume File", "", "*.dat") };
    if (fileName.isEmpty()){
        return false;
    }
    auto task{
        QtConcurrent::task([](QString fName){
            auto v{ read_volume_dat(std::filesystem::path{fName.toStdString()}) };
            return v.map([](Volume& r){ return MoveCell{std::move(r)}; });
        }).withArguments(fileName)
        .spawn()
    };
    watcher.setFuture(task);
    return true;
}
