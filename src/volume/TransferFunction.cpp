//
// Created by janek on 08.03.22.
//

#include <range/v3/view/iota.hpp>
#include <QPainter>
#include <QMenu>
#include "TransferFunction.h"

using namespace ranges::views;

static constexpr i32 RADIUS{ 5 };

TransferFunction::TransferFunction(std::shared_ptr<VolumeTexture> volTex, std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<ShaderEnvironment> shaderEnv, i32 width, i32 height) :
    segments{},
    vol_tex{std::move(volTex)},
    shader{std::move(shaderEnv)},
    geo_env{std::move(geoEnv)},
    initialized{false},
    segment_to_add{true},
    width{static_cast<f32>(width)},
    height{static_cast<f32>(height)},
    selected_point{},
    selected_point_for_color{},
    last_selected_segment{},
    last_mouse_pos{},
    trans_fn_tex{std::move(std::make_unique<GLTex>(QOpenGLTexture::Target1D))},
    visible_block_position_tex{ std::make_unique<GLTex>(QOpenGLTexture::Target1D) },
    visible_block_positions{},
    blocks_visible{},
    block_position_update_needed{ true },
    block_visible_update_needed{ true }
{
    static u32 debugIndex{ 0 };
    debug_index = debugIndex++;
}

fn TransferFunction::renderHistogram() -> void {
    glClearColor(1.0f,1.0f,1.0f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    shader->histogram.bind();

    glActiveTexture(GL_TEXTURE0);
    vol_tex->bindHistogram();
    shader->histogram.setUniformValue("histogram", 0);

    let red{ QColor{255, 0, 0} };
    shader->histogram.setUniformValue("color", red);

    geo_env->quad.bind();

    let location{ shader->histogram.attributeLocation("vertexPosition") };
    shader->histogram.enableAttributeArray(location);
    shader->histogram.setAttributeBuffer(location,GL_FLOAT,0,3,sizeof(QVector3D));

    QMatrix4x4 matrix{};
    matrix.setToIdentity();
    matrix.scale((width - static_cast<f32>(RADIUS*2)) / width, (height - static_cast<f32>(RADIUS*2)) / height, 1.0f);
    shader->histogram.setUniformValue("matrix", matrix);

    geo_env->quad.draw();

    geo_env->quad.unbind();
    vol_tex->unbindHistogram();
    shader->histogram.release();
}

fn TransferFunction::init() -> void {
    if (!initialized) {
#ifndef Q_TEST
        initializeOpenGLFunctions();
#endif
        initialized = true;
//        qDebug() << "transfer fn initialized" << debug_index;
    } else {
//        qDebug() << "transfer fn re-initialized" << debug_index;
    }

    trans_fn_tex->create1DImage(TRANS_FN_TEX_SIZE, QOpenGLTexture::RGBA32F);
    segments.clear();
    segments.emplace_back();
    segments.begin()->color = QColor{200, 200, 200};
    segments.begin()->points = QList<IndexedPoint>{{0.0f, 1.0f, 0}, {1.0f, 0.0f, 1}};
    segment_to_add = false;
    mouse_hover = false;
    selected_point = None;
    selected_point_for_color = None;
    last_selected_segment = None;
    last_mouse_pos = QPoint{};

    visible_block_positions.clear();
    blocks_visible.clear();
    block_visible_update_needed = true;
    block_position_update_needed = true;

    volume_update_connection = QObject::connect(&*vol_tex, &VolumeTexture::newVolumeTransFnUpdate, [this](){
        block_visible_update_needed = true;
    });
}

fn TransferFunction::deinit() -> void {
    TRY(trans_fn_tex)->destroy();
    QObject::disconnect(volume_update_connection);
    segments.clear();
    visible_block_positions.clear();
    blocks_visible.clear();
}

fn TransferFunction::renderCurves(QPainter& p) -> void {
    p.setPen(QColor{120, 120, 120});
    p.drawRect(RADIUS, RADIUS, static_cast<i32>(width) - 2*RADIUS, static_cast<i32>(height) - 2*RADIUS);
    for (let& seg : segments) {
        p.setBrush({seg.color});
        p.setPen({seg.color});
        Option<std::pair<i32, i32>> previous{ None };
        for (let vec : seg.points) {
            let x{ static_cast<i32>(vec.x*(width - 2*RADIUS)) + RADIUS };
            let y{ static_cast<i32>(vec.y*(height - 2*RADIUS)) + RADIUS };
            p.drawEllipse(x-RADIUS, y-RADIUS, RADIUS*2, RADIUS*2);
            if (previous) {
                p.drawLine(previous->first, previous->second, x, y);
            }
            previous = std::make_pair(x, y);
        }
    }
    if (mouse_hover) {
        p.setPen(QColor{0, 0, 0, 50});
        p.drawLine(last_mouse_pos.x(), 0, last_mouse_pos.x(), static_cast<i32>(height));
    }
}

fn TransferFunction::mouseMove(QMouseEvent* event) -> bool {
    let p{ event->pos() };
    DEFER(last_mouse_pos = p);
    if ((event->modifiers() & Qt::ControlModifier) && mouse_primary_down) {
        auto& seg{ segments[selected_point->segment] };
        for (auto& point : seg.points) {
            point.x = std::clamp(point.x - static_cast<f32>(last_mouse_pos.x()-p.x()) / width, 0.0f, 1.0f);
            point.y = std::clamp(point.y - static_cast<f32>(last_mouse_pos.y()-p.y()) / height, 0.0f, 1.0f);
        }
    } else if (selected_point) {
        auto& seg{ segments[selected_point->segment] };
        seg.points[selected_point->point].x = std::clamp(static_cast<f32>(p.x() - RADIUS) / (width - 2.0f*RADIUS), 0.0f, 1.0f);
        seg.points[selected_point->point].y = std::clamp(static_cast<f32>(p.y() - RADIUS) / (height - 2.0f*RADIUS), 0.0f, 1.0f);

        sortAndCorrect();
    }
    return true;
}

fn TransferFunction::mousePress(QMouseEvent* event) -> MousePressAction {
    let p{ event->pos() };
    last_mouse_pos = p;
    auto x{ static_cast<f32>(p.x() - RADIUS) / (width - 2*RADIUS) };
    auto y{ static_cast<f32>(p.y() - RADIUS) / (height - 2*RADIUS) };
    selected_point_for_color = None;
    mouse_primary_down = event->button() == Qt::LeftButton;
    if (event->button() == Qt::RightButton) {
        selected_point = None;
        if (let pIdx{ checkPointCollision(QVector2D{p}) }; pIdx) {
            QMenu contextMenu{};
            contextMenu.addAction("change color");
            contextMenu.addAction("remove");
            contextMenu.addAction("remove segment");

            if (let action{ contextMenu.exec(event->globalPosition().toPoint()) }; action) {
                if (action->text() == "change color") {
                    selected_point_for_color = pIdx;
                    last_selected_segment = pIdx;
                    return MousePressAction::OpenColor;
                } else if (action->text() == "remove") {
                    auto& seg{ segments[pIdx->segment] };
                    seg.points.erase(seg.points.begin() + pIdx->point);
                    last_selected_segment = pIdx;
                    u8 actions{ MousePressAction::Redraw };
                    if (seg.points.empty() && removeSegment(*pIdx)) {
                        actions |= MousePressAction::SegmentsEmpty;
                    }
                    return actions;
                } else if (action->text() == "remove segment") {
                    u8 actions{ MousePressAction::Redraw };
                    if (removeSegment(*pIdx)) {
                        actions |= MousePressAction::SegmentsEmpty;
                    }
                    return actions;
                }
            }
        }
        return MousePressAction::NoAction;
    } else if (event->modifiers() & Qt::ControlModifier) {
        return MousePressAction::NoAction;
    } else {
        x = std::clamp(x, 0.0f, 1.0f);
        y = std::clamp(y, 0.0f, 1.0f);

        if (segment_to_add) {
            selected_point_for_color = PointIdx{ static_cast<u8>(segments.size()), 0 };
            last_selected_segment = selected_point_for_color;
            segments.emplace_back();
            auto last{ --segments.end() };
            last->color = QColor{200, 200, 200};
            last->points.push_back({
                x,
                y,
                static_cast<u8>(0)
            });
            segment_to_add = false;
            return MousePressAction::Redraw | MousePressAction::OpenColor | MousePressAction::AddedSegment;
        } else {
            if (let pIdx{ checkPointCollision(QVector2D{p}) }; pIdx) {
                selected_point = pIdx;
                last_selected_segment = pIdx;
            } else {
                let segmentIdx{ last_selected_segment ? last_selected_segment->segment : static_cast<u8>(segments.size()-1) };

                auto last{ segments.begin() + segmentIdx };
                selected_point = PointIdx{segmentIdx, static_cast<u8>(last->points.size())};
                last->points.push_back({
                    x,
                    y,
                    static_cast<u8>(last->points.size())
                });
                sortAndCorrect();
            }
            return MousePressAction::Redraw;
        }
    }
}

fn TransferFunction::mouseRelease(QMouseEvent* event) -> bool {
    selected_point = None;
    mouse_primary_down = false;
    return false;
}

fn TransferFunction::mouseEnter(QEnterEvent* event) -> bool {
    mouse_hover = true;
    return false;
}

fn TransferFunction::mouseLeave(QEvent* event) -> bool {
    mouse_hover = false;
    return true;
}

fn TransferFunction::checkPointCollision(QVector2D mp) -> Option<PointIdx> {
    u8 segmentIdx{ 0 };
    for (let& seg : segments) {
        u8 pointIdx{ 0 };
        for (let vec : seg.points) {
            let x{ vec.x * (width - 2*RADIUS) + RADIUS };
            let y{ vec.y * (height - 2*RADIUS) + RADIUS };
            if (QVector2D{x, y}.distanceToPoint(mp) <= RADIUS) {
                return PointIdx{segmentIdx, pointIdx};
            }
            pointIdx++;
        }
        segmentIdx++;
    }
    return None;
}

fn TransferFunction::colorSelected(const QColor& color) -> bool {
    if (selected_point_for_color) {
        segments[selected_point_for_color->segment].color = color;
        return true;
    } else {
        return false;
    }
}

fn TransferFunction::addSegment() -> void {
    segment_to_add = true;
}

fn TransferFunction::segmentToAdd() -> bool {
    return segment_to_add;
}

fn TransferFunction::sortAndCorrect() -> void {
    if (selected_point) {
        auto& seg{ segments[selected_point->segment] };
        for (auto[i, p] : seg.points | enumerate) {
            p.idx = i;
        }
        std::sort(seg.points.begin(), seg.points.end(), [](const IndexedPoint& left, const IndexedPoint& right){
            return left.x <= right.x;
        });
        for (let i : seg.points | enumerate | FILTER(it.second.idx == selected_point->point) | MAP(it.first)) {
           selected_point->point = i;
           break;
        }
    }
}

fn TransferFunction::removeSegment(PointIdx pIdx) -> bool {
    segments.erase(segments.begin() + pIdx.segment);
    selected_point = None;
    selected_point_for_color = None;
    last_selected_segment = None;
    if (segments.empty()) {
        segment_to_add = true;
        return true;
    } else {
        return false;
    }
}

fn TransferFunction::resize(i32 w, i32 h) -> void {
    width = static_cast<f32>(w);
    height = static_cast<f32>(h);
}

fn TransferFunction::writeToTex(std::span<std::array<f32, 4>, TRANS_FN_TEX_SIZE> buffer) -> void {
    trans_fn_tex->setData(QOpenGLTexture::RGBA, QOpenGLTexture::Float32, buffer.data());
}

fn TransferFunction::bind() -> void {
    trans_fn_tex->bind();
}

fn TransferFunction::bindVBPT(u32 unit) -> void {
    glBindImageTexture(unit, visible_block_position_tex->textureId(), 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA32F);
}

fn TransferFunction::unbindVBPT(u32 unit) -> void {
    glBindImageTexture(unit, 0, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA32F);
}

fn TransferFunction::unbind() -> void {
    trans_fn_tex->release();
}

fn TransferFunction::sample(std::span<std::array<f32, 4>, TRANS_FN_TEX_SIZE> buffer) -> void {
    std::vector<std::array<f32, TRANS_FN_TEX_SIZE>> segmentAlphaPoints{};
    segmentAlphaPoints.resize(segments.size());

    let toIdx{ [](f32 x, bool lowerBound){ return static_cast<i32>(std::ceil(x * TRANS_FN_TEX_SIZE)); } };

    for (auto[segment, sap] : zip(segments, segmentAlphaPoints)) {
        let& seg{ segment.points };
        let& col{ segment.color };
        if (seg.size() >= 2) {
            auto lower{ seg.front().x };
            auto lowerIdx{ 0 };
            for (let i : ints(toIdx(lower, true), toIdx(seg.back().x, false))) {
                let x{ static_cast<f32>(i)/TRANS_FN_TEX_SIZE_F };
                auto upperIdx{ lowerIdx+1 };
                auto upper{ seg[upperIdx].x };
                while (x > upper && upperIdx < seg.size()-1) {
                    lowerIdx++;
                    upperIdx++;
                    lower = seg[lowerIdx].x;
                    upper = seg[upperIdx].x;
                }
                sap[i] = std::lerp(seg[lowerIdx].y, seg[upperIdx].y, (x-lower)/(upper - lower));
            }
        }
    }
    for (auto[i, buf] : buffer | enumerate) {
        f32 r{0.0}, g{0.0}, b{0.0};
        f32 alpha{0.0};
        f32 alphaSum{0.0};

        for (let sapValue : segmentAlphaPoints | MAP(it[i])) {
            alpha = std::max(alpha, sapValue);
            alphaSum += sapValue;
        }
        if (alphaSum != 0.0) {
            for (let[sapValue, segment] : zip(segmentAlphaPoints | MAP(it[i]), segments)) {
                f32 sr{0.0}, sg{0.0}, sb{0.0};
                segment.color.getRgbF(&sr, &sg, &sb);
                r += sr * (sapValue / alphaSum);
                g += sg * (sapValue / alphaSum);
                b += sb * (sapValue / alphaSum);
            }
            alpha = 1.0f - alpha;
        }
        buf = {r, g, b, alpha};
    }
}

fn TransferFunction::cloneInto(TransferFunction& transFn) const -> void {
    // this is a deep copy
    transFn.segments = segments;
    transFn.width = width;
    transFn.height = height;
    transFn.segment_to_add = segment_to_add;

    transFn.visible_block_positions = visible_block_positions;
    transFn.block_visible_update_needed = block_visible_update_needed;
    transFn.block_position_update_needed = block_position_update_needed;
    transFn.blocks_visible = blocks_visible;

    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> buffer{};
    transFn.sample(std::span{ buffer });
    transFn.writeToTex(std::span{ buffer });

    TRY(*transFn.visible_block_position_tex).destroy();
    transFn.visible_block_position_tex->create1DImage(static_cast<i32>(transFn.visible_block_positions.size()), QOpenGLTexture::RGBA32F);
    transFn.visible_block_position_tex->setData(QOpenGLTexture::RGB, QOpenGLTexture::Float32, visible_block_positions.data());
}

fn TransferFunction::deleteTexture() -> void {
    TRY(*visible_block_position_tex).destroy();
}

fn TransferFunction::sample() -> void {
    if (auto& volume{ vol_tex->getVolume() }; volume) {
        std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnBuffer{};
        sample(std::span{ transFnBuffer });
        auto summedAreaTable{ std::move(TransferFunction::buildSumAreaTable(std::span{ transFnBuffer })) };
        classifyTree(std::span{ summedAreaTable });
        writeToTex(std::span{ transFnBuffer });
    }
}


fn TransferFunction::classifyTree(const std::span<f32> sumAreaTable) -> void {
    auto& volumeOpt{ vol_tex->getVolume() };
    assert(volumeOpt);
    auto& volume{ *volumeOpt };

    if (block_visible_update_needed) {
        blocks_visible.resize(volume.blocks.size(), true);
    }

    block_position_update_needed |= block_visible_update_needed;

    auto visibleBlockCount{0};
    for (auto[block, block_visible] : zip(volume.blocks, blocks_visible)) {
        let minIdx{ static_cast<i32>(block.min * static_cast<f32>(sumAreaTable.size()-1)) };
        let maxIdx{ static_cast<i32>(block.max * static_cast<f32>(sumAreaTable.size()-1)) };
        let opacitySum{ sumAreaTable[maxIdx] - sumAreaTable[minIdx] };
        bool wasVisible{ block_visible };
        block_visible = opacitySum != 0.0f;
        if (block_visible) {
            visibleBlockCount++;
        }

        block_position_update_needed |= wasVisible != block_visible;
    }

    if (block_position_update_needed) {
        visible_block_positions.resize(visibleBlockCount);
        for (let[i, blockPositionIndex]
                : blocks_visible
                  | enumerate
                  | FILTER(it.second)
                  | enumerate
                  | MAP(std::make_tuple(it.second.first, static_cast<i64>(it.first)))
        ) {
            let blockZ{ i / (volume.x_block_count * volume.y_block_count) };
            let blockY{(i - blockZ*volume.x_block_count*volume.y_block_count) / volume.x_block_count };
            let blockX{ i - blockZ*volume.x_block_count*volume.y_block_count - blockY*volume.x_block_count };

            let volumeX{ static_cast<f32>(blockX * BLOCK_DIM) };
            let volumeY{ static_cast<f32>(blockY * BLOCK_DIM) };
            let volumeZ{ static_cast<f32>(blockZ * BLOCK_DIM) };

            visible_block_positions[blockPositionIndex] = QVector3D{volumeX, volumeY, volumeZ };
        }

        TRY(*visible_block_position_tex).destroy();
        visible_block_position_tex->create1DImage(visibleBlockCount, QOpenGLTexture::RGBA32F);
        visible_block_position_tex->setData(QOpenGLTexture::RGB, QOpenGLTexture::Float32, visible_block_positions.data());

        block_position_update_needed = false;
        block_visible_update_needed = false;
    }
}

fn TransferFunction::buildSumAreaTable(std::span<std::array<f32, 4>> transferFn) -> std::vector<f32> {
    std::vector<f32> sumAreaTable(static_cast<isize>(transferFn.size()), 0.0f);
    let transferFnOpacity{ transferFn | MAP(it[3]) };
    std::exclusive_scan(
            transferFnOpacity.begin(),  transferFnOpacity.end(),
            sumAreaTable.begin(), 0.0f
    );

    return sumAreaTable;
}

fn TransferFunction::visibleBlockPositions() -> std::span<QVector3D> {
    return std::span{ visible_block_positions };
}

fn TransferFunction::visibleBlockPositions() const -> const std::span<const QVector3D> {
    return std::span{ visible_block_positions };
}
