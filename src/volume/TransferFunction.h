//
// Created by janek on 08.03.22.
//

#ifndef VIS_PROJECT_TRANSFERFUNCTION_H
#define VIS_PROJECT_TRANSFERFUNCTION_H

#include <vector>
#include <span>

#include <QColor>
#include <QOpenGLTexture>
#include <QMouseEvent>

#include "Fbo.h"
#include "ShaderEnvironment.h"
#include "geometry_environment.h"
#include "VolumeTexture.h"

#include "../oxidize.h"

const i32 TRANS_FN_TEX_SIZE = 512;
const f32 TRANS_FN_TEX_SIZE_F = static_cast<f32>(TRANS_FN_TEX_SIZE);

class MousePressAction {
public:
    inline MousePressAction(u8 action) : action{action} {
    }
    enum : u8 {
        NoAction = 0,
        Redraw = 1 << 0,
        OpenColor = 1 << 1,
        AddedSegment = 1 << 2,
        SegmentsEmpty = 1 << 4,
    };

    inline bool operator==(u8 other) const {
        return (action & other) != 0;
    }

private:
    u8 action;
};

class TransferFunction;


class TransferFunction : protected QOpenGLExtraFunctions{
    NO_COPY(TransferFunction);
public:
    TransferFunction(std::shared_ptr<VolumeTexture> volTex, std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<ShaderEnvironment> shaderEnv, i32 width, i32 height);
    TransferFunction(TransferFunction&&) = default;
    TransferFunction& operator=(TransferFunction&&) = default;
    ~TransferFunction() = default;

private:
    struct IndexedPoint {
        f32 x;
        f32 y;
        u8 idx;
        friend class TransferFunction;
        friend class Segment;
    };
public:

    class Segment {

    private:
        QColor color;
        QList<IndexedPoint> points;

        friend class TransferFunction;
        friend class TransferFunctionSamplerTest;
    };

    fn renderHistogram() -> void;
    fn init() -> void;
    fn deinit() -> void;
    fn renderCurves(QPainter& p) -> void;
    fn mouseMove(QMouseEvent* event) -> bool;
    fn mousePress(QMouseEvent* event) -> MousePressAction;
    fn mouseRelease(QMouseEvent* event) -> bool;
    fn mouseEnter(QEnterEvent* event) -> bool;
    fn mouseLeave(QEvent* event) -> bool;
    fn resize(i32 w, i32 h) -> void;
    fn colorSelected(const QColor& color) -> bool;
    fn addSegment() -> void;
    fn segmentToAdd() -> bool;
    fn bind() -> void;
    fn bindVBPT(u32 unit) -> void;

    fn unbind() -> void;
    fn unbindVBPT(u32 unit) -> void;

    fn cloneInto(TransferFunction& transFn) const -> void;
    fn deleteTexture() -> void;

    fn sample() -> void;

    fn visibleBlockPositions() -> std::span<QVector3D>;
    fn visibleBlockPositions() const -> const std::span<const QVector3D>;

private:
    struct PointIdx {
        u8 segment;
        u8 point;
    };

    fn checkPointCollision(QVector2D mp) -> Option<PointIdx>;
    fn sortAndCorrect() -> void;
    fn removeSegment(PointIdx pIdx) -> bool;

    fn classifyTree(std::span<f32> sumAreaTable) -> void;
    fn writeToTex(std::span<std::array<f32, 4>, TRANS_FN_TEX_SIZE> buffer) -> void;
    fn sample(std::span<std::array<f32, 4>, TRANS_FN_TEX_SIZE> buffer) -> void;

    static fn buildSumAreaTable(std::span<std::array<f32, 4>> transferFn) -> std::vector<f32>;

private:
    std::vector<Segment> segments;
    std::shared_ptr<ShaderEnvironment> shader;
    std::shared_ptr<GeometryEnvironment> geo_env;
    std::shared_ptr<VolumeTexture> vol_tex;
    bool initialized;

    Option<PointIdx> selected_point;
    Option<PointIdx> selected_point_for_color;
    Option<PointIdx> last_selected_segment;
    bool segment_to_add;
    bool mouse_hover;
    bool mouse_primary_down;

    f32 width;
    f32 height;

    QPoint last_mouse_pos;

    // unfortunate that QOpenGLTexture cannot be moved, this sucks hard, but it is what it is
    std::unique_ptr<GLTex> trans_fn_tex;
    std::unique_ptr<GLTex> visible_block_position_tex;
    std::vector<QVector3D> visible_block_positions;
    std::vector<bool> blocks_visible;
    bool block_position_update_needed{};
    bool block_visible_update_needed{};

    QMetaObject::Connection volume_update_connection;

    u32 debug_index;

    friend class TransFnColorDialog;
    friend class TransferFunctionSamplerTest;
};


#endif //VIS_PROJECT_TRANSFERFUNCTION_H
