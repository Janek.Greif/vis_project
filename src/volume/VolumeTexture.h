//
// Created by janek on 04.02.22.
//

#ifndef VIS_PROJECT_VOLUMETEXTURE_H
#define VIS_PROJECT_VOLUMETEXTURE_H

#include <memory>
#include <vector>
#include <span>


#include <QObject>
#include <QOpenGLExtraFunctions>
#include <QFutureWatcher>

#include "volume.h"
#include "ShaderEnvironment.h"
#include "GLTex.h"
#include "../util/MoveCell.h"
#include "../oxidize.h"

class VolumeTexture : public QObject, protected QOpenGLExtraFunctions {
    Q_OBJECT
public:
    NO_COPY(VolumeTexture);
    VolumeTexture(VolumeTexture&& volumeTexture) = delete;
    VolumeTexture& operator=(VolumeTexture&& volumeTexture) = delete;

    VolumeTexture(std::shared_ptr<ShaderEnvironment> shaderEnv);

    fn bind() -> void;
    fn unbind() -> void;
    fn bindGradient() -> void;
    fn unbindGradient() -> void;
    fn bindHistogram() -> void;
    fn unbindHistogram() -> void;
    fn getVolume() -> Option<Volume>&;
    fn getVolume() const -> const Option<Volume>&;


signals:
    fn newVolumeTransFnUpdate() -> void;
    fn newVolumeDataRendererUpdate() -> void;
    fn setCursor(Qt::CursorShape cursor) -> void;

public slots:
    fn overrideVolume(MoveCell<Volume> vol) -> void;
    fn deleteTexture() -> void;

private slots:
    fn doComputeShader() -> void;

private:
    fn init() -> void;

private:
    using HistogramFuture = Option<MoveCell<std::pair<std::vector<f32>, Volume>>>;

    std::shared_ptr<ShaderEnvironment> shader_env;
    Option<Volume> volume;
    GLTex tex;
    GLTex gradient;
    GLTex histogram;
    bool initialized;

    QFutureWatcher<HistogramFuture>* histogram_future;
};


#endif //VIS_PROJECT_VOLUMETEXTURE_H
