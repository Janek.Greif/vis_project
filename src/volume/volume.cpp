//
// Created by janek on 04.02.22.
//

#include "volume.h"

Volume::Volume(QString name, QList<u16>&& data, u16 width, u16 height, u16 depth, f32 spacingX, f32 spacingY, f32 spacingZ) :
    name{std::move(name)},
    data{std::move(data)},
    width{width},
    height{height},
    depth{depth},
    spacing_x{spacingX},
    spacing_y{spacingY},
    spacing_z{spacingZ},
    blocks{},
    x_block_count{0},
    y_block_count{0},
    z_block_count{0}
{
}

f32 Volume::operator[](VolumeIdx idx) const {
    let index{ idx.x + idx.y*width + idx.z*width*height };
    return static_cast<f32>(data[index]) / std::numeric_limits<u16>::max();
}

fn Volume::buildTree() -> void {
    blocks.clear();
    x_block_count = static_cast<i32>(std::ceil(static_cast<f32>(width) / static_cast<f32>(BLOCK_DIM)));
    y_block_count = static_cast<i32>(std::ceil(static_cast<f32>(height) / static_cast<f32>(BLOCK_DIM)));
    z_block_count = static_cast<i32>(std::ceil(static_cast<f32>(depth) / static_cast<f32>(BLOCK_DIM)));

    for (let z : ints(0, z_block_count) | MAP(it * BLOCK_DIM)) {
        for (let y : ints(0, y_block_count) | MAP(it * BLOCK_DIM)) {
            for (let x : ints(0, x_block_count) | MAP(it * BLOCK_DIM)) {
                blocks.emplace_back(*this, x, y, z);
            }
        }
    }
}

