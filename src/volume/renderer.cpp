//
// Created by janek on 15.02.22.
//

#include <iostream>

#include <QtMath>
#include <QtImGui.h>
#include <imgui.h>
#include <ImGuizmo.h>
#include "DataRenderer.h"
#include "../ui/VolumeWidget.h"

fn DataRenderer::renderVolume(const VolumeRenderer& vr) -> void {
    renderVolumetric(shader->volume_renderer, &shader->volume_renderer_shaded, [this](QOpenGLShaderProgram& s){
        s.setUniformValue("colorDensity", volume.color_density);
    });
}

fn DataRenderer::renderXRay(const XRayRenderer& xr) -> void {
    if (xray.mode) {
        renderVolumetric(shader->xray_average, &shader->xray_average_shaded, [](auto& _){});
    } else {
        renderVolumetric(shader->xray_maximum, nullptr, [](auto& _){});
    }
}

//fn DataRenderer::renderVolumetric(QOpenGLShaderProgram& volumeShader, QOpenGLShaderProgram* volumeShaderShaded) -> void {
fn DataRenderer::renderVolumetric(QOpenGLShaderProgram& volumeShader, QOpenGLShaderProgram* volumeShaderLight, std::invocable<QOpenGLShaderProgram&> auto fun) -> void {
    if (!renderer_fbo) {
        return;
    }
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (let& volume{ vol_tex->getVolume() }; volume) {

        renderWhiteSpaceSkippingMaps();

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        let useShading{ light->data.status && (volumeShaderLight != nullptr) };
        auto& s{ useShading ? *volumeShaderLight : volumeShader };

        let modelViewProjectionMatrix{ projection() * modelView()};
        let invertedViewProjection{ modelViewProjectionMatrix.inverted() };

        s.bind();
        s.setUniformValue("viewProjection", modelViewProjectionMatrix);
        s.setUniformValue("inversViewProjection", invertedViewProjection);


        let volDimLoc{ s.uniformLocation("volumeDimensions") };
        let volSpacingLoc{ s.uniformLocation("volumeSpacing") };
        QVector3D volumeSize{
                static_cast<f32>(volume->width),
                static_cast<f32>(volume->height),
                static_cast<f32>(volume->depth)
        };
        QVector3D volumeSpacing{volume->spacing_x, volume->spacing_y, volume->spacing_z};
        s.setUniformValue("volumeDimensions", volumeSize);
        s.setUniformValue("volumeSpacing", volumeSpacing);
        s.setUniformValue("quality", general.quality);

        if (useShading) {
            let cameraPos{ camera_position->data.inverted() * QVector4D{0.0, 0.0, 0.0, 1.0}};
            s.setUniformValue("cameraPos", cameraPos.toVector3D() / cameraPos.w());
            s.setUniformValue("lightPos", (light->lightPos * QVector4D{0.0, 0.0, 0.0, 1.0}).toVector3D());
            s.setUniformValue("lightPower", light->data.power);
            s.setUniformValue("ambientF", light->data.ambient);
            s.setUniformValue("diffuseF", light->data.diffuse);
            s.setUniformValue("specularF", light->data.specular);
            s.setUniformValue("shininess", light->data.shininess);
        }

        fun(s);

        glActiveTexture(GL_TEXTURE0);
        vol_tex->bind();
        s.setUniformValue("volumeTexture", 0);


        glActiveTexture(GL_TEXTURE1);
        white_space_skip_start.bindDepth();
        s.setUniformValue("skipWhitespaceStart", 1);

        glActiveTexture(GL_TEXTURE2);
        white_space_skip_end.bindDepth();
        s.setUniformValue("skipWhitespaceEnd", 2);

        glActiveTexture(GL_TEXTURE3);
        vol_wid->transferFunctionByIdx(trans_fn->transfer_fn_idx)->bind();
        s.setUniformValue("transferFunction", 3);

        if (useShading) {
            glActiveTexture(GL_TEXTURE4);
            vol_tex->bindGradient();
            s.setUniformValue("gradientTexture", 4);
        }

        geo_env->quad.bind();

        let location{ s.attributeLocation("vertexPosition") };
        s.enableAttributeArray(location);
        s.setAttributeBuffer(location, GL_FLOAT, 0, 3, sizeof(QVector3D));

        geo_env->quad.draw();

        s.release();

        if (slice_positions->shared) {
            QMatrix4x4 scaleMat{};
            QVector3D scale{
                    1.0f / slice_positions->scale.x(),
                    1.0f / slice_positions->scale.y(),
                    1.0f / slice_positions->scale.z(),
            };
            scaleMat.scale(scale);
            scaleMat.translate(slice_positions->globalCenter);
            renderSlicePolyLines(None, modelViewProjectionMatrix * scaleMat);
            renderBoundingBox({1.0, 1.0, 1.0});
        }
    }
}

fn DataRenderer::renderSlice(const SliceRenderer& sr) -> void {
    if (!renderer_fbo) {
        return;
    }
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (let& volume{ vol_tex->getVolume() }; volume) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        auto& s{ light->data.status ? shader->slice_renderer_shaded : shader->slice_renderer };

        QMatrix4x4 globTrans{};
        globTrans.translate(slice_positions->globalCenter);
        let modelViewProjectionMatrix{projection() * modelView() * globTrans };

        s.bind();
        glActiveTexture(GL_TEXTURE0);
        vol_tex->bind();
        s.setUniformValue("volumeTexture", 0);

        glActiveTexture(GL_TEXTURE3);
        vol_wid->transferFunctionByIdx(trans_fn->transfer_fn_idx)->bind();
        s.setUniformValue("transferFunction", 3);

        if (light->data.status) {
            glActiveTexture(GL_TEXTURE4);
            vol_tex->bindGradient();
            s.setUniformValue("gradientTexture", 4);
        }

        s.setUniformValue("viewProjection", modelViewProjectionMatrix);
        QVector3D volSize{
            static_cast<f32>(volume->width) * volume->spacing_x,
            static_cast<f32>(volume->height) * volume->spacing_y,
            static_cast<f32>(volume->depth) * volume->spacing_z,
        };
        let m{ std::max({volSize.x(), volSize.y(), volSize.z()}) };
        s.setUniformValue("volSize", volSize / m);
        s.setUniformValue("center", slice_positions->center);
        s.setUniformValue("globalCenter", slice_positions->globalCenter);
        s.setUniformValue("scale", slice_positions->scale);
        if (light->data.status) {
            QVector3D volumeSize{
                    static_cast<f32>(volume->width),
                    static_cast<f32>(volume->height),
                    static_cast<f32>(volume->depth)
            };
            QVector3D volumeSpacing{volume->spacing_x, volume->spacing_y, volume->spacing_z};
            let cameraPos{(camera_position->data * camera_zoom->zoom).inverted() * QVector4D{0.0, 0.0, 0.0, 1.0}};

            s.setUniformValue("volumeDimensions", volumeSize);
            s.setUniformValue("volumeSpacing", volumeSpacing);
            s.setUniformValue("cameraPos", cameraPos.toVector3D() / cameraPos.w());
            s.setUniformValue("lightPos", (light->lightPos * QVector4D{0.0, 0.0, 0.0, 1.0}).toVector3D());
            s.setUniformValue("lightPower", light->data.power);
            s.setUniformValue("ambientF", light->data.ambient);
            s.setUniformValue("diffuseF", light->data.diffuse);
            s.setUniformValue("specularF", light->data.specular);
            s.setUniformValue("shininess", light->data.shininess);
        }

        std::array<IntersectionPolygon, 3> polygons{
                IntersectionPolygon{slice_positions->normal1, slice_positions->center},
                IntersectionPolygon{slice_positions->normal2, slice_positions->center},
                IntersectionPolygon{slice_positions->normal3, slice_positions->center},
        };

        std::array<bool, 3> shouldDraw{
            slice_positions->data.show_plane_x,
            slice_positions->data.show_plane_y,
            slice_positions->data.show_plane_z,
        };

        for (auto[slice_vb, poly, sd] : zip(slice.polygon, polygons, shouldDraw)) {
            if (!sd) {
                continue;
            }
            slice_vb.bind();
            slice_vb.write(0, poly.data(), poly.count() * static_cast<i32>(sizeof(QVector3D)));

            let location{ s.attributeLocation("vertexPosition") };
            s.enableAttributeArray(location);
            s.setAttributeBuffer(location, GL_FLOAT, 0, 3, sizeof(QVector3D));
            glDrawArrays(GL_TRIANGLE_FAN, 0, poly.count());
        }

        s.release();

        renderSlicePolyLines(polygons, modelViewProjectionMatrix);
        renderBoundingBox(slice_positions->scale);
    }
}

fn DataRenderer::renderContour(const ContourRenderer& cr) -> void {

}


fn DataRenderer::renderImGuiVolume(const VolumeRenderer& r) -> void {
    renderGuiOverlay([this, r](){
        if (let& vol{ vol_tex->getVolume() }; vol && slice_positions->focus == render_index && slice_positions->shared) {
            renderSliceGuiInteractions(*vol);
        }
        renderLightWidget();
        renderViewManipulation();
    });
}

fn DataRenderer::renderImGuiXRay(const XRayRenderer& r) -> void {
    renderGuiOverlay([this, r](){
        if (let& vol{ vol_tex->getVolume() }; vol && slice_positions->focus == render_index && slice_positions->shared) {
            renderSliceGuiInteractions(*vol);
        }
        renderLightWidget();
        renderViewManipulation();
    });
}

fn DataRenderer::renderImGuiSlice(const SliceRenderer& r) -> void {
    renderGuiOverlay([this, r](){
        renderLightWidget();
        if (let& vol{ vol_tex->getVolume() }; vol && slice_positions->focus == render_index) {
            renderSliceGuiInteractions(*vol);
        }
        renderViewManipulation();
    });
}

fn DataRenderer::renderImGuiContour(const ContourRenderer &r) -> void {

}

fn DataRenderer::initVolume(const VolumeRenderer& vr) -> void {

}

fn DataRenderer::initXRay(const XRayRenderer& xr) -> void {

}

fn DataRenderer::initSlice(const SliceRenderer& sr) -> void {

}

fn DataRenderer::initContour(const ContourRenderer& cr) -> void {

}






fn DataRenderer::reinitVolume(const VolumeRenderer& vr) -> void {

}

fn DataRenderer::reinitXRay(const XRayRenderer& xr) -> void {

}

fn DataRenderer::reinitSlice(const SliceRenderer& sr) -> void {

}

fn DataRenderer::reinitContour(const ContourRenderer& cr) -> void {

}

fn DataRenderer::arcballVector(f32 x, f32 y) const -> QVector3D {
    auto p{ QVector3D(2.0f*x / static_cast<f32>(width)-1.0f, -2.0f*y / static_cast<f32>(height)+1.0f, 0.0) };

    let length2{ p.x()*p.x() + p.y()*p.y() };

    if (length2 < 1.0f) {
        p.setZ(std::sqrt(1.0f - length2));
    } else {
        p.normalize();
    }

    return p;
}

fn DataRenderer::mouseVolume(QMouseEvent* event, const VolumeRenderer& vr) -> Option<PropertyLinkType> {
    return basicMouseInteraction(event);
}

fn DataRenderer::mouseZoom(QWheelEvent* event) -> bool {
    f32 scaleFactor{ 0.01 };

    if(let scrollAmountI{ event->angleDelta().y() }; scrollAmountI){
        let scrollAmount{ static_cast<f32>(scrollAmountI)};
        f32 zoomValue;
        if(scrollAmount > 0.0){
            zoomValue = scrollAmount * scaleFactor;
        }else{
            zoomValue = 1.0f/(-scrollAmount * scaleFactor);
        }

        camera_zoom->zoomValue *= zoomValue;
        camera_zoom->zoom.scale(zoomValue);

        return true;
    }
    return false;
}

fn DataRenderer::mouseXRay(QMouseEvent* event, const XRayRenderer& xr) -> Option<PropertyLinkType> {
    return basicMouseInteraction(event);
}

fn DataRenderer::mouseSlice(QMouseEvent* event, const SliceRenderer& sr) -> Option<PropertyLinkType> {
    return basicMouseInteraction(event);
}

fn DataRenderer::mouseContour(QMouseEvent* event, const ContourRenderer& cr) -> Option<PropertyLinkType> {
    return None;
}

fn DataRenderer::renderWhiteSpaceSkippingMaps() -> void {
    let volW{static_cast<f32>(vol_tex->getVolume()->width)};
    let volH{static_cast<f32>(vol_tex->getVolume()->height)};
    let volD{static_cast<f32>(vol_tex->getVolume()->depth)};
    let spacingW{static_cast<f32>(vol_tex->getVolume()->spacing_x)};
    let spacingH{static_cast<f32>(vol_tex->getVolume()->spacing_y)};
    let spacingD{static_cast<f32>(vol_tex->getVolume()->spacing_z)};
    let sizeW{ volW * spacingW };
    let sizeH{ volH * spacingH };
    let sizeD{ volD * spacingD };

    QMatrix4x4 volumeToUnitCoordinates{};
    let maxScale{ std::max({sizeW, sizeH, sizeD}) };
    volumeToUnitCoordinates.translate(-sizeW / maxScale, -sizeH / maxScale, -sizeD / maxScale);
    volumeToUnitCoordinates.scale(2.0);
    volumeToUnitCoordinates.scale(1.0f / maxScale, 1.0f / maxScale, 1.0f / maxScale);

    let modelViewProjectionMatrix{projection() * modelView() * volumeToUnitCoordinates};
    QVector3D blockSize{BLOCK_DIM_F * spacingW, BLOCK_DIM_F * spacingH, BLOCK_DIM_F * spacingD};

    shader->block.bind();
    shader->block.setUniformValue("modelViewProjectionMatrix", modelViewProjectionMatrix);
    shader->block.setUniformValue("blockSize", blockSize);
    shader->block.setUniformValue("volumeSpacing", QVector3D{ spacingW, spacingH, spacingD });

    geo_env->cube.bind();

    let tFnPtr{ vol_wid->transferFunctionByIdx(trans_fn->transfer_fn_idx) };
    assert(tFnPtr);
    auto& tFn{ *tFnPtr };

    tFn.bindVBPT(0);
    let &countVisibleBlocks{tFn.visibleBlockPositions().size()};

    let location{shader->block.attributeLocation("vertexPosition")};
    shader->block.enableAttributeArray(location);
    shader->block.setAttributeBuffer(location, GL_FLOAT, 0, 3, sizeof(QVector3D));

    glEnable(GL_DEPTH_TEST);

    white_space_skip_start.bind();
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClearDepthf(1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDepthFunc(GL_LESS);

    geo_env->cube.draw(countVisibleBlocks);

    white_space_skip_end.bind();
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClearDepthf(0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDepthFunc(GL_GREATER);

    geo_env->cube.draw(countVisibleBlocks);

    tFn.unbindVBPT(0);

    renderer_fbo.bind();
    glDepthFunc(GL_LESS);
    glClearDepthf(1.0);
}

fn DataRenderer::renderBoundingBox(QVector3D scale) -> void {
    if (let& volume{ vol_tex->getVolume() }; volume) {
        shader->line_renderer.bind();
        geo_env->cube.bindScaffold();

        auto modelViewProjectionMatrix{ projection() * modelView()};
        modelViewProjectionMatrix.scale(scale);
        QVector3D volSize{
                static_cast<f32>(volume->width) * volume->spacing_x,
                static_cast<f32>(volume->height) * volume->spacing_y,
                static_cast<f32>(volume->depth) * volume->spacing_z,
        };
        let m{ std::max({volSize.x(), volSize.y(), volSize.z()}) };
        shader->line_renderer.setUniformValue("viewProjection", modelViewProjectionMatrix);
        shader->line_renderer.setUniformValue("volSize", volSize / m);
        shader->line_renderer.setUniformValue("color", QColor{240, 240, 240});

        let location{ shader->line_renderer.attributeLocation("vertexPosition") };
        shader->line_renderer.enableAttributeArray(location);
        shader->line_renderer.setAttributeBuffer(location, GL_FLOAT, 0, 3, sizeof(QVector3D));

        geo_env->cube.drawScaffold();

        shader->line_renderer.release();
    }
}

fn DataRenderer::renderSlicePolyLines(Option<std::span<const IntersectionPolygon>> polygons, QMatrix4x4 viewProj) -> void {
    if (let& volume{ vol_tex->getVolume() }; volume) {
        shader->line_renderer.bind();

        let modelViewProjectionMatrix{ projection() * modelView()};
        QVector3D volumeSize{
                static_cast<f32>(volume->width),
                static_cast<f32>(volume->height),
                static_cast<f32>(volume->depth)
        };
        QVector3D volumeSpacing{volume->spacing_x, volume->spacing_y, volume->spacing_z};

        let volSize{ volumeSize * volumeSpacing };
        let m{ std::max({volSize.x(), volSize.y(), volSize.z()}) };

        shader->line_renderer.setUniformValue("viewProjection", viewProj);
        shader->line_renderer.setUniformValue("volSize", volSize / m);

        std::array<QColor, 3> colors {
                QColor{255, 0, 0},
                QColor{0, 255, 0},
                QColor{0, 0, 255},
        };

        std::span<const IntersectionPolygon> polys;
        if (polygons) {
            polys = *polygons;
        } else {
            std::array<IntersectionPolygon, 3> p{
                    IntersectionPolygon{slice_positions->normal1, slice_positions->center},
                    IntersectionPolygon{slice_positions->normal2, slice_positions->center},
                    IntersectionPolygon{slice_positions->normal3, slice_positions->center},
            };
            polys = p;
        }

        std::array<bool, 3> shouldDraw{
                slice_positions->data.show_plane_x,
                slice_positions->data.show_plane_y,
                slice_positions->data.show_plane_z
        };

        bool polyUpdateDone{ polygons };

        for (auto[slice_vb, color, poly, sd] : zip(slice.polygon, colors, polys, shouldDraw)) {
            if (!sd) {
                continue;
            }
            shader->line_renderer.setUniformValue("color", color);

            slice_vb.bind();
            if (!polyUpdateDone) {
                slice_vb.write(0, poly.data(), poly.count() * static_cast<i32>(sizeof(QVector3D)));
            }

            let location{ shader->line_renderer.attributeLocation("vertexPosition") };
            shader->line_renderer.enableAttributeArray(location);
            shader->line_renderer.setAttributeBuffer(location, GL_FLOAT, 0, 3, sizeof(QVector3D));

            glDrawArrays(GL_LINE_LOOP, 0, poly.count());
        }
        shader->line_renderer.release();
    }
}

fn DataRenderer::renderViewManipulation() -> void {
    if (ImGuizmo::ViewManipulate(camera_position->data.data(), 2.0f*std::sqrt(3.0f), ImVec2(0, 0), ImVec2(128, 128), 0x10101010, view_manipulate_data)) {
        if (!animating) {
            vol_wid->startAnimation(PropertyLinkType::CAMERA_POS);
            animating = true;
        }
    } else {
        if (animating) {
            vol_wid->stopAnimation(PropertyLinkType::CAMERA_POS);
            animating = false;
        }
    }
}

fn DataRenderer::basicMouseInteraction(QMouseEvent* event) -> Option<PropertyLinkType> {
    if (event->buttons() & Qt::LeftButton) {
        if (current_x != previous_x || current_y != previous_y) {

            let va{ arcballVector(previous_x, previous_y) };
            let vb{ arcballVector(current_x, current_y) };

            if (va != vb) {
                let angle{ std::acos(std::max(-1.0f, std::min(1.0f, QVector3D::dotProduct(va, vb)))) };
                let axis{ QVector3D::crossProduct(va,vb) };

                let inverseModelViewMatrix{ (camera_zoom->panning * camera_position->data * camera_zoom->zoom).inverted() };
                let transformedAxis{ inverseModelViewMatrix * QVector4D{axis, 0.0f} };

                camera_position->data.rotate(qRadiansToDegrees(angle), transformedAxis.toVector3D());
                return PropertyLinkType::CAMERA_POS;
            }
        }
    }

    f32 moveFactor{ 0.0025 };
    if (event->buttons() & Qt::RightButton) {
        if (current_x != previous_x || current_y != previous_y) {
            let xPan{ ( current_x - previous_x) * moveFactor };
            let yPan{ ( previous_y - current_y) * moveFactor };
            camera_zoom->panning.translate(xPan, yPan);
            camera_zoom->panValue += QVector3D{xPan, yPan, 0.0};

            return PropertyLinkType::CAMERA_ZOOM;
        }
    }

    return None;
}

fn DataRenderer::renderSliceGuiInteractions(const Volume& vol) -> void {
    ImGuizmo::SetRect(0, 0, static_cast<f32>(width), static_cast<f32>(height));
    if (slice.showRotate) {
        let v{modelView() * slice_positions->translateMatrix * slice_positions->globalTranslationMatrix };
        if (ImGuizmo::Manipulate(
                v.constData(),
                perspective_matrix.constData(),
                ImGuizmo::ROTATE,
                ImGuizmo::LOCAL,
                slice_positions->rotateMatrix.data()
        )) {
            slice_positions->normal1 = slice_positions->rotateMatrix.mapVector(QVector3D{1.0, 0.0, 0.0});
            slice_positions->normal2 = slice_positions->rotateMatrix.mapVector(QVector3D{0.0, 1.0, 0.0});
            slice_positions->normal3 = slice_positions->rotateMatrix.mapVector(QVector3D{0.0, 0.0, 1.0});
            update2(PropertyLinkType::SLICE_POSITIONS);
        }
    } else if (slice.showTranslate) {
        QVector3D volSize{
                static_cast<f32>(vol.width) * vol.spacing_x,
                static_cast<f32>(vol.height) * vol.spacing_y,
                static_cast<f32>(vol.depth) * vol.spacing_z,
        };
        let m{ std::max({volSize.x(), volSize.y(), volSize.z()}) };
        let v{modelView() * slice_positions->globalTranslationMatrix};
        if (ImGuizmo::Manipulate(
                v.constData(),
                perspective_matrix.constData(),
                ImGuizmo::TRANSLATE,
                ImGuizmo::LOCAL,
                slice_positions->translateMatrix.data()
        )) {
            slice_positions->center = (slice_positions->translateMatrix * QVector4D{0.0, 0.0, 0.0, -1.0}).toVector3D() / (volSize / m);
            update2(PropertyLinkType::SLICE_POSITIONS);
        }
    } else if (slice.showScale) {
        if (ImGuizmo::Manipulate(
                modelView().constData(),
                perspective_matrix.constData(),
                ImGuizmo::SCALE,
                ImGuizmo::LOCAL,
                slice_positions->scaleMatrix.data()
        )) {
            slice_positions->scale = slice_positions->scaleMatrix.mapVector(QVector3D{1.0, 1.0, 1.0});
            update2(PropertyLinkType::SLICE_POSITIONS);
        }
    } else if (slice.showGlobalTranslate) {
        if (ImGuizmo::Manipulate(
                modelView().constData(),
                perspective_matrix.constData(),
                ImGuizmo::TRANSLATE,
                ImGuizmo::LOCAL,
                slice_positions->globalTranslationMatrix.data()
        )) {
            slice_positions->globalCenter = (slice_positions->globalTranslationMatrix * QVector4D{0.0, 0.0, 0.0, 1.0}).toVector3D();
            update2(PropertyLinkType::SLICE_POSITIONS);
        }
    }
}

fn DataRenderer::renderLightWidget() -> void {
    if (light->focus == render_index) {
        ImGuizmo::SetRect(0, 0, static_cast<f32>(width), static_cast<f32>(height));
        if (ImGuizmo::Manipulate(
                modelView().constData(),
                perspective_matrix.constData(),
                ImGuizmo::TRANSLATE,
                ImGuizmo::LOCAL,
                light->lightPos.data()
        )) {
            update2(PropertyLinkType::LIGHT);
        }
    }
}

fn DataRenderer::renderGuiOverlay(std::invocable<> auto fun) -> void {
    let imguiRef{ vol_wid->getImGuiRef() };
    QtImGui::newFrame(imguiRef);
    ImGuizmo::BeginFrame();
    ImGuizmo::Enable(true);

    fun();

    ImGui::Render();
    QtImGui::render(imguiRef);
}

fn DataRenderer::modelView() const -> QMatrix4x4 {
    return camera_zoom->panning * camera_position->data * camera_zoom->zoom;
}

fn DataRenderer::projection() const -> const QMatrix4x4& {
    return general.projection ? perspective_matrix : orthographic_matrix;
}

#ifdef Q_TEST
fn DataRenderer::initEmpty(const EmptyRenderer &r) -> void {

}

fn DataRenderer::renderEmpty(const EmptyRenderer &r) -> void {
    render_update_count++;
}

fn DataRenderer::renderImGuiEmpty(const EmptyRenderer &r) -> void {

}

fn DataRenderer::reinitEmpty(const EmptyRenderer &r) -> void {

}

fn DataRenderer::mouseEmpty(QMouseEvent* event, const EmptyRenderer& cr) -> bool {
    return false;
}
#endif

