//
// Created by janek on 25.02.22.
//

#include "LinkToken.h"
#include "property_link_count.h"

LinkToken::LinkToken(PropertyLinkType type, u8 id, u8 generation) : type{type}, id{id}, generation{generation} {}

LinkToken::LinkToken() : type{PropertyLinkType::CAMERA_POS}, id{255}, generation{0} {

}

std::ostream &operator<<(std::ostream& out, const LinkToken& token) {
    out << "LinkToken(";
    static_assert(PROPERTY_LINK_COUNT == 5, "include other properties");
    switch(token.type) {
        case PropertyLinkType::CAMERA_POS:
            out << "CAMERA_POS";
            break;
        case PropertyLinkType::CAMERA_ZOOM:
            out << "CAMERA_ZOOM";
            break;
        case PropertyLinkType::TRANSFER_FUNCTION:
            out << "TRANSFER_FUNCTION";
            break;
        case PropertyLinkType::SLICE_POSITIONS:
            out << "SLICE_POSITIONS";
            break;
        case PropertyLinkType::LIGHT:
            out << "LIGHT";
            break;
    }
    return out << ", " << std::to_string(token.id) << "," << std::to_string(token.generation) << ")";
}

QDataStream &operator<<(QDataStream& stream, const LinkToken& token) {
    stream << token.id;
    stream << token.generation;
    stream << token.type;
    return stream;
}

QDataStream &operator>>(QDataStream& stream, LinkToken& token) {
    stream >> token.id;
    stream >> token.generation;
    stream >> token.type;
    return stream;
}

QDebug &operator<<(QDebug& stream, const LinkToken& token) {
    QDebugStateSaver saver{stream};
    stream.nospace() << "LinkToken{" << token.type << ", " << token.id << ", " << token.generation << "}";
    return stream;
}
