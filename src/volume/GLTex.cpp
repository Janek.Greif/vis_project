//
// Created by janek on 21.04.22.
//

#include "GLTex.h"

fn GLTex::create1DImage(i32 size, QOpenGLTexture::TextureFormat format) -> void {
    GLTex::create1DImage(*this, size, format);
}

fn GLTex::create3DImage(i32 width, i32 height, i32 depth, QOpenGLTexture::TextureFormat format) -> void {
    GLTex::create3DImage(*this, width, height, depth, format);
}

fn GLTex::create1DImage(QOpenGLTexture& t, i32 size, QOpenGLTexture::TextureFormat format) -> void {
#ifndef Q_TEST
    t.setBorderColor(0.0f, 0.0f, 0.0f, 0.0f);
    t.setWrapMode(QOpenGLTexture::ClampToBorder);
    t.setFormat(format);
    t.setMinificationFilter(QOpenGLTexture::Linear);
    t.setMagnificationFilter(QOpenGLTexture::Linear);
    t.setAutoMipMapGenerationEnabled(false);
    t.setSize(size);
    t.allocateStorage();
#endif
}

fn GLTex::create3DImage(QOpenGLTexture& t, i32 width, i32 height, i32 depth, QOpenGLTexture::TextureFormat format) -> void {
#ifndef Q_TEST
    t.setBorderColor(0.0f, 0.0f, 0.0f, 0.0f);
    t.setWrapMode(QOpenGLTexture::ClampToBorder);
    t.setFormat(format);
    t.setMinificationFilter(QOpenGLTexture::Linear);
    t.setMagnificationFilter(QOpenGLTexture::Linear);
    t.setAutoMipMapGenerationEnabled(false);
    t.setSize(width, height, depth);
    t.allocateStorage();
#endif
}
