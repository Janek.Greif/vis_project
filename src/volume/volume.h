//
// Created by janek on 04.02.22.
//

#ifndef VIS_PROJECT_VOLUME_H
#define VIS_PROJECT_VOLUME_H

#include <QList>

#include "../oxidize.h"

const i32 BLOCK_DIM{ 16 };
const f32 BLOCK_DIM_F{ static_cast<f32>(BLOCK_DIM) };
const i32 BLOCK_SIZE{ BLOCK_DIM*BLOCK_DIM*BLOCK_DIM };

struct Volume;

struct VolumeBlock{
    VolumeBlock(const Volume& volume, i32 xMin, i32 yMin, i32 zMin);

    f32 min;
    f32 max;
};

struct VolumeIdx{
    i32 x;
    i32 y;
    i32 z;
};

struct Volume {
    Volume(QString name, QList<u16>&& data, u16 width, u16 height, u16 depth, f32 spacingX, f32 spacingY, f32 spacingZ);

    NO_COPY(Volume);
    Volume(Volume&& volume) = default;
    Volume& operator=(Volume&& volume) = default;

    f32 operator[](VolumeIdx idx) const;
    fn buildTree() -> void;

    u16 width{0}, height{0}, depth{0};
    f32 spacing_x{1.0}, spacing_y{1.0}, spacing_z{1.0};
    QList<u16> data;

    std::vector<VolumeBlock> blocks;
    i32 x_block_count{}, y_block_count{}, z_block_count{};

    QString name;
};

#endif //VIS_PROJECT_VOLUME_H
