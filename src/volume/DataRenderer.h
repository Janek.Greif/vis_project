//
// Created by janek on 15.02.22.
//

#ifndef VIS_PROJECT_DATARENDERER_H
#define VIS_PROJECT_DATARENDERER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QList>
#include <imgui.h>
#include <ImGuizmo.h>

#include "VolumeTexture.h"
#include "renderer_types.h"
#include "property_link.h"
#include "geometry_environment.h"
#include "Fbo.h"
#include "ShaderEnvironment.h"
#include "../ui/render_settings_event.h"
#include "../util/plane_math.h"
#include "../oxidize.h"

class VolumeWidget;


class DataRenderer : protected QOpenGLExtraFunctions {
public:
    DataRenderer(std::shared_ptr<VolumeTexture> volTex, std::shared_ptr<GeometryEnvironment> genEnv, std::shared_ptr<ShaderEnvironment> shaderEnv);

    fn init(VolumeWidget* volWid, TransFnIndex tIdx) -> void;
    fn detachFromRenderer() -> void;
    fn update(u32 count = 1) -> void;
    fn update2(PropertyLinkType type) -> void;

    fn render() -> void;
    fn reinit(const Renderer& rend) -> void;
    fn initRenderer() -> void;

    fn mouseMove(QMouseEvent* event) -> void;
    fn mouseScroll(QWheelEvent* event) -> void;
    fn mousePress(QMouseEvent* event) -> void;
    fn mouseRelease(QMouseEvent* event) -> void;
    fn keyPress(QKeyEvent* event) -> void;
    fn keyRelease(QKeyEvent* event) -> void;

    fn resize(i32 w, i32 h) -> void;

    fn renderChange(Renderer r) -> void;

    fn markForRedraw() -> void;

    fn needsRedraw() -> bool;
    fn link(LinkProperty property, bool firstLink) -> void;
    fn unlink(PropertyLinkType linkType, bool lastLink) -> void;

    fn getTransferFunctionIndex() -> TransFnIndex;
    fn createSettingsUpdateSignal() -> SettingsUpdateSignal;
    fn qualityUpdate(GeneralData data) -> void;
    fn xRayUpdate(XRayData data) -> void;
    fn volumeUpdate(VolumeData data) -> void;
    fn sliceUpdate(SliceData data) -> void;
    fn lightUpdate(LightData data) -> void;

private: // implemented in renderer.cpp

    [[nodiscard]] fn arcballVector(f32 x, f32 y) const -> QVector3D;

    fn initVolume(const VolumeRenderer& r) -> void;
    fn initXRay(const XRayRenderer& r) -> void;
    fn initSlice(const SliceRenderer& r) -> void;
    fn initContour(const ContourRenderer& r) -> void;

    fn renderVolume(const VolumeRenderer& r) -> void;
    fn renderXRay(const XRayRenderer& r) -> void;
    fn renderSlice(const SliceRenderer& r) -> void;
    fn renderContour(const ContourRenderer& r) -> void;

    fn renderImGuiVolume(const VolumeRenderer& r) -> void;
    fn renderImGuiXRay(const XRayRenderer& r) -> void;
    fn renderImGuiSlice(const SliceRenderer& r) -> void;
    fn renderImGuiContour(const ContourRenderer& r) -> void;

    fn reinitVolume(const VolumeRenderer& r) -> void;
    fn reinitXRay(const XRayRenderer& r) -> void;
    fn reinitSlice(const SliceRenderer& r) -> void;
    fn reinitContour(const ContourRenderer& r) -> void;

    fn mouseVolume(QMouseEvent*, const VolumeRenderer& r) -> Option<PropertyLinkType>;
    fn mouseZoom(QWheelEvent*) -> bool;
    fn mouseXRay(QMouseEvent*, const XRayRenderer& r) -> Option<PropertyLinkType>;
    fn mouseSlice(QMouseEvent*, const SliceRenderer& r) -> Option<PropertyLinkType>;
    fn mouseContour(QMouseEvent*, const ContourRenderer& r) -> Option<PropertyLinkType>;

    fn renderWhiteSpaceSkippingMaps() -> void;
    fn renderBoundingBox(QVector3D scale) -> void;
    fn renderSlicePolyLines(Option<std::span<const IntersectionPolygon>> polygons, QMatrix4x4 viewProj) -> void;
    fn renderGuiOverlay(std::invocable<> auto fun) -> void;
    fn renderViewManipulation() -> void;
    fn basicMouseInteraction(QMouseEvent* event) -> Option<PropertyLinkType>;
    fn renderSliceGuiInteractions(const Volume& vol) -> void;
    /// the second parameter is a pointer, because c++ does not support putting a reference in an Option.
    /// That means nullptr is used as the None variant.
    fn renderVolumetric(QOpenGLShaderProgram& volumeShader, QOpenGLShaderProgram* volumeShaderLight, std::invocable<QOpenGLShaderProgram&> auto fun) -> void;
    fn renderLightWidget() -> void;
    fn modelView() const -> QMatrix4x4;
    fn projection() const -> const QMatrix4x4&;

#ifdef Q_TEST
    fn initEmpty(const EmptyRenderer& r) -> void;
    fn renderEmpty(const EmptyRenderer& r) -> void;
    fn renderImGuiEmpty(const EmptyRenderer& r) -> void;
    fn reinitEmpty(const EmptyRenderer& r) -> void;
    fn mouseEmpty(QMouseEvent*, const EmptyRenderer& r) -> bool;
#endif

private:
    struct Slice {
        bool showRotate;
        bool showTranslate;
        bool showScale;
        bool showGlobalTranslate;
        std::array<QOpenGLBuffer, 3> polygon;
    };

private:
    std::shared_ptr<VolumeTexture> vol_tex;
    Renderer renderer;
    VolumeWidget* vol_wid;
    Fbo renderer_fbo;
    Fbo white_space_skip_start;
    Fbo white_space_skip_end;

    std::shared_ptr<GeometryEnvironment> geo_env;
    std::shared_ptr<ShaderEnvironment> shader;
    QMatrix4x4 perspective_matrix;
    QMatrix4x4 orthographic_matrix;
    f32 current_x, current_y;
    f32 previous_x, previous_y;
    i32 width{}, height{};
    bool mouse_pressed;
    bool initialized;
    bool needs_redraw;
    u32 update_count;

    bool animating;
    u8 render_index;

    Slice slice;
    XRayData xray;
    VolumeData volume;
    GeneralData general;

    CameraPositionProperty camera_position;
    CameraZoomProperty camera_zoom;
    TransFnProperty trans_fn;
    SlicePositionsProperty slice_positions;
    LightProperty light;

    Option<TransFn> backed_up_transfer_fn_tex;

    ImGuizmo::ViewManipulateData view_manipulate_data;

#ifdef Q_TEST
public:
    usize render_update_count{ 0 };
#endif
};


#endif //VIS_PROJECT_DATARENDERER_H
