//
// Created by janek on 05.03.22.
//

#ifndef VIS_PROJECT_GEOMETRYENVIROMENT_H
#define VIS_PROJECT_GEOMETRYENVIROMENT_H

#include "GeometryBuffer.h"
#include "../oxidize.h"

struct GeometryEnvironment {
    NO_COPY(GeometryEnvironment);
    GeometryEnvironment();

    fn create() -> void;

    GeometryBuffer cube;
    GeometryBuffer quad;
};


#endif //VIS_PROJECT_GEOMETRYENVIROMENT_H
