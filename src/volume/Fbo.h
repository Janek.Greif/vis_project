//
// Created by janek on 07.03.22.
//

#ifndef VIS_PROJECT_FBO_H
#define VIS_PROJECT_FBO_H

#include <QOpenGLExtraFunctions>
#include "../oxidize.h"

class Fbo : protected QOpenGLExtraFunctions{
    NO_COPY(Fbo);

public:
    Fbo();
    Fbo(i32 width, i32 height, bool useDepth = true);
    Fbo(Fbo&& other);
    Fbo& operator=(Fbo&& other);
    ~Fbo();

    fn bind() -> void;
    fn unbind() -> void;
    fn bindTex() -> void;
    fn unbindTex() -> void;
    fn bindDepth() -> void;
    fn unbindDepth() -> void;
    fn takeDepth() -> u32;
    fn takeDepth(u32 depthReplacement) -> u32;
    fn takeDepth(i32 newWidth, i32 newHeight) -> u32;
    fn useDepth(u32 depthId) -> void;
    fn useDepth(i32 width, i32 height) -> void;

    inline constexpr explicit operator bool() const noexcept {
        return fbo != 0;
    }

private:
    fn destroy() -> void;

private:
    u32 fbo;
    u32 tex;
    u32 depth;
    bool initialized;
    bool use_depth;
};


#endif //VIS_PROJECT_FBO_H
