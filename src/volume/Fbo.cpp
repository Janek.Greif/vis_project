//
// Created by janek on 07.03.22.
//

#include "Fbo.h"

Fbo::Fbo() : fbo{0}, tex{0}, depth{0}, initialized{false}, use_depth{false} { }

Fbo::Fbo(i32 width, i32 height, bool useDepth) : Fbo() {
    initializeOpenGLFunctions();
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

    if (useDepth) {
        glGenTextures(1, &depth);
        glBindTexture(GL_TEXTURE_2D, depth);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    initialized = true;
    use_depth = useDepth;
}

Fbo::Fbo(Fbo&& other) {
    if (!initialized && other.fbo != 0) {
        initializeOpenGLFunctions();
        initialized = true;
    }
    fbo = other.fbo;
    tex = other.tex;
    depth = other.depth;
    use_depth = other.use_depth;

    other.fbo = 0;
    other.tex = 0;
    other.depth = 0;
    other.use_depth = false;
}

Fbo &Fbo::operator=(Fbo &&other) {
    if (!initialized && other.fbo != 0) {
        initializeOpenGLFunctions();
    }
    destroy();

    fbo = other.fbo;
    tex = other.tex;
    depth = other.depth;
    use_depth = other.use_depth;

    other.fbo = 0;
    other.tex = 0;
    other.depth = 0;
    other.use_depth = false;
    return *this;
}

Fbo::~Fbo() {
    destroy();
}

fn Fbo::destroy() -> void {
    if (fbo != 0) {
        glDeleteTextures(1, &tex);
        if (use_depth) {
            glDeleteTextures(1, &depth);
        }
        glDeleteFramebuffers(1, &fbo);
    }
}

fn Fbo::bind() -> void {
    if (fbo != 0) {
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        if (use_depth) {
            glBindTexture(GL_TEXTURE_2D, depth);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);
        }
        glBindTexture(GL_TEXTURE_2D, tex);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
    } else {
//        qDebug() << "bind uninitialized fbo";
    }
}

fn Fbo::unbind() -> void {
    if (fbo != 0) {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    } else {
//        qDebug() << "unbind uninitialized fbo";
    }
}

fn Fbo::bindTex() -> void {
    if (fbo != 0) {
        glBindTexture(GL_TEXTURE_2D, tex);
    } else {
//        qDebug() << "bind uninitialized fbo-texture";
    }
}

fn Fbo::unbindTex() -> void {
    if (fbo != 0) {
        glBindTexture(GL_TEXTURE_2D, 0);
    } else {
//        qDebug() << "unbind uninitialized fbo-texture";
    }
}

fn Fbo::bindDepth() -> void {
    if (fbo != 0 && use_depth) {
        glBindTexture(GL_TEXTURE_2D, depth);
    }
}

fn Fbo::unbindDepth() -> void {
    if (fbo != 0 && use_depth) {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

fn Fbo::takeDepth() -> u32 {
    if (fbo != 0 && use_depth) {
        use_depth = false;
        let oldDepth{ depth };
        depth = 0;
        return oldDepth;
    }
    return 0;
}
fn Fbo::takeDepth(u32 depthId) -> u32 {
    if (fbo != 0 && use_depth) {
        let oldDepth{ depth };
        depth = depthId;
        return oldDepth;
    }
    return 0;
}
fn Fbo::takeDepth(i32 newWidth, i32 newHeight) -> u32 {
    if (fbo != 0 && use_depth) {
        let oldDepth{ depth };

        glGenTextures(1, &depth);
        glBindTexture(GL_TEXTURE_2D, depth);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, newWidth, newHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        return oldDepth;
    }
    return 0;
}
fn Fbo::useDepth(u32 depthId) -> void {
    if (fbo != 0) {
        if (use_depth) {
            glDeleteTextures(1, &depth);
        }
        depth = depthId;
        use_depth = true;
    }
}

fn Fbo::useDepth(i32 width, i32 height) -> void {
    if (fbo != 0) {
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        if (use_depth) {
            glDeleteTextures(1, &depth);
        }
        glGenTextures(1, &depth);
        glBindTexture(GL_TEXTURE_2D, depth);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        use_depth = true;
    }
}
