//
// Created by janek on 25.02.22.
//

#ifndef VIS_PROJECT_LINKTOKEN_H
#define VIS_PROJECT_LINKTOKEN_H

#include <QDebug>

#include "property_link_count.h"
#include "property_link.h"
#include "../oxidize.h"

class LinkToken {
public:
    LinkToken(const LinkToken& linkToken) = default;
    LinkToken& operator=(const LinkToken& linkToken) = default;
    LinkToken();

    // using space-ship operator results in ICE?????
    bool operator==(const LinkToken& other) const = default;
    bool operator!=(const LinkToken& other) const = default;

    inline constexpr explicit operator bool() const noexcept {
        return id <= PROPERTY_LINK_COUNT;
    }

    friend std::ostream & operator<<(std::ostream &out, const LinkToken& token);

    inline fn linkType() const -> PropertyLinkType {
        return type;
    };

    friend QDataStream& operator<< (QDataStream& stream, const LinkToken& token);
    friend QDataStream& operator>> (QDataStream& stream, LinkToken& token);
    friend QDebug& operator<< (QDebug& stream, const LinkToken& token);

private:
    LinkToken(PropertyLinkType type, u8 id, u8 generation);
    PropertyLinkType type;
    u8 id;
    u8 generation;

    friend class SharedProperties;
};

QDataStream& operator<< (QDataStream& stream, const LinkToken& token);
QDataStream& operator>> (QDataStream& stream, LinkToken& token);
QDebug& operator<< (QDebug& stream, const LinkToken& token);

#endif //VIS_PROJECT_LINKTOKEN_H
