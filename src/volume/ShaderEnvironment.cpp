//
// Created by janek on 08.03.22.
//

#include "ShaderEnvironment.h"

ShaderEnvironment::ShaderEnvironment() : QObject{nullptr}, initialized{false}{

}

fn createShader(QOpenGLShaderProgram& program, const QString& name, const QString& vertex, const QString& fragment) {
    QString file{};
    file.push_back("shader:");
    file.push_back(vertex);
    file.push_back("-vs.glsl");
    if (!program.addShaderFromSourceFile(QOpenGLShader::Vertex, file)) {
        qDebug() << "Could not load" << vertex << "vertex shader for" << name << "!";
    }
    file.clear();
    file.push_back("shader:");
    file.push_back(fragment);
    file.push_back("-fs.glsl");
    if (!program.addShaderFromSourceFile(QOpenGLShader::Fragment, file)) {
        qDebug() << "Could not load" << fragment << "fragment shader for" << name << "!";
    }

    if (!program.link()) {
        qDebug() << "Could not link" << name << "shader!";
    }
}

fn createCompute(QOpenGLShaderProgram& program, const QString& name, const QString& compute) {
    QString file{};
    file.push_back("shader:");
    file.push_back(compute);
    file.push_back("-cs.glsl");
    if (!program.addShaderFromSourceFile(QOpenGLShader::Compute, file)) {
        qDebug() << "Could not load" << name << "compute shader for" << name << "!";
    }
    if (!program.link()) {
        qDebug() << "Could not link" << name << "compute shader!";
    }
}

fn ShaderEnvironment::init() -> void {
    if (!initialized) {
        initializeOpenGLFunctions();
        createShaders();
        initialized = true;
    }
}

fn ShaderEnvironment::createShaders() -> void {
    createShader(xray_average, "xray average", "volume", "xray_average");
    createShader(xray_average_shaded, "xray average shaded", "volume", "xray_average_shaded");
    createShader(xray_maximum, "xray maximum", "volume", "xray_maximum");
    createShader(volume_renderer, "volume renderer", "volume", "volume_renderer");
    createShader(volume_renderer_shaded, "volume renderer shaded", "volume", "volume_renderer_shaded");
    createShader(slice_renderer, "slice renderer", "slice_renderer", "slice_renderer");
    createShader(slice_renderer_shaded, "slice renderer shaded", "slice_renderer", "slice_renderer_shaded");
    createShader(cube, "cube", "cube", "cube");

    createCompute(low_pass, "texture low pass", "low_pass");

    createShader(histogram, "histogram", "matrix_quad", "histogram_quad");
    createCompute(gradient, "gradient", "gradient");

    createShader(depth_texture, "depth_texture", "depth_texture_quad", "depth_texture_quad");
    createShader(line_renderer, "line renderer", "line_renderer", "line_renderer");
    createShader(block, "block test shader", "block", "block");
}

#ifdef QT_DEBUG
fn ShaderEnvironment::reload() -> void {
    xray_average.removeAllShaders();
    xray_average.release();
    xray_average_shaded.removeAllShaders();
    xray_average_shaded.release();
    xray_maximum.removeAllShaders();
    xray_maximum.release();
    volume_renderer.removeAllShaders();
    volume_renderer.release();
    volume_renderer_shaded.removeAllShaders();
    volume_renderer_shaded.release();
    slice_renderer.removeAllShaders();
    slice_renderer.release();
    slice_renderer_shaded.removeAllShaders();
    slice_renderer_shaded.release();
    cube.removeAllShaders();
    cube.release();

    low_pass.removeAllShaders();
    low_pass.release();

    histogram.removeAllShaders();
    histogram.release();
    gradient.removeAllShaders();
    gradient.release();

    depth_texture.removeAllShaders();
    depth_texture.release();
    line_renderer.removeAllShaders();
    line_renderer.release();
    block.removeAllShaders();
    block.release();
    createShaders();
}
#endif