//
// Created by janek on 22.02.22.
//

#include "DataRendererStore.h"

DataRendererStore::DataRendererStore(std::shared_ptr<VolumeTexture> volTex, std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<ShaderEnvironment> shaderEnv) :
    vol_tex{volTex},
    geo_env{geoEnv},
    shader_env{shaderEnv},

    r0{volTex, geoEnv, shaderEnv},
    r1{volTex, geoEnv, shaderEnv},
    r2{volTex, geoEnv, shaderEnv},
    r3{volTex, geoEnv, shaderEnv},
    r4{volTex, geoEnv, shaderEnv},
    r5{volTex, geoEnv, shaderEnv},
    trans_fns{},
    trans_fn_gens{},
    g0{0},
    g1{0},
    g2{0},
    g3{0},
    g4{0},
    g5{0},
    free{ ALL }
{
    trans_fns.emplace_back(volTex, geoEnv, shaderEnv, 10, 10);
    trans_fns.emplace_back(volTex, geoEnv, shaderEnv, 10, 10);
    trans_fns.emplace_back(volTex, geoEnv, shaderEnv, 10, 10);
    trans_fns.emplace_back(volTex, geoEnv, shaderEnv, 10, 10);
    trans_fns.emplace_back(volTex, geoEnv, shaderEnv, 10, 10);
    trans_fns.emplace_back(volTex, geoEnv, shaderEnv, 10, 10);

    trans_fn_gens.push_back({0, true});
    trans_fn_gens.push_back({0, true});
    trans_fn_gens.push_back({0, true});
    trans_fn_gens.push_back({0, true});
    trans_fn_gens.push_back({0, true});
    trans_fn_gens.push_back({0, true});
}


fn DataRendererStore::requestNew(VolumeWidget* volWidget) -> Option<RendererIndex> {
    if (let freeIdx{ findFree() }; freeIdx){
        RendererIndex newIdx { *freeIdx, getGenerationByIdx(*freeIdx) };
        assert(trans_fn_gens[*freeIdx].free);
        assert(trans_fn_gens[*freeIdx].generation == newIdx.generation);
        TransFnIndex newTransFnIdx{ newIdx.index, newIdx.generation };
        free &= ~getFreeByIdx(newIdx.index);
        trans_fn_gens[newTransFnIdx.index].free = false;
        trans_fns[newTransFnIdx.index].init();
        getRendererByIdx(newIdx.index)->init(volWidget, newTransFnIdx);

        return newIdx;
    } else {
        return None;
    }
}

fn DataRendererStore::requestNewTransFn() -> TransFnIndex {
    for (auto[i, gen, freeTFn] : trans_fn_gens
                     | enumerate
                     | drop(6)
                     | FILTER(it.second.free)
                     | MAP(std::make_tuple(static_cast<u8>(it.first), it.second.generation, &it.second.free))
    ) {
        TransFnIndex newIdx{ i, gen };
        trans_fns[i].init();
        *freeTFn = false;
        assert(!trans_fn_gens[i].free);
        return newIdx;
    }
    let i{ static_cast<u8>(trans_fn_gens.size()) };
    trans_fn_gens.push_back({0, false});
    trans_fns.emplace_back(vol_tex, geo_env, shader_env, 10, 10);
    trans_fns.back().init();
    return TransFnIndex{ i, 0 };
}

fn DataRendererStore::get(RendererIndex idx) -> DataRenderer* {
    if (getGenerationByIdx(idx.index) == idx.generation){
        return getRendererByIdx(idx.index);
    }
    return nullptr;
}

fn DataRendererStore::get(RendererIndex idx) const -> const DataRenderer* {
    if (getGenerationByIdx(idx.index) == idx.generation){
        return getRendererByIdx(idx.index);
    }
    return nullptr;
}

fn DataRendererStore::getTransFn(TransFnIndex idx) -> TransferFunction* {
    if (idx.index < trans_fn_gens.size() && !trans_fn_gens[idx.index].free && trans_fn_gens[idx.index].generation == idx.generation){
        return &trans_fns[idx.index];
    }
    return nullptr;
}

fn DataRendererStore::getTransFn(TransFnIndex idx) const -> const TransferFunction* {
    if (idx.index < trans_fn_gens.size() && !trans_fn_gens[idx.index].free && trans_fn_gens[idx.index].generation == idx.generation){
        return &trans_fns[idx.index];
    }
    return nullptr;
}

fn DataRendererStore::operator[](RendererIndex idx) -> DataRenderer* {
    if (getGenerationByIdx(idx.index) == idx.generation){
        return getRendererByIdx(idx.index);
    }
    return nullptr;
}

fn DataRendererStore::operator[](RendererIndex idx) const -> const DataRenderer* {
    if (getGenerationByIdx(idx.index) == idx.generation){
        return getRendererByIdx(idx.index);
    }
    return nullptr;
}

fn DataRendererStore::findFree() const -> Option<u8> {
    if (free & F0) {
        return 0;
    }
    if (free & F1) {
        return 1;
    }
    if (free & F2) {
        return 2;
    }
    if (free & F3) {
        return 3;
    }
    if (free & F4) {
        return 4;
    }
    if (free & F5) {
        return 5;
    }
    return None;
}

fn DataRendererStore::getRendererByIdx(u8 idx) -> DataRenderer* {
    switch(idx) {
        case 0:
            return &r0;
        case 1:
            return &r1;
        case 2:
            return &r2;
        case 3:
            return &r3;
        case 4:
            return &r4;
        case 5:
            return &r5;
        default:
            qFatal("request for DataRenderer with out of bounds index: %d", idx);
    }
}

fn DataRendererStore::getRendererByIdx(u8 idx) const -> const DataRenderer* {
    switch(idx) {
        case 0:
            return &r0;
        case 1:
            return &r1;
        case 2:
            return &r2;
        case 3:
            return &r3;
        case 4:
            return &r4;
        case 5:
            return &r5;
        default:
            qFatal("request for DataRenderer with out of bounds index: %d", idx);
    }
}

fn DataRendererStore::getGenerationByIdx(u8 idx) const -> u8 {
    switch(idx) {
        case 0:
            return g0;
        case 1:
            return g1;
        case 2:
            return g2;
        case 3:
            return g3;
        case 4:
            return g4;
        case 5:
            return g5;
        default:
            qFatal("request for generation with out of bounds index: %d", idx);
    }
}

fn DataRendererStore::getFreeByIdx(u8 idx) const -> u8 {
    switch(idx) {
        case 0:
            return F0;
        case 1:
            return F1;
        case 2:
            return F2;
        case 3:
            return F3;
        case 4:
            return F4;
        case 5:
            return F5;
        default:
            qFatal("request for free with out of bounds index: %d", idx);
    }
}

fn DataRendererStore::remove(TransFnIndex tIdx) -> bool {
    if (tIdx.index < 6) {
        // cannot remove transfer function of a Data Renderer without remove the DataRenderer itself
        return false;
    }
    if (tIdx.index < trans_fn_gens.size() && !trans_fn_gens[tIdx.index].free && trans_fn_gens[tIdx.index].generation == tIdx.generation) {
        // don't deallocate, we may need to reuse it later
        trans_fn_gens[tIdx.index].generation++;
        trans_fn_gens[tIdx.index].free = true;
        trans_fns[tIdx.index].deinit();
        return true;
    }
    return false;
}

fn DataRendererStore::removeRenderer(RendererIndex rIdx) -> bool {
    // there is always a transfer-function for a renderer, since it cannot be removed.
    trans_fn_gens[rIdx.index].generation++;
    trans_fn_gens[rIdx.index].free = true;
    trans_fns[rIdx.index].deinit();
    switch (rIdx.index) {
        case 0:
            if (g0 == rIdx.generation) {
				g0++;
                free |= F0;
                return true;
			}
            break;
        case 1:
            if (g1 == rIdx.generation) {
				g1++;
                free |= F1;
                return true;
			}
            break;
        case 2:
            if (g2 == rIdx.generation) {
				g2++;
                free |= F2;
                return true;
			}
            break;
        case 3:
            if (g3 == rIdx.generation) {
				g3++;
                free |= F3;
                return true;
			}
            break;
        case 4:
            if (g4 == rIdx.generation) {
				g4++;
                free |= F4;
                return true;
			}
            break;
        case 5:
            if (g5 == rIdx.generation) {
				g5++;
                free |= F5;
                return true;
			}
            break;
        default:
            qFatal("request to remove element with out of bounds index: %d", rIdx.index);
    }
    return false;
}

fn DataRendererStore::begin() -> DataRendererStore::DataRendererStoreIter {
    return DataRendererStore::DataRendererStoreIter(this, 0);
}

fn DataRendererStore::end() -> DataRendererStore::DataRendererStoreIter {
    return DataRendererStore::DataRendererStoreIter(this, 6);
}

fn DataRendererStore::deleteTextures() -> void {
    for (auto& tFn : trans_fns) {
        tFn.deleteTexture();
    }
}

DataRendererStore::DataRendererStoreIter::DataRendererStoreIter(DataRendererStore* store, u8 idx) : store{store}, idx{idx} {

}

DataRendererStore::DataRendererStoreIter& DataRendererStore::DataRendererStoreIter::operator++() {
    idx++;
    return *this;
}

DataRenderer* DataRendererStore::DataRendererStoreIter::operator*() {
    return store->getRendererByIdx(idx);
}

bool DataRendererStore::DataRendererStoreIter::operator==(const DataRendererStore::DataRendererStoreIter& other) const {
    return store == other.store && idx == other.idx;
}

bool DataRendererStore::DataRendererStoreIter::operator!=(const DataRendererStore::DataRendererStoreIter& other) const {
    return store != other.store || idx != other.idx;
}
