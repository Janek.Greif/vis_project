//
// Created by janek on 04.02.22.
//

#ifndef VIS_PROJECT_DAT_READER_H
#define VIS_PROJECT_DAT_READER_H
#include <filesystem>

#include <QFutureWatcher>
#include "volume.h"
#include "../util/MoveCell.h"
#include "../oxidize.h"

enum class ReadVolumeDatError {
    DAT_FILE_NOT_FOUND,
    INI_FILE_NOT_FOUND,
    ERROR_READING_FILE,
    FILE_TOO_BIG,
};

fn read_volume_dat(const std::filesystem::path& name) -> Result<Volume, ReadVolumeDatError>;

fn read_volume_dat_with_dialog(QFutureWatcher<Result<MoveCell<Volume>, ReadVolumeDatError>>& watcher, QWidget* parent = nullptr) -> bool;

#endif //VIS_PROJECT_DAT_READER_H
