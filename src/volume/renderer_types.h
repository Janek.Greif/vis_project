//
// Created by janek on 18.02.22.
//

#ifndef VIS_PROJECT_RENDERER_TYPES_H
#define VIS_PROJECT_RENDERER_TYPES_H
#include <variant>

struct VolumeRenderer {};
struct XRayRenderer {};
struct SliceRenderer {};
struct ContourRenderer {};

static constinit VolumeRenderer VOLUME{};
static constinit XRayRenderer XRAY{};
static constinit SliceRenderer SLICE{};
static constinit ContourRenderer CONTOUR{};

#ifdef Q_TEST
struct EmptyRenderer {};
static constinit EmptyRenderer EMPTY_RENDERER{};
using Renderer = std::variant<VolumeRenderer, XRayRenderer, SliceRenderer, ContourRenderer, EmptyRenderer>;
#else
using Renderer = std::variant<VolumeRenderer, XRayRenderer, SliceRenderer, ContourRenderer>;
#endif


#endif //VIS_PROJECT_RENDERER_TYPES_H
