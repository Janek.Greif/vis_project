//
// Created by janek on 15.02.22.
//
#include <QKeyEvent>

#include <imgui.h>
#include <ImGuizmo.h>

#include "DataRenderer.h"
#include "../ui/VolumeWidget.h"

DataRenderer::DataRenderer(std::shared_ptr<VolumeTexture> volTex, std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<ShaderEnvironment> shaderEnv):
        vol_tex{std::move(volTex)},
        vol_wid{nullptr},
        geo_env{std::move(geoEnv)},
        shader{std::move(shaderEnv)},
        perspective_matrix{},
        orthographic_matrix{},
        current_x{0},
        current_y{0},
        previous_x{0},
        previous_y{0},
        mouse_pressed{false},
        initialized{ false },
        needs_redraw{ false },
        camera_position{std::make_shared<CameraPosition>()},
        camera_zoom{std::make_shared<CameraZoom>()},
        trans_fn{},
        slice_positions{std::make_shared<SlicePositions>()},
        light{std::make_shared<Light>()},
        renderer_fbo{},
        white_space_skip_start{},
        white_space_skip_end{},
        animating{false},
        backed_up_transfer_fn_tex{None},
        xray{
            .mode = Mode::MAXIMUM,
        },
        volume{
            .color_density = 10.0f,
        },
        slice{
            .showRotate = false,
            .showTranslate = false,
            .showScale = false,
            .showGlobalTranslate = false,
            .polygon{
                QOpenGLBuffer{QOpenGLBuffer::VertexBuffer},
                QOpenGLBuffer{QOpenGLBuffer::VertexBuffer},
                QOpenGLBuffer{QOpenGLBuffer::VertexBuffer}
            },
        },
        general{
            .quality = 1.0f,
            .projection = Projection::PERSPECTIVE,
        },
#ifdef Q_TEST
        renderer{ EMPTY_RENDERER }
#else
        renderer{ XRAY }
#endif
{
    static u8 i{0};
    render_index = i++;

    QObject::connect(&*vol_tex, &VolumeTexture::newVolumeDataRendererUpdate, [this](){
        if (initialized) {
            vol_wid->sampleTransferFunction();
            markForRedraw();
            update();
        }
    });
}

fn DataRenderer::init(VolumeWidget* volWid, TransFnIndex tIdx) -> void {
    if (!initialized && volWid != nullptr) {
        // don't initialize, if there is no VolumeWidget given
        initializeOpenGLFunctions();

        geo_env->create();
        shader->init();
        for (auto& sp : slice.polygon) {
            if (!sp.isCreated()) {
                sp.create();
            }
            sp.bind();
            sp.allocate(6 * sizeof(QVector3D));
        }
    }

    initialized = true;
    vol_wid = volWid;
    trans_fn = std::make_shared<TransFn>(tIdx);
    initRenderer();
}

fn DataRenderer::detachFromRenderer() -> void {
//    vol_wid = nullptr;
}

fn DataRenderer::update(u32 count) -> void {
    update_count = count;
#ifdef Q_TEST
    render();
#else
    TRY(vol_wid)->update();
#endif
}

fn DataRenderer::update2(PropertyLinkType type) -> void {
    vol_wid->update2(type);
}

fn DataRenderer::mouseMove(QMouseEvent* event) -> void {
    if (!ImGuizmo::IsOver() && !ImGuizmo::IsUsing() && mouse_pressed) {
        let pos{ event->position() };
        current_x = static_cast<f32>(pos.x());
        current_y = static_cast<f32>(pos.y());

        let update{
            std::visit(overloaded{
                [this, event](const VolumeRenderer& r){return mouseVolume(event, r);},
                [this, event](const XRayRenderer& r){return mouseXRay(event, r);},
                [this, event](const SliceRenderer& r){return mouseSlice(event, r);},
                [this, event](const ContourRenderer& r){return mouseContour(event, r);},
#ifdef Q_TEST
                [this, event](const EmptyRenderer& r){return mouseEmpty(event, r);},
#endif
            }, renderer)
        };

        previous_x = current_x;
        previous_y = current_y;

        if (update){
            update2(*update);
        }
    } else {
        this->update();
    }
}

fn DataRenderer::mousePress(QMouseEvent* event) -> void {
    mouse_pressed = true;
    let pos{ event->position() };
    current_x = static_cast<f32>(pos.x());
    current_y = static_cast<f32>(pos.y());

    previous_x = current_x;
    previous_y = current_y;

    update(2);
}

fn DataRenderer::mouseRelease(QMouseEvent* event) -> void {
    mouse_pressed = false;
    update(2);
}

fn DataRenderer::keyPress(QKeyEvent* event) -> void {
    // R: rotate
    // T: translate
    // G: global translate
    // F: focus zoom | Z: zoom

    // L: light pos
    if (event->key() == Qt::Key_R) {
        if (slice_positions->focus == render_index) {
            slice.showRotate = !slice.showRotate;
        } else {
            slice.showRotate = true;
        }
        slice.showTranslate = false;
        slice.showScale = false;
        slice.showGlobalTranslate = false;
        light->focus = 255;
        slice_positions->focus = render_index;
        update2(PropertyLinkType::SLICE_POSITIONS);
    } else if (event->key() == Qt::Key_T) {
        if (slice_positions->focus == render_index) {
            slice.showTranslate = !slice.showTranslate;
        } else {
            slice.showTranslate = true;
        }
        slice.showRotate = false;
        slice.showScale = false;
        slice.showGlobalTranslate = false;
        light->focus = 255;
        slice_positions->focus = render_index;
        update2(PropertyLinkType::SLICE_POSITIONS);
    } else if (event->key() == Qt::Key_F || event->key() == Qt::Key_Z) {
        if (slice_positions->focus == render_index) {
            slice.showScale = !slice.showScale;
        } else {
            slice.showScale = true;
        }
        slice.showRotate = false;
        slice.showTranslate = false;
        slice.showGlobalTranslate = false;
        light->focus = 255;
        slice_positions->focus = render_index;
        update2(PropertyLinkType::SLICE_POSITIONS);
    } else if (event->key() == Qt::Key_G) {
        if (slice_positions->focus == render_index) {
            slice.showGlobalTranslate = !slice.showGlobalTranslate;
        } else {
            slice.showGlobalTranslate = true;
        }
        slice.showRotate = false;
        slice.showTranslate = false;
        slice.showScale = false;
        light->focus = 255;
        slice_positions->focus = render_index;
        update2(PropertyLinkType::SLICE_POSITIONS);
    }

    if (event->key() == Qt::Key_L) {
        if (light->focus == render_index) {
            light->focus = 255;
        } else {
            light->focus = render_index;
        }
        slice.showRotate = false;
        slice.showTranslate = false;
        slice.showScale = false;
        slice.showGlobalTranslate = false;
        update2(PropertyLinkType::LIGHT);
    }
    update();
}

fn DataRenderer::keyRelease(QKeyEvent* event) -> void {
}

fn DataRenderer::mouseScroll(QWheelEvent* event) -> void{
    if (!ImGuizmo::IsOver() && !ImGuizmo::IsUsing()) {
        if(mouseZoom(event)){
            update2(PropertyLinkType::CAMERA_ZOOM);
        }
    }else{
        this->update();
    }

}

fn DataRenderer::render() -> void {
    if (needs_redraw) {
        TRY(renderer_fbo).bind();

        std::visit(overloaded{
                [this](const VolumeRenderer &r) { renderVolume(r); },
                [this](const XRayRenderer &r) { renderXRay(r); },
                [this](const SliceRenderer &r) { renderSlice(r); },
                [this](const ContourRenderer &r) { renderContour(r); },
#ifdef Q_TEST
                [this](const EmptyRenderer& r){return renderEmpty(r);},
#endif
        }, renderer);

        TRY(renderer_fbo).unbind();

        needs_redraw = false;
    }

#ifndef Q_TEST
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if (renderer_fbo) {
        shader->depth_texture.bind();

        glActiveTexture(GL_TEXTURE1);
        renderer_fbo.bindTex();
        shader->depth_texture.setUniformValue("screenTexture", 1);

        glActiveTexture(GL_TEXTURE2);
        renderer_fbo.bindDepth();
        shader->depth_texture.setUniformValue("depthTexture", 2);

        geo_env->quad.bind();

        let location{ shader->depth_texture.attributeLocation("vertexPosition") };
        shader->depth_texture.enableAttributeArray(location);
        shader->depth_texture.setAttributeBuffer(location,GL_FLOAT,0,3,sizeof(QVector3D));

        geo_env->quad.draw();
        renderer_fbo.unbindDepth();
        renderer_fbo.unbindTex();
    }
#endif

    std::visit(overloaded{
            [this](const VolumeRenderer& r){return renderImGuiVolume(r);},
            [this](const XRayRenderer& r){return renderImGuiXRay(r);},
            [this](const SliceRenderer& r){return renderImGuiSlice(r);},
            [this](const ContourRenderer& r){return renderImGuiContour(r);},
#ifdef Q_TEST
            [this](const EmptyRenderer& r){return renderImGuiEmpty(r);},
#endif
    }, renderer);
    if (update_count > 1) {
        update_count--;
        update(update_count);
    }
}

fn DataRenderer::reinit(const Renderer& rend) -> void {
    std::visit(overloaded{
        [this](const VolumeRenderer& r){reinitVolume(r);},
        [this](const XRayRenderer& r){reinitXRay(r);},
        [this](const SliceRenderer& r){reinitSlice(r);},
        [this](const ContourRenderer& r){reinitContour(r);},
#ifdef Q_TEST
        [this](const EmptyRenderer& r){return reinitEmpty(r);},
#endif
    }, rend);
}

fn DataRenderer::initRenderer() -> void {
    std::visit(overloaded{
        [this](const VolumeRenderer& r){initVolume(r);},
        [this](const XRayRenderer& r){initXRay(r);},
        [this](const SliceRenderer& r){initSlice(r);},
        [this](const ContourRenderer& r){initContour(r);},
#ifdef Q_TEST
        [this](const EmptyRenderer& r){return initEmpty(r);},
#endif
    }, renderer);
}

fn DataRenderer::renderChange(Renderer r) -> void {
    reinit(r);
    renderer = r;
    markForRedraw();
    update();
}

fn DataRenderer::resize(i32 w, i32 h) -> void {
    width = w;
    height = h;
    let aspectRatio{ static_cast<f32>(w) / static_cast<f32>(h) };

    let nearPlane{ 0.5f };
    let farPlane{ 32.0f };
    let fov{ 45.0f };

    perspective_matrix.setToIdentity();
    perspective_matrix.perspective(fov, aspectRatio, nearPlane, farPlane);
    orthographic_matrix.setToIdentity();
    orthographic_matrix.ortho(-1.0, 1.0, -1.0, 1.0, nearPlane, farPlane);

    markForRedraw();
    renderer_fbo = Fbo{width, height};
    white_space_skip_start = Fbo{width, height};
    white_space_skip_end = Fbo{width, height};
}

fn DataRenderer::markForRedraw() -> void {
    needs_redraw = true;
}

fn DataRenderer::needsRedraw() -> bool {
    return needs_redraw;
}

fn DataRenderer::link(LinkProperty property, bool firstLink) -> void {
    std::visit(overloaded{
        [this, firstLink](CameraPositionProperty& cameraPosition) {
            if (firstLink) {
                *cameraPosition = *camera_position;
                camera_position = cameraPosition;
            } else {
                camera_position = cameraPosition;
                markForRedraw();
                update();
            }
        },
        [this, firstLink](CameraZoomProperty& cameraZoom) {
           if (firstLink) {
               *cameraZoom = *camera_zoom;
               camera_zoom = cameraZoom;
           } else {
               camera_zoom = cameraZoom;
               markForRedraw();
               update();
           }
        },
        [this, firstLink](TransFnProperty& transFn) {
            if (firstLink) {
                let newTransFn{ vol_wid->cloneTransFn(trans_fn->transfer_fn_idx) };
                assert(newTransFn);
                *transFn = TransFn{ *newTransFn };
                backed_up_transfer_fn_tex = *trans_fn;
                trans_fn = transFn;
            } else {
                backed_up_transfer_fn_tex = *trans_fn;
                trans_fn = transFn;
                markForRedraw();
                update();
            }
            vol_wid->notifyNewTransferFunction(trans_fn->transfer_fn_idx);
        },
        [this, firstLink](SlicePositionsProperty& slicePositions) {
            if (firstLink) {
                *slicePositions = *slice_positions;
                slice_positions = slicePositions;
                slice_positions->shared = true;
            } else {
                slice_positions = slicePositions;
            }
            vol_wid->notifyNewRenderSettings(createSettingsUpdateSignal());
            markForRedraw();
            update();
        },
        [this, firstLink](LightProperty& lightProperty) {
            if (firstLink) {
                *lightProperty = *light;
            }
            light = lightProperty;
            vol_wid->notifyNewRenderSettings(createSettingsUpdateSignal());
            markForRedraw();
            update();
        }
    }, property);
}

fn DataRenderer::unlink(PropertyLinkType linkType, bool lastLink) -> void {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch(linkType) {
        case PropertyLinkType::CAMERA_POS: {
            camera_position = std::make_shared<CameraPosition>(*camera_position);
            break;
        }
        case PropertyLinkType::CAMERA_ZOOM: {
            camera_zoom = std::make_shared<CameraZoom>(*camera_zoom);
            break;
        }
        case PropertyLinkType::TRANSFER_FUNCTION: {
            if (backed_up_transfer_fn_tex) {
                if (lastLink) {
                    vol_wid->removeTransFn(trans_fn->transfer_fn_idx);
                }
                trans_fn = std::make_shared<TransFn>(*backed_up_transfer_fn_tex);
                backed_up_transfer_fn_tex = None;
            } else {
                qFatal("unlink transfer function with no background");
            }
            markForRedraw();
            update();
            vol_wid->notifyNewTransferFunction(trans_fn->transfer_fn_idx);
            break;
        }
        case PropertyLinkType::SLICE_POSITIONS: {
            let slicePosition{ *slice_positions };
            slice_positions = std::make_shared<SlicePositions>(slicePosition);
            if (slice_positions->shared) {
                slice_positions->shared = false;
                markForRedraw();
                update();
            }
            break;
        }
        case PropertyLinkType::LIGHT: {
            light = std::make_shared<Light>(*light);
            break;
        }
    }
}


fn DataRenderer::getTransferFunctionIndex() -> TransFnIndex {
    return trans_fn->transfer_fn_idx;
}

fn DataRenderer::createSettingsUpdateSignal() -> SettingsUpdateSignal {
    return SettingsUpdateSignal{
        .quality = general,
        .xray{
            .mode = xray.mode ? Mode::AVERAGE : Mode::MAXIMUM,
        },
        .volume = volume,
        .slice = slice_positions->data,
        .light = light->data,
    };
}

fn DataRenderer::qualityUpdate(GeneralData data) -> void {
    general = data;
    markForRedraw();
    update();
}

fn DataRenderer::xRayUpdate(XRayData data) -> void {
    xray = data;
    markForRedraw();
    update();
}

fn DataRenderer::volumeUpdate(VolumeData data) -> void {
    volume = data;
    markForRedraw();
    update();
}

fn DataRenderer::sliceUpdate(SliceData data) -> void {
    slice_positions->data = data;
    update2(PropertyLinkType::SLICE_POSITIONS);
}

fn DataRenderer::lightUpdate(LightData data) -> void {
    light->data = data;
    update2(PropertyLinkType::LIGHT);
}
