//
// Created by janek on 04.02.22.
//

#ifndef VIS_PROJECT_GEOMETRYBUFFER_H
#define VIS_PROJECT_GEOMETRYBUFFER_H

#include <QOpenGLBuffer>
#include <QOpenGLExtraFunctions>
#include <QVector3D>
#include <QDebug>

#include "../oxidize.h"

extern constinit QVector3D QUAD_VERTEX_DATA[4];
extern constinit u16 QUAD_INDEX_DATA[6];
extern constinit u16 QUAD_LINE_INDEX_DATA[8];

extern constinit QVector3D CUBE_VERTEX_DATA[8];
extern constinit u16 CUBE_INDEX_DATA[36];
extern constinit u16 CUBE_LINE_INDEX_DATA[24];

class GeometryBuffer : protected QOpenGLExtraFunctions {
public:
    NO_COPY(GeometryBuffer);

    template<usize V, usize I, usize L>
    GeometryBuffer(QVector3D (&vert)[V], u16 (&indi)[I], u16 (&lineIndi)[L]) :
            vertex_buffer{std::move(std::make_unique<QOpenGLBuffer>(QOpenGLBuffer::VertexBuffer))},
            index_buffer{std::move(std::make_unique<QOpenGLBuffer>(QOpenGLBuffer::IndexBuffer))},
            line_index_buffer{std::move(std::make_unique<QOpenGLBuffer>(QOpenGLBuffer::IndexBuffer))},
            vertices{vert},
            indices{indi},
            line_indices{lineIndi},
            vertices_count{V},
            indices_count{I},
            line_indices_count{L},
            created{false}
    {
//        qDebug() << "vertex count:" << V << "index count:" << I;
    }

    GeometryBuffer(GeometryBuffer&& buffer) = default;
    GeometryBuffer& operator=(GeometryBuffer&& buffer) = default;

    fn bind() -> void;
    fn unbind() -> void;
    fn bindScaffold() -> void;
    fn unbindScaffold() -> void;
    fn draw() -> void;
    fn draw(u32 count) -> void;
    fn drawScaffold() -> void;
    fn drawScaffold(u32 count) -> void;
    fn create() -> void;

private:
    // static
    QVector3D* vertices;
    u16* indices;
    u16* line_indices;

    // allocated
    std::unique_ptr<QOpenGLBuffer> vertex_buffer;
    std::unique_ptr<QOpenGLBuffer> index_buffer;
    std::unique_ptr<QOpenGLBuffer> line_index_buffer;

    //normal
    usize vertices_count;
    usize indices_count;
    usize line_indices_count;

    bool created;
};

#endif //VIS_PROJECT_GEOMETRYBUFFER_H
