//
// Created by janek on 25.02.22.
//

#include "SharedProperties.h"

SharedProperties::SharedProperties() : camera_position_properties{}
{ }

template<Property P>
fn SharedProperties::getCheckedElement(
        std::vector<PropertyElement<P>>& properties,
        LinkToken token
) -> SharedProperties::PropertyElement<P>* {
    if (token.id < properties.size()) {
        auto element{ &properties[token.id] };
        if (element->generation == token.generation && !(element->free)) {
            return element;
        }
    }
    return nullptr;
}

template<Property P>
auto SharedProperties::requestLinkToken(std::vector<PropertyElement<P>>& properties) -> LinkToken {
    for (auto[i, element] : properties | enumerate | FILTER(it.second.free)) {
        element.data = std::make_shared<P>();
        element.generation++;
        return LinkToken { P::TOKEN_TYPE, static_cast<u8>(i), element.generation };
    }
    LinkToken newToken{ P::TOKEN_TYPE, static_cast<u8>(properties.size()), 0 };
    properties.emplace_back(std::make_shared<P>(), static_cast<u8>(0), std::make_shared<std::vector<RendererIndex>>(), false);
    return newToken;
}

template<Property P>
fn SharedProperties::removeLinkToken(std::vector<PropertyElement<P>>& properties, LinkToken token) -> bool {
    if (let element{ getCheckedElement(properties, token)}; element) {
        if (element->linked_renderers->empty()) {
            element->data = std::make_shared<P>();
            element->free = true;
            return true;
        }
    }
    return false;
}

template<Property P>
fn SharedProperties::addLink(
        std::vector<PropertyElement<P>>& properties,
        LinkToken token,
        RendererIndex rIdx
) -> Option<std::pair<LinkProperty, bool>> {
    if (let element{ getCheckedElement(properties, token)}; element) {
        let end{ element->linked_renderers->end() };
        if (std::find(element->linked_renderers->begin(), end, rIdx) == end) {
            element->linked_renderers->push_back(rIdx);
            return std::make_pair(element->data, element->linked_renderers->size() == 1);
        }
    }
    return None;
}

template<Property P>
fn SharedProperties::removeLink(std::vector<PropertyElement<P>>& properties, LinkToken token, RendererIndex rIdx) -> bool {
    if (let element{ getCheckedElement(properties, token)}; element) {
        let it{ std::remove(element->linked_renderers->begin(), element->linked_renderers->end(), rIdx) };
        element->linked_renderers->erase(it, element->linked_renderers->end());
        return element->linked_renderers->empty();
    }
    return false;
}

template<Property P>
fn SharedProperties::isLinked(std::vector<PropertyElement<P>>& properties, RendererIndex rIdx) -> bool {
    return std::ranges::any_of(properties, [rIdx](const PropertyElement<P>& element){
        let end{ element.linked_renderers->end() };
        return std::find(element.linked_renderers->begin(), end, rIdx) != end;
    });
}

template<Property P>
fn SharedProperties::removeAllLinks(std::vector<PropertyElement<P>>& properties, RendererIndex rIdx) -> void {
    for (let& element : properties) {
        let end{ std::remove(element.linked_renderers->begin(), element.linked_renderers->end(), rIdx) };
        element.linked_renderers->erase(end, element.linked_renderers->end());
    }
}

fn SharedProperties::getProperty(LinkToken token) -> Option<LinkProperty> {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch (token.type) {
        case PropertyLinkType::CAMERA_POS:
            if (let element{ getCheckedElement(camera_position_properties, token)}; element) {
                return element->data;
            }
            break;
        case PropertyLinkType::CAMERA_ZOOM:
            if (let element{ getCheckedElement(camera_zoom_properties, token)}; element) {
                return element->data;
            }
            break;
        case PropertyLinkType::TRANSFER_FUNCTION:
            if (let element{ getCheckedElement(trans_fn_properties, token)}; element) {
                return element->data;
            }
            break;
        case PropertyLinkType::SLICE_POSITIONS:
            if (let element{ getCheckedElement(slice_positions_properties, token)}; element) {
                return element->data;
            }
            break;
        case PropertyLinkType::LIGHT:
            if (let element{ getCheckedElement(light_properties, token)}; element) {
                return element->data;
            }
            break;
    }
    return None;
}

fn SharedProperties::getLinkedRenderers(LinkToken token) -> Option<std::shared_ptr<std::vector<RendererIndex>>> {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch (token.type) {
        case PropertyLinkType::CAMERA_POS:
            if (let element{ getCheckedElement(camera_position_properties, token)}; element) {
                return element->linked_renderers;
            }
            break;
        case PropertyLinkType::CAMERA_ZOOM:
            if (let element{ getCheckedElement(camera_zoom_properties, token)}; element) {
                return element->linked_renderers;
            }
            break;
        case PropertyLinkType::TRANSFER_FUNCTION:
            if (let element{ getCheckedElement(trans_fn_properties, token)}; element) {
                return element->linked_renderers;
            }
            break;
        case PropertyLinkType::SLICE_POSITIONS:
            if (let element{ getCheckedElement(slice_positions_properties, token)}; element) {
                return element->linked_renderers;
            }
            break;
        case PropertyLinkType::LIGHT:
            if (let element{ getCheckedElement(light_properties, token)}; element) {
                return element->linked_renderers;
            }
            break;
    }
    return None;
}

fn SharedProperties::requestToken(PropertyLinkType linkType) -> LinkToken {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch (linkType) {
        case PropertyLinkType::CAMERA_POS:
            return requestLinkToken(camera_position_properties);
        case PropertyLinkType::CAMERA_ZOOM:
            return requestLinkToken(camera_zoom_properties);
        case PropertyLinkType::TRANSFER_FUNCTION:
            return requestLinkToken(trans_fn_properties);
        case PropertyLinkType::SLICE_POSITIONS:
            return requestLinkToken(slice_positions_properties);
        case PropertyLinkType::LIGHT:
            return requestLinkToken(light_properties);
    }
    qFatal("switch-case is missing other cases");
}

fn SharedProperties::removeToken(LinkToken token) -> bool {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch(token.linkType()) {
        case PropertyLinkType::CAMERA_POS:
            return removeLinkToken(camera_position_properties, token);
        case PropertyLinkType::CAMERA_ZOOM:
            return removeLinkToken(camera_zoom_properties, token);
        case PropertyLinkType::TRANSFER_FUNCTION:
            return removeLinkToken(trans_fn_properties, token);
        case PropertyLinkType::SLICE_POSITIONS:
            return removeLinkToken(slice_positions_properties, token);
        case PropertyLinkType::LIGHT:
            return removeLinkToken(light_properties, token);
    }
    qFatal("switch-case is missing other cases");
}

fn SharedProperties::tokenInUse(LinkToken token) -> bool {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch(token.linkType()) {
        case PropertyLinkType::CAMERA_POS:
            if (let element{ getCheckedElement(camera_position_properties, token)}; element) {
                return !element->linked_renderers->empty();
            }
            break;
        case PropertyLinkType::CAMERA_ZOOM:
            if (let element{ getCheckedElement(camera_zoom_properties, token)}; element) {
                return !element->linked_renderers->empty();
            }
            break;
        case PropertyLinkType::TRANSFER_FUNCTION:
            if (let element{ getCheckedElement(trans_fn_properties, token)}; element) {
                return !element->linked_renderers->empty();
            }
            break;
        case PropertyLinkType::SLICE_POSITIONS:
            if (let element{ getCheckedElement(slice_positions_properties, token)}; element) {
                return !element->linked_renderers->empty();
            }
            break;
        case PropertyLinkType::LIGHT:
            if (let element{ getCheckedElement(light_properties, token)}; element) {
                return !element->linked_renderers->empty();
            }
            break;
    }
    return false;
}



fn SharedProperties::addLink(LinkToken token, RendererIndex index) -> Option<std::pair<LinkProperty, bool>> {
    if (isLinked(token.type, index)) {
        return None;
    }
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch(token.type) {
        case PropertyLinkType::CAMERA_POS:
            return addLink(camera_position_properties, token, index);
        case PropertyLinkType::CAMERA_ZOOM:
            return addLink(camera_zoom_properties, token, index);
        case PropertyLinkType::TRANSFER_FUNCTION:
            return addLink(trans_fn_properties, token, index);
        case PropertyLinkType::SLICE_POSITIONS:
            return addLink(slice_positions_properties, token, index);
        case PropertyLinkType::LIGHT:
            return addLink(light_properties, token, index);
    }
    qFatal("switch-case is missing other cases");
}

fn SharedProperties::removeLink(LinkToken token, RendererIndex index) -> bool {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch(token.type) {
        case PropertyLinkType::CAMERA_POS:
            return removeLink(camera_position_properties, token, index);
        case PropertyLinkType::CAMERA_ZOOM:
            return removeLink(camera_zoom_properties, token, index);
        case PropertyLinkType::TRANSFER_FUNCTION:
            return removeLink(trans_fn_properties, token, index);
        case PropertyLinkType::SLICE_POSITIONS:
            return removeLink(slice_positions_properties, token, index);
        case PropertyLinkType::LIGHT:
            return removeLink(light_properties, token, index);
    }
    qFatal("switch-case is missing other cases");
}

fn SharedProperties::isLinked(PropertyLinkType linkType, RendererIndex index) -> bool {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch(linkType) {
        case PropertyLinkType::CAMERA_POS:
            return isLinked(camera_position_properties, index);
        case PropertyLinkType::CAMERA_ZOOM:
            return isLinked(camera_zoom_properties, index);
        case PropertyLinkType::TRANSFER_FUNCTION:
            return isLinked(trans_fn_properties, index);
        case PropertyLinkType::SLICE_POSITIONS:
            return isLinked(slice_positions_properties, index);
        case PropertyLinkType::LIGHT:
            return isLinked(light_properties, index);
    }
    qFatal("switch-case is missing other cases");
}

fn SharedProperties::removeAllLinks(RendererIndex idx) -> void {
    static_assert(PROPERTY_LINK_COUNT == 5, "consider all cases");
    removeAllLinks(camera_position_properties, idx);
    removeAllLinks(camera_zoom_properties, idx);
    removeAllLinks(trans_fn_properties, idx);
    removeAllLinks(slice_positions_properties, idx);
    removeAllLinks(light_properties, idx);
}

