//
// Created by janek on 25.02.22.
//

#ifndef VIS_PROJECT_PROPERTY_LINK_COUNT_H
#define VIS_PROJECT_PROPERTY_LINK_COUNT_H

#include <concepts>

const std::integral auto PROPERTY_LINK_COUNT = 5;

#endif //VIS_PROJECT_PROPERTY_LINK_COUNT_H
