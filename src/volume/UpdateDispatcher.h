//
// Created by janek on 22.02.22.
//

#ifndef VIS_PROJECT_UPDATEDISPATCHER_H
#define VIS_PROJECT_UPDATEDISPATCHER_H

#include <memory>
#include <variant>
#include <span>

#include <QObject>
#include <QTimer>
#include <QMultiHash>
#include "DataRenderer.h"
#include "DataRendererStore.h"
#include "property_link.h"
#include "LinkToken.h"
#include "../ui/render_settings_event.h"
#include "../oxidize.h"

namespace UDState {

struct Idle{};
struct EventPause{};

using State = std::variant<Idle, EventPause>;

}

class UpdateDispatcher : public QObject{
    Q_OBJECT

    NO_COPY(UpdateDispatcher);
public:
    UpdateDispatcher(std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<VolumeTexture> volTex, std::shared_ptr<ShaderEnvironment> shaderEnv, QObject* parent = nullptr);

    fn requestRenderer(VolumeWidget* volWidget) -> Option<RendererIndex>;
    fn removeRender(RendererIndex rIdx) -> bool;
    fn cloneTransFn(TransFnIndex tIdx) -> Option<TransFnIndex>;
    fn globalUpdate(RendererIndex idx) -> void;
    fn updateAll() -> void;
    fn link(RendererIndex idx, LinkProperty property, bool firstLink) -> void;
    fn unlink(RendererIndex idx, PropertyLinkType linkType, bool lastLink) -> void;
    fn setRenderer(RendererIndex rIdx, Renderer renderer) -> void;

    fn resize(RendererIndex idx, i32 w, i32 h) -> void;
    fn render(RendererIndex idx) -> void;
    fn mouseRelease(RendererIndex idx, QMouseEvent* event) -> void;
    fn mousePress(RendererIndex idx, QMouseEvent* event) -> void;
    fn mouseMove(RendererIndex idx, QMouseEvent* event) -> void;
    fn mouseScroll(RendererIndex idx, QWheelEvent* event) -> void;
    fn keyPress(RendererIndex idx, QKeyEvent* event) -> void;
    fn keyRelease(RendererIndex idx, QKeyEvent* event) -> void;

    fn markForRedraw(std::span<const RendererIndex> other_renderers) -> void;
    fn detachFromRenderer(RendererIndex idx) -> void;

//    fn getTransFn(RendererIndex idx) ->
    fn getTransFn(TransFnIndex idx) -> TransferFunction*;
    fn getTransFn(TransFnIndex idx) const -> const TransferFunction*;
    fn removeTransFn(TransFnIndex idx) -> void;

    fn startAnimation(RendererIndex idx, PropertyLinkType linkType) -> void;
    fn stopAnimation(RendererIndex idx, PropertyLinkType linkType) -> void;

    fn sampleTransFn(TransFnIndex idx) -> void;
    fn getTransferFunctionIndexForRenderer(RendererIndex rIdx) -> Option<TransFnIndex>;
    fn createSettingsUpdateSignal(RendererIndex rIdx) -> Option<SettingsUpdateSignal>;
    fn qualityUpdate(RendererIndex rIdx, GeneralData data) -> void;
    fn xRayUpdate(RendererIndex rIdx, XRayData data) -> void;
    fn volumeUpdate(RendererIndex rIdx, VolumeData data) -> void;
    fn sliceUpdate(RendererIndex rIdx, SliceData data) -> void;
    fn lightUpdate(RendererIndex rIdx, LightData data) -> void;

public slots:
    fn deleteTextures() -> void;

private slots:
    fn animationUpdate() -> void;

private:
    DataRendererStore all_renderers;

    UDState::State state;

    QTimer animation_timer;

    QMultiHash<PropertyLinkType, RendererIndex> active_animations;

    friend class UpdateDispatcherTest;
};


#endif //VIS_PROJECT_UPDATEDISPATCHER_H
