//
// Created by janek on 04.02.22.
//

#include <QDebug>
#include <QtConcurrent>
#include "TransferFunction.h"
#include "VolumeTexture.h"

VolumeBlock::VolumeBlock(const Volume& volume, i32 xMin, i32 yMin, i32 zMin) {
    let xMax{ std::min(xMin+BLOCK_DIM, static_cast<i32>(volume.width)) };
    let yMax{ std::min(yMin+BLOCK_DIM, static_cast<i32>(volume.height)) };
    let zMax{ std::min(zMin+BLOCK_DIM, static_cast<i32>(volume.depth)) };

    min = 1.0;
    max = 0.0;

    for (let z : ints(zMin, zMax)) {
        for (let y : ints(yMin, yMax)) {
            for (let x : ints(xMin, xMax)) {
                let vV{ volume[{x, y, z}] };
                min = std::min(min, vV);
                max = std::max(max, vV);
            }
        }
    }
}

VolumeTexture::VolumeTexture(std::shared_ptr<ShaderEnvironment> shaderEnv) :
    volume{std::nullopt},
    shader_env{std::move(shaderEnv)},
    tex{QOpenGLTexture::Target3D},
    gradient{QOpenGLTexture::Target3D},
    histogram{QOpenGLTexture::Target1D},
    initialized{false},
    histogram_future{nullptr}
{
}

fn VolumeTexture::bind() -> void {
    init();
    TRY(tex).bind();
}

fn VolumeTexture::unbind() -> void {
    TRY(tex).release();
}

fn VolumeTexture::bindGradient() -> void {
    init();
    TRY(gradient).bind();
}

fn VolumeTexture::unbindGradient() -> void {
    TRY(gradient).release();
}

fn VolumeTexture::bindHistogram() -> void {
    init();
    TRY(histogram).bind();
}

fn VolumeTexture::unbindHistogram() -> void {
    TRY(histogram).release();
}

fn VolumeTexture::overrideVolume(MoveCell<Volume> pVol) -> void {
    if (histogram_future) {
        delete histogram_future;
        histogram_future = nullptr;
    }
    histogram_future = new QFutureWatcher<HistogramFuture>{};
    connect(histogram_future, &QFutureWatcher<HistogramFuture>::finished, this, &VolumeTexture::doComputeShader);

    auto task{
            QtConcurrent::task([](MoveCell<Volume> pVol) -> HistogramFuture {
              if (auto vol{ pVol.take() }; vol) {
                  std::vector<u32> counts(4096, 0);
                  for (let vd : vol->data) {
                      counts[vd / 16]++;
                  }
                  let max{ static_cast<f32>(*std::max_element(counts.begin(), counts.end())) };
                  std::vector<f32> normalizedCounts(4096, 0.0f);
                  for (auto[nc, c] : zip(normalizedCounts, counts)) {
                      nc = static_cast<f32>(c) / max;
                  }

                  vol->buildTree();
                  return MoveCell{std::move(std::make_pair<std::vector<f32>, Volume>(std::move(normalizedCounts), std::move(*vol)))};
              } else {
                  return None;
              }
        })
        .withArguments(pVol)
        .spawn()
    };
    emit setCursor(Qt::CursorShape::WaitCursor);
    histogram_future->setFuture(task);
//    if (auto vol{ pVol.take() }; vol) {
//
//        TRY(tex).destroy();
//        TRY(gradient).destroy();
//        TRY(histogram).destroy();
//        GLTex rawGradient{QOpenGLTexture::Target3D};
//        histogram.create1DImage(4096);
//        tex.create3DImage(volume->width, volume->height, volume->depth);
//        rawGradient.create3DImage(volume->width, volume->height, volume->depth, QOpenGLTexture::RGBA32F);
//
//        let grnGX{ static_cast<i32>(std::ceil(static_cast<f32>(volume->width)/3.0)) };
//        let grnGY{ static_cast<i32>(std::ceil(static_cast<f32>(volume->height)/3.0)) };
//        let grnGZ{ static_cast<i32>(std::ceil(static_cast<f32>(volume->depth)/3.0)) };
//        gradient.create3DImage(grnGX, grnGY, grnGZ, QOpenGLTexture::RGBA32F);
//
//        let data{ reinterpret_cast<const void*>(volume->data.data()) };
//        let histogramData{ reinterpret_cast<const void*>(normalizedCounts.data()) };
//        tex.setData(
//                  0,
//                  0,
//                  0,
//                volume->width,
//                volume->height,
//                volume->depth,
//                  QOpenGLTexture::Red,
//                  QOpenGLTexture::UInt16,
//                data
//        );
//        histogram.setData(0, 4096, QOpenGLTexture::Red, QOpenGLTexture::Float32, histogramData);
//
//        i32 gSX = 4, gSY = 4, gSZ = 4;
//        i32 nGX = std::ceil(static_cast<f32>(volume->width)/static_cast<f32>(gSX));
//        i32 nGY = std::ceil(static_cast<f32>(volume->height)/static_cast<f32>(gSY));
//        i32 nGZ = std::ceil(static_cast<f32>(volume->depth)/static_cast<f32>(gSZ));
//
//
//        shader_env->gradient.bind();
//        glBindImageTexture(0, tex.textureId(), 0, GL_TRUE, 0, GL_READ_ONLY, GL_R32F);
//        glBindImageTexture(1, rawGradient.textureId(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA32F);
//        glDispatchCompute(nGX, nGY, nGZ);
//        shader_env->gradient.release();
//
//        shader_env->low_pass.bind();
//        glBindImageTexture(0, rawGradient.textureId(), 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA32F);
//        glBindImageTexture(1, gradient.textureId(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA32F);
//        // yes, it is divided by 9 in total
//        glDispatchCompute(grnGX, grnGY, grnGZ);
//        shader_env->low_pass.release();
//        rawGradient.destroy();
//
//        emit newVolumeTransFnUpdate();
//        emit newVolumeDataRendererUpdate();
//    } else {
//        qCritical() << "Volume was empty";
//    }
}

fn VolumeTexture::doComputeShader() -> void {
    if (histogram_future) {
        if (auto volAndHisOpt{ histogram_future->result() }; volAndHisOpt) {
            if (auto volAndHis{ volAndHisOpt->take() }; volAndHis) {
                auto his{ std::move(std::get<0>(*volAndHis)) };
                auto vol{ std::move(std::get<1>(*volAndHis)) };
                volume = std::move(vol);
                init();

                TRY(tex).destroy();
                TRY(gradient).destroy();
                TRY(histogram).destroy();
                GLTex rawGradient{QOpenGLTexture::Target3D};
                histogram.create1DImage(4096);
                tex.create3DImage(volume->width, volume->height, volume->depth);
                rawGradient.create3DImage(volume->width, volume->height, volume->depth, QOpenGLTexture::RGBA32F);

                let grnGX{ static_cast<i32>(std::ceil(static_cast<f32>(volume->width)/3.0)) };
                let grnGY{ static_cast<i32>(std::ceil(static_cast<f32>(volume->height)/3.0)) };
                let grnGZ{ static_cast<i32>(std::ceil(static_cast<f32>(volume->depth)/3.0)) };
                gradient.create3DImage(grnGX, grnGY, grnGZ, QOpenGLTexture::RGBA32F);

                let data{ reinterpret_cast<const void*>(volume->data.data()) };
                let histogramData{ reinterpret_cast<const void*>(his.data()) };
                tex.setData(
                        0,
                        0,
                        0,
                        volume->width,
                        volume->height,
                        volume->depth,
                        QOpenGLTexture::Red,
                        QOpenGLTexture::UInt16,
                        data
                );
                histogram.setData(0, 4096, QOpenGLTexture::Red, QOpenGLTexture::Float32, histogramData);

                i32 gSX = 4, gSY = 4, gSZ = 4;
                i32 nGX = std::ceil(static_cast<f32>(volume->width)/static_cast<f32>(gSX));
                i32 nGY = std::ceil(static_cast<f32>(volume->height)/static_cast<f32>(gSY));
                i32 nGZ = std::ceil(static_cast<f32>(volume->depth)/static_cast<f32>(gSZ));


                shader_env->gradient.bind();
                glBindImageTexture(0, tex.textureId(), 0, GL_TRUE, 0, GL_READ_ONLY, GL_R32F);
                glBindImageTexture(1, rawGradient.textureId(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA32F);
                glDispatchCompute(nGX, nGY, nGZ);
                shader_env->gradient.release();

                shader_env->low_pass.bind();
                glBindImageTexture(0, rawGradient.textureId(), 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA32F);
                glBindImageTexture(1, gradient.textureId(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA32F);
                // yes, it is divided by 9 in total
                glDispatchCompute(grnGX, grnGY, grnGZ);
                shader_env->low_pass.release();
                rawGradient.destroy();

                emit newVolumeTransFnUpdate();
                emit newVolumeDataRendererUpdate();
            }
        }

        delete histogram_future;
        histogram_future = nullptr;
    }
    emit setCursor(Qt::CursorShape::ArrowCursor);
}

fn VolumeTexture::deleteTexture() -> void {
    TRY(tex).destroy();
    TRY(gradient).destroy();
    TRY(histogram).destroy();
}

fn VolumeTexture::getVolume() -> Option<Volume>& {
    return volume;
}

fn VolumeTexture::getVolume() const -> const Option<Volume>& {
    return volume;
}

fn VolumeTexture::init() -> void {
    if (!initialized) {
        initializeOpenGLFunctions();
        histogram.create1DImage(4096);
        initialized = true;
    }
}
