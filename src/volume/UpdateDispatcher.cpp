//
// Created by janek on 22.02.22.
//

#include <span>
#include "UpdateDispatcher.h"

UpdateDispatcher::UpdateDispatcher(std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<VolumeTexture> volTex, std::shared_ptr<ShaderEnvironment> shaderEnv, QObject* parent) :
    QObject{parent},
    all_renderers{std::move(volTex), std::move(geoEnv), std::move(shaderEnv) },
    animation_timer{},
    active_animations{}
{
    animation_timer.setInterval(41);
    connect(&animation_timer, &QTimer::timeout, this, &UpdateDispatcher::animationUpdate);
}

fn UpdateDispatcher::globalUpdate(RendererIndex idx) -> void {
    if (auto r{ all_renderers.get(idx) }; r) {
        r->update();
        updateAll();
    }
}

fn UpdateDispatcher::updateAll() -> void {
    let now{ std::chrono::system_clock::now() };
    auto it{ all_renderers.begin() };
    let end{ all_renderers.end() };
    for (; it != end; ++it) {
        let later{ std::chrono::system_clock::now() };
        let diff{ std::chrono::duration_cast<std::chrono::milliseconds>(later - now) };
        if (diff.count() >= 200) {
//            qDebug() << "render time:" << diff.count();
            break;
        }
        let renderer{ *it };
        if (renderer->needsRedraw()) {
            renderer->update();
        }
    }
    // if state is EventPause, then there is a future re-update already planned so no need to schedule another one.
    let hasUnfinishedUpdates{ std::holds_alternative<UDState::EventPause>(state) };

    auto anyOutdatedRenderers{ false };
    for (; it != end; ++it) {
        anyOutdatedRenderers |= (*it)->needsRedraw();
    }
    if (anyOutdatedRenderers && hasUnfinishedUpdates) {
        state = UDState::EventPause{};
        QMetaObject::invokeMethod(this, &UpdateDispatcher::updateAll, Qt::QueuedConnection);
    } else {
        state = UDState::Idle{};
    }
}

fn UpdateDispatcher::animationUpdate() -> void {
    let end{ active_animations.keyEnd() };
    for (auto keyIt{ active_animations.keyBegin() }; keyIt != end; ++keyIt) {
        for (let renderIdx : active_animations.values(*keyIt)) {
            TRY(all_renderers.get(renderIdx))->update2(*keyIt);
        }
    }
}

fn UpdateDispatcher::cloneTransFn(TransFnIndex tIdx) -> Option<TransFnIndex> {
    if (!all_renderers.getTransFn(tIdx)) {
        // cannot keep Transfer-Function across requestNewTransFn call, since it modifies the vector behind it, which may relocate.
        return None;
    }
    let newIdx{ all_renderers.requestNewTransFn() };
    auto newTransFn{ all_renderers.getTransFn(newIdx) };
    assert(newTransFn);
    let transFn{ all_renderers.getTransFn(tIdx) };
    assert(transFn);
    transFn->cloneInto(*newTransFn);
    return newIdx;
}

fn UpdateDispatcher::requestRenderer(VolumeWidget* volWidget) -> Option<RendererIndex> {
    return all_renderers.requestNew(volWidget);
}

fn UpdateDispatcher::removeRender(RendererIndex rIdx) -> bool {
    return all_renderers.removeRenderer(rIdx);
}

fn UpdateDispatcher::resize(RendererIndex idx, i32 w, i32 h) -> void {
    TRY(all_renderers.get(idx))->resize(w, h);
};

fn UpdateDispatcher::render(RendererIndex idx) -> void {
    TRY(all_renderers.get(idx))->render();
}

fn UpdateDispatcher::mouseRelease(RendererIndex idx, QMouseEvent* event) -> void {
    TRY(all_renderers.get(idx))->mouseRelease(event);
}

fn UpdateDispatcher::mousePress(RendererIndex idx, QMouseEvent* event) -> void {
    TRY(all_renderers.get(idx))->mousePress(event);
}

fn UpdateDispatcher::mouseMove(RendererIndex idx, QMouseEvent* event) -> void {
    TRY(all_renderers.get(idx))->mouseMove(event);
}

fn UpdateDispatcher::keyPress(RendererIndex idx, QKeyEvent* event) -> void {
    TRY(all_renderers.get(idx))->keyPress(event);
}

fn UpdateDispatcher::keyRelease(RendererIndex idx, QKeyEvent* event) -> void {
    TRY(all_renderers.get(idx))->keyRelease(event);
}

fn UpdateDispatcher::mouseScroll(RendererIndex idx, QWheelEvent* event) -> void {
    TRY(all_renderers.get(idx))->mouseScroll(event);
}

fn UpdateDispatcher::markForRedraw(std::span<const RendererIndex> other_renderers) -> void {
    for (let indices : other_renderers) {
        TRY(all_renderers.get(indices))->markForRedraw();
    }
}

fn UpdateDispatcher::link(RendererIndex idx, LinkProperty property, bool firstLink) -> void {
    TRY(all_renderers.get(idx))->link(std::move(property), firstLink);
}

fn UpdateDispatcher::unlink(RendererIndex idx, PropertyLinkType linkType, bool lastLink) -> void {
    TRY(all_renderers.get(idx))->unlink(linkType, lastLink);
}

fn UpdateDispatcher::setRenderer(RendererIndex rIdx, Renderer renderer) -> void {
    TRY(all_renderers.get(rIdx))->renderChange(renderer);
}

fn UpdateDispatcher::detachFromRenderer(RendererIndex idx) -> void {
    TRY(all_renderers.get(idx))->detachFromRenderer();
}

fn UpdateDispatcher::getTransFn(TransFnIndex idx) -> TransferFunction* {
    return all_renderers.getTransFn(idx);
}

fn UpdateDispatcher::getTransFn(TransFnIndex idx) const -> const TransferFunction* {
    return all_renderers.getTransFn(idx);
}

fn UpdateDispatcher::removeTransFn(TransFnIndex idx) -> void {
    all_renderers.remove(idx);
}

fn UpdateDispatcher::startAnimation(RendererIndex idx, PropertyLinkType linkType) -> void {
    active_animations.insert(linkType, idx);
    if (!animation_timer.isActive()) {
        animation_timer.start();
    }
}

fn UpdateDispatcher::stopAnimation(RendererIndex idx, PropertyLinkType linkType) -> void {
    active_animations.remove(linkType, idx);
    if (active_animations.empty()) {
        animation_timer.stop();
    }
}

fn UpdateDispatcher::sampleTransFn(TransFnIndex transFnIdx) -> void {
    TRY(getTransFn(transFnIdx))->sample();
}

fn UpdateDispatcher::getTransferFunctionIndexForRenderer(RendererIndex rIdx) -> Option<TransFnIndex> {
    if (let r{ all_renderers.get(rIdx) }; r) {
        return r->getTransferFunctionIndex();
    } else {
        return None;
    }
}

fn UpdateDispatcher::createSettingsUpdateSignal(RendererIndex rIdx) -> Option<SettingsUpdateSignal> {
    if (let r{ all_renderers.get(rIdx) }; r) {
        return r->createSettingsUpdateSignal();
    } else {
        return None;
    }
}

fn UpdateDispatcher::qualityUpdate(RendererIndex rIdx, GeneralData data) -> void {
    TRY(all_renderers.get(rIdx))->qualityUpdate(data);
}

fn UpdateDispatcher::xRayUpdate(RendererIndex rIdx, XRayData data) -> void {
    TRY(all_renderers.get(rIdx))->xRayUpdate(data);
}

fn UpdateDispatcher::volumeUpdate(RendererIndex rIdx, VolumeData data) -> void {
    TRY(all_renderers.get(rIdx))->volumeUpdate(data);
}

fn UpdateDispatcher::sliceUpdate(RendererIndex rIdx, SliceData data) -> void {
    TRY(all_renderers.get(rIdx))->sliceUpdate(data);
}

fn UpdateDispatcher::lightUpdate(RendererIndex rIdx, LightData data) -> void {
    TRY(all_renderers.get(rIdx))->lightUpdate(data);
}

fn UpdateDispatcher::deleteTextures() -> void {
    all_renderers.deleteTextures();
}
