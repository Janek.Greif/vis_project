//
// Created by janek on 25.02.22.
//

#ifndef VIS_PROJECT_PROPERTY_LINK_H
#define VIS_PROJECT_PROPERTY_LINK_H

#include <memory>

#include <QMatrix4x4>
#include <QDebug>

#include "RendererIndex.h"
#include "property_link_count.h"
#include "../ui/render_settings_event.h"
#include "../oxidize.h"

enum class PropertyLinkType : u8 {
    CAMERA_POS = 0,
    CAMERA_ZOOM = 1,
    TRANSFER_FUNCTION = 2,
    SLICE_POSITIONS = 3,
    LIGHT = 4,
};

inline QDebug& operator<<(QDebug& debug, const PropertyLinkType& type) {
    QDebugStateSaver saver{debug};
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch (type) {
        case PropertyLinkType::CAMERA_POS:
            debug.nospace() << "CAMERA_POS";
            break;
        case PropertyLinkType::CAMERA_ZOOM:
            debug.nospace() << "CAMERA_ZOOM";
            break;
        case PropertyLinkType::TRANSFER_FUNCTION:
            debug.nospace() << "TRANSFER_FUNCTION";
            break;
        case PropertyLinkType::SLICE_POSITIONS:
            debug.nospace() << "SLICE_POSITIONS";
            break;
        case PropertyLinkType::LIGHT:
            debug.nospace() << "LIGHT";
            break;
    }
    return debug;
}

constexpr inline fn humanName(PropertyLinkType type) -> const char* {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing other cases");
    switch (type) {
        case PropertyLinkType::CAMERA_POS:
            return "Camera Position";
        case PropertyLinkType::CAMERA_ZOOM:
            return "Camera Zoom&Pan";
        case PropertyLinkType::TRANSFER_FUNCTION:
            return "Transfer Function";
        case PropertyLinkType::SLICE_POSITIONS:
            return "Slice Positions";
        case PropertyLinkType::LIGHT:
            return "Light Properties";
    }
    return nullptr;
}

template<typename T>
concept Property = std::is_default_constructible_v<T> &&
        requires{ {T::TOKEN_TYPE} -> std::same_as<const PropertyLinkType&>; };

struct CameraPosition {
    static const PropertyLinkType TOKEN_TYPE{ PropertyLinkType::CAMERA_POS };

    inline CameraPosition() : data{} {
        data.translate(0.0, 0.0, -2.0f*std::sqrt(3.0f));
    };

    QMatrix4x4 data;
};

struct CameraZoom {
    static const PropertyLinkType TOKEN_TYPE{ PropertyLinkType::CAMERA_ZOOM };

    inline explicit CameraZoom() : zoomValue{1.0}, zoom{}, panning{}, panValue{} { };

    f32 zoomValue;
    QVector3D panValue;
    QMatrix4x4 zoom;
    QMatrix4x4 panning;
};

struct TransFn {
    static const PropertyLinkType TOKEN_TYPE{ PropertyLinkType::TRANSFER_FUNCTION };

    inline TransFn() : transfer_fn_idx{} {};

    explicit inline TransFn(TransFnIndex transferFnIdx) : transfer_fn_idx{transferFnIdx} {}

    TransFnIndex transfer_fn_idx;
};

struct SlicePositions {
    static const PropertyLinkType TOKEN_TYPE{ PropertyLinkType::SLICE_POSITIONS };

    inline SlicePositions() :
        data{
            .show_plane_x = true,
            .show_plane_y = true,
            .show_plane_z = true,
        },
        center{0.0, 0.0, 0.0},
        globalCenter{0.0, 0.0, 0.0},
        scale{1.0, 1.0, 1.0},
        normal1{1.0, 0.0, 0.0},
        normal2{0.0, 1.0, 0.0},
        normal3{0.0, 0.0, 1.0},
        rotateMatrix{},
        translateMatrix{},
        scaleMatrix{},
        globalTranslationMatrix{},
        focus{255},
        shared{false}
    {}

    inline fn getNormal(usize i) -> QVector3D& {
        switch(i) {
            case 0:
                return normal1;
            case 1:
                return normal2;
            case 2:
                return normal3;
        }
        qFatal("normal out of range");
    }

    inline fn getNormal(usize i) const -> const QVector3D& {
        switch(i) {
            case 0:
                return normal1;
            case 1:
                return normal2;
            case 2:
                return normal3;
        }
        qFatal("normal out of range");
    }

    SliceData data;
    QVector3D center;
    QVector3D globalCenter;
    QVector3D scale;
    QVector3D normal1;
    QVector3D normal2;
    QVector3D normal3;
    QMatrix4x4 rotateMatrix;
    QMatrix4x4 translateMatrix;
    QMatrix4x4 scaleMatrix;
    QMatrix4x4 globalTranslationMatrix;
    u8 focus;
    bool shared;
};

struct Light {
    static const PropertyLinkType TOKEN_TYPE{ PropertyLinkType::LIGHT };

    inline Light() :
        lightPos{},
        data{
            .status = false,
            .power = 5.0,
            .ambient = 0.5,
            .diffuse = 0.7,
            .specular = 1.0,
            .shininess = 30.0,
        },
        focus{255}
    { };

    QMatrix4x4 lightPos;
    LightData data;
    u8 focus;
};

using CameraPositionProperty = std::shared_ptr<CameraPosition>;
using CameraZoomProperty = std::shared_ptr<CameraZoom>;
using TransFnProperty = std::shared_ptr<TransFn>;
using SlicePositionsProperty = std::shared_ptr<SlicePositions>;
using LightProperty = std::shared_ptr<Light>;

using LinkProperty = std::variant<CameraPositionProperty, CameraZoomProperty, TransFnProperty, SlicePositionsProperty, LightProperty>;


#endif //VIS_PROJECT_PROPERTY_LINK_H
