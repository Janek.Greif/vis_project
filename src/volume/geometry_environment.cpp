//
// Created by janek on 05.03.22.
//

#include "geometry_environment.h"

GeometryEnvironment::GeometryEnvironment() :
    cube{ CUBE_VERTEX_DATA, CUBE_INDEX_DATA, CUBE_LINE_INDEX_DATA },
    quad{ QUAD_VERTEX_DATA, QUAD_INDEX_DATA, QUAD_LINE_INDEX_DATA }
{
}

fn GeometryEnvironment::create() -> void {
    cube.create();
    quad.create();
}
