//
// Created by janek on 26.04.22.
//

#ifndef VIS_PROJECT_PLANE_MATH_H
#define VIS_PROJECT_PLANE_MATH_H

#include <tuple>
#include <array>

#include <QVector3D>
#include "../oxidize.h"

struct IntersectionPolygon {
    IntersectionPolygon(QVector3D normal, QVector3D center);
    fn as_span() -> std::span<QVector3D>;
    fn as_span() const -> std::span<const QVector3D>;

    fn data() -> void*;
    fn data() const -> const void*;
    fn count() const -> i32;
private:
    std::array<QVector3D, 6> points;
    u8 point_count;
};

fn intersectPlaneWithBox(QVector3D normal, QVector3D center) -> std::pair<std::array<QVector3D, 6>, u8>;

#endif //VIS_PROJECT_PLANE_MATH_H
