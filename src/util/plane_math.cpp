//
// Created by janek on 26.04.22.
//

#include "plane_math.h"

struct CartesianPlane {
    CartesianPlane(QVector3D normal, QVector3D center) :
        a{ normal.x() },
        b{ normal.y() },
        c{ normal.z() },
        d{ QVector3D::dotProduct(normal, center) }
    {};
    f32 a;
    f32 b;
    f32 c;
    f32 d;
};

fn rayToPlane(
        QVector3D rayOrig,
        QVector3D rayDir,
        CartesianPlane plane
) -> Option<std::pair<f32, f32>> /* t, vd */ {
    let outVD{ plane.a * rayDir.x() + plane.b * rayDir.y() + plane.c * rayDir.z() };
    if (outVD == 0.0f) {
        return None;
    }
    let outT = - (plane.a * rayOrig.x() + plane.b * rayOrig.y() + plane.c * rayOrig.z() + plane.d) / outVD;
    return std::make_pair(outT, outVD);
}

IntersectionPolygon::IntersectionPolygon(QVector3D normal, QVector3D center) {

    QVector3D aabbMin{-1.0, -1.0, -1.0};
    QVector3D aabbMax{1.0, 1.0, 1.0};

    CartesianPlane plane{normal, center};

    point_count = 0;

    // Test edges along X axis, pointing right.
    QVector3D dir{ aabbMax.x() - aabbMin.x(), 0.f, 0.f };
    QVector3D orig = aabbMin;
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMin.x(), aabbMax.y(), aabbMin.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMin.x(), aabbMin.y(), aabbMax.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMin.x(), aabbMax.y(), aabbMax.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;

    // Test edges along Y axis, pointing up.
    dir = QVector3D{0.f, aabbMax.y() - aabbMin.y(), 0.f};
    orig = QVector3D{aabbMin.x(), aabbMin.y(), aabbMin.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMax.x(), aabbMin.y(), aabbMin.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMin.x(), aabbMin.y(), aabbMax.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMax.x(), aabbMin.y(), aabbMax.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;

    // Test edges along Z axis, pointing forward.
    dir = QVector3D{0.f, 0.f, aabbMax.z() - aabbMin.z()};
    orig = QVector3D{aabbMin.x(), aabbMin.y(), aabbMin.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMax.x(), aabbMin.y(), aabbMin.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMin.x(), aabbMax.y(), aabbMin.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;
    orig = QVector3D{aabbMax.x(), aabbMax.y(), aabbMin.z()};
    if (let tvd{ rayToPlane(orig, dir, plane) }; tvd && tvd->first >= 0.f && tvd->first <= 1.f)
        points[point_count++] = orig + dir * tvd->first;

    let origin{ points[0] };
    let s{ as_span() };

    std::sort(s.begin(),  s.end(), [origin, normal](const QVector3D& lhs, const QVector3D& rhs){
        let v{ QVector3D::crossProduct(lhs - origin, rhs - origin) };
        return QVector3D::dotProduct(v, normal) < 0.0;
    });
}

fn IntersectionPolygon::as_span() -> std::span<QVector3D> {
    return {points.begin(), points.begin() + point_count};
}

fn IntersectionPolygon::as_span() const -> std::span<const QVector3D> {
    return {points.begin(), points.begin() + point_count};
}

fn IntersectionPolygon::data() -> void* {
    return points.data();
}

fn IntersectionPolygon::data() const -> const void* {
    return points.data();
}

fn IntersectionPolygon::count() const -> i32 {
    return static_cast<i32>(point_count);
}