//
// Created by janek on 05.02.22.
//

#ifndef VIS_PROJECT_MOVECELL_H
#define VIS_PROJECT_MOVECELL_H

#include <memory>
#include <mutex>
#include <utility>
#include <optional>
#include <QDebug>
#include "../oxidize.h"

template<typename T>
struct MutexPair {
    explicit MutexPair(T&& t) :
        t{std::make_optional(std::move(t))},
        m{std::mutex{}}
    {}
    Option<T> t;
    std::mutex m;
};

/// This is to wrap move-only types to send them across a signal.
/// This type can be copied on demand, but only one copy can successfully take the value out
/// This class is thread safe
template<typename T>
class MoveCell {
public:
    explicit MoveCell(T&& t) :
        cell{ std::make_shared<MutexPair<T>>(std::move(t)) }
    {}

    fn take() -> Option<T> {
        std::lock_guard<std::mutex> guard(cell->m);
        Option<T> i{ std::nullopt };
        cell->t.swap(i);
        return i;
    }
private:
    std::shared_ptr<MutexPair<T>> cell;
};

#endif //VIS_PROJECT_MOVECELL_H
