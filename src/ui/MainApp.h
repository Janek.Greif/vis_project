//
// Created by janek on 04.02.22.
//

#ifndef VIS_PROJECT_MAINAPP_H
#define VIS_PROJECT_MAINAPP_H

#include <QMainWindow>
#include <QFutureWatcher>
#include "VolumeWidget.h"
#include "ViewArea.h"

#include "../util/MoveCell.h"
#include "../volume/dat_reader.h"
#include "../oxidize.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainApp; }
QT_END_NAMESPACE

class MainApp : public QMainWindow {
Q_OBJECT

public:
    explicit MainApp(Option<QString> fileToLoad, QWidget *parent = nullptr);

    ~MainApp() override;

public slots:
    fn loadDat() -> void;

signals:
    fn datLoaded(MoveCell<Volume> volume) -> void;
    fn closeRequest() -> void;

protected:
    fn closeEvent(QCloseEvent* event) -> void override ;

private slots:
    fn sideCardReady() -> void;
    fn viewAreaReady() -> void;
    fn volumeLoaded() -> void;

private:
    fn loadInitialFile() -> void;

private:
    ViewArea* view_area;

    Ui::MainApp *ui;

    Option<QString> file_to_load;
    bool side_card_ready;
    bool view_area_ready;

    QFutureWatcher<Result<MoveCell<Volume>, ReadVolumeDatError>>* volume_reading_future;
};


#endif //VIS_PROJECT_MAINAPP_H
