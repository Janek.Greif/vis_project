//
// Created by janek on 17.04.22.
//

#ifndef VIS_PROJECT_LINKTOKENWIDGET_H
#define VIS_PROJECT_LINKTOKENWIDGET_H

#include <QWidget>
#include <QLabel>

#include "../volume/LinkToken.h"
#include "../oxidize.h"

struct ColorIndex {
private:
    ColorIndex(u8 index, u8 generation);
    ColorIndex();

    u8 index;
    u8 generation;
    friend class ColorStore;
    friend class LinkTokenWidgetSerde;
public:
    bool operator==(const ColorIndex& other) const = default;

    friend QDataStream& operator<< (QDataStream& stream, const ColorIndex& cIdx);
    friend QDataStream& operator>> (QDataStream& stream, ColorIndex& cIdx);
};

QDataStream& operator<< (QDataStream& stream, const ColorIndex& cIdx);
QDataStream& operator>> (QDataStream& stream, ColorIndex& cIdx);


class ColorStore : public QObject {
Q_OBJECT
public:
    ColorStore();

    fn isValid(ColorIndex cIdx) -> bool;
    fn getColor(ColorIndex cIdx) -> Option<QColor>;

private:
    fn requestNew() -> ColorIndex;
    fn setColor(ColorIndex cIdx, QColor c) -> void;

    fn openColorDialog(ColorIndex cIdx) -> void;

    fn getColorFromIdx(ColorIndex cIdx) -> QColor*;

    fn colorSelected(ColorIndex cIdx, QColor newColor) -> void;
    fn colorPreviewSelected(ColorIndex cIdx, QColor previewColor) -> void;
    fn colorRestore(ColorIndex cIdx) -> void;

signals:
    fn colorChanged(ColorIndex cIdx, QColor c) -> void;

private:

    struct ColorEntry {
        u8 generation;
        bool free;
    };

    std::vector<QColor> colors;
    std::vector<ColorEntry> color_entries;

    friend class LinkTokenWidget;
};

struct LinkTokenWidgetSerde {
    LinkTokenWidgetSerde(LinkToken token, ColorIndex cIdx);
    LinkTokenWidgetSerde();

    LinkToken token;
    ColorIndex c_idx;
};

QDataStream& operator<< (QDataStream& stream, const LinkTokenWidgetSerde& ltws);
QDataStream& operator>> (QDataStream& stream, LinkTokenWidgetSerde& ltws);

class LinkTokenWidget : public QWidget {
    Q_OBJECT
public:
    /// All LinkTokenWidgets, that share the same LinkToken, crate a group that also shares the color.
    /// To create another element in that group, use the clone method. It will share the LinkToken and also handle
    /// color changes for all grouped LinkTokenWidgets.
    ///
    /// This constructor creates a not only a new LinkTokenWidget but also a new group.
    explicit LinkTokenWidget(std::shared_ptr<ColorStore> colorStore, LinkToken token, Option<QColor> = None, bool deletable = false, QWidget* parent = nullptr);

    /// All LinkTokenWidgets that share the same LinkToken crate a group that also shares the color.
    ///
    /// This constructor creates a new LinkTokenWidget with the group given by the LinkToken. The ColorIndex has to match
    /// the group.
    LinkTokenWidget(ColorIndex cIdx, std::shared_ptr<ColorStore> colorStore, LinkToken token, Option<QColor> = None, bool deletable = false, QWidget* parent = nullptr);

    /// All LinkTokenWidgets that share the same LinkToken crate a group that also shares the color.
    /// This method creates another element in this group. It will share the LinkToken and also handle
    /// color changes for all grouped LinkTokenWidgets.
    /// clones are not deletable (the don't open the context-menu)
    fn clone(QWidget* parent = nullptr) -> LinkTokenWidget*;

    fn serdeRepresentation() -> LinkTokenWidgetSerde;
    fn pixmap() -> QPixmap;
    fn tokenType() const -> PropertyLinkType;

public slots:
    fn removeQueryResult(QPoint pos, bool removable, LinkToken token) -> void;

signals:
    fn dragStart(LinkTokenWidget* origin) -> void;
    fn queryRemovable(QPoint pos, LinkToken token) -> void;
    fn removeToken(LinkTokenWidget* self, LinkToken token) -> void;

private slots:
    fn applyColor(ColorIndex cIdx, QColor c) -> void;

protected:
    fn mouseReleaseEvent(QMouseEvent* event) -> void override;
    fn mousePressEvent(QMouseEvent *event) -> void override;

private:
    LinkToken token;
    std::shared_ptr<ColorStore> color_store;
    ColorIndex c_idx;
    QLabel* icon;
    bool deletable;
};


#endif //VIS_PROJECT_LINKTOKENWIDGET_H
