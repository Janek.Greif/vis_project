//
// Created by janek on 07.05.22.
//

#include "render_settings_event.h"

const Mode Mode::AVERAGE{true};
const Mode Mode::MAXIMUM{false};

const Projection Projection::PERSPECTIVE{true};
const Projection Projection::ORTHOGRAPHIC{false};

