//
// Created by janek on 08.03.22.
//

#ifndef VIS_PROJECT_SIDECARD_H
#define VIS_PROJECT_SIDECARD_H

#include <memory>

#include <QDockWidget>
#include <QPushButton>
#include "TransferFnWidget.h"
#include "render_settings_event.h"
#include "../volume/VolumeTexture.h"
#include "../volume/geometry_environment.h"
#include "../volume/ShaderEnvironment.h"
#include "../volume/TransferFunction.h"
#include "../volume/UpdateDispatcher.h"
#include "../volume/SharedProperties.h"
#include "../oxidize.h"

class SideCard : public QWidget {
    Q_OBJECT
public:
    SideCard(std::shared_ptr<UpdateDispatcher> dispatcher, std::shared_ptr<SharedProperties> properties, std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<ShaderEnvironment> shaderEnv, std::shared_ptr<VolumeTexture> volume, QWidget* parent = nullptr);

    fn renderSettings() -> RenderSettings*;

public slots:
    fn addedSegment() -> void;
    fn addSegment() -> void;

private slots:
    fn init() -> void;


signals:
    fn renderTransFn() -> void;
    fn ready() -> void;
    fn setTransferFunction(TransFnIndex transFn, Option<LinkToken> tFnLinkToken) -> void;

    fn showVolumeInfo(QString name, u32 x, u32 y, u32 z) -> void;
    fn updateSettings(RendererIndex idx, SettingsUpdateSignal sig) -> void;
    fn updateQuality(RendererIndex idx, GeneralData event) -> void;
    fn updateXray(RendererIndex idx, XRayData event) -> void;
    fn updateVolume(RendererIndex idx, VolumeData event) -> void;
    fn updateSlice(RendererIndex idx, SliceData event) -> void;
    fn updateLight(RendererIndex idx, LightData event) -> void;

private:
    std::shared_ptr<VolumeTexture> volume_tex;
    std::shared_ptr<GeometryEnvironment> geo_env;
    std::shared_ptr<ShaderEnvironment> shader_env;
    std::shared_ptr<UpdateDispatcher> dispatcher;
    std::shared_ptr<SharedProperties> properties;

    TransferFnWidget* transfer_fn_widget;
    RenderSettings* render_settings;

    QPushButton* add_segment_btn;
};


#endif //VIS_PROJECT_SIDECARD_H
