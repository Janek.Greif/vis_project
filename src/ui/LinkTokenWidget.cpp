//
// Created by janek on 17.04.22.
//

#include <QMenu>
#include <QMouseEvent>
#include <QColorDialog>
#include <QPainter>
#include "../volume/property_link_count.h"
#include "../volume/property_link.h"
#include "LinkTokenWidget.h"

fn resourceFromLinkToken(LinkToken token) -> const char* {
    static_assert(PROPERTY_LINK_COUNT == 5, "missing switch case branches.");
    switch (token.linkType()) {
        case PropertyLinkType::CAMERA_POS:
            return "icons:/camera_pos_link_token.png";
        case PropertyLinkType::CAMERA_ZOOM:
            return "icons:/camera_zoom_link_token.png";
        case PropertyLinkType::TRANSFER_FUNCTION:
            return "icons:/transfer_function_link_token.png";
        case PropertyLinkType::SLICE_POSITIONS:
            return "icons:/slice_positions_link_token.png";
        case PropertyLinkType::LIGHT:
            return "icons:/light_link_token.png";
        default:
            qFatal("missing switch case branches.");
    }
}

ColorIndex::ColorIndex(u8 index, u8 generation) : index{index}, generation{generation} {
}

ColorIndex::ColorIndex() : index{255}, generation{255} {

}

QDataStream& operator<<(QDataStream& stream, const ColorIndex& cIdx) {
    stream << cIdx.index;
    stream << cIdx.generation;
    return stream;
}

QDataStream& operator>>(QDataStream& stream, ColorIndex& cIdx) {
    stream >> cIdx.index;
    stream >> cIdx.generation;
    return stream;
}

LinkTokenWidgetSerde::LinkTokenWidgetSerde(LinkToken token, ColorIndex cIdx) :
        token{token}, c_idx{cIdx}
{ }

LinkTokenWidgetSerde::LinkTokenWidgetSerde() :
        token{}, c_idx{}
{ }

QDataStream& operator<<(QDataStream& stream, const LinkTokenWidgetSerde& ltws) {
    stream << ltws.token;
    stream << ltws.c_idx;
    return stream;
}

QDataStream& operator>>(QDataStream& stream, LinkTokenWidgetSerde& ltws) {
    stream >> ltws.token;
    stream >> ltws.c_idx;
    return stream;
}

LinkTokenWidget::LinkTokenWidget(
        std::shared_ptr<ColorStore> colorStore,
		LinkToken token,
        Option<QColor> c,
		bool deletable,
		QWidget* parent
) :
    LinkTokenWidget{colorStore->requestNew(), colorStore, token, c, deletable, parent}
{
}

LinkTokenWidget::LinkTokenWidget(
        ColorIndex cIdx,
		std::shared_ptr<ColorStore> colorStore,
		LinkToken token,
        Option<QColor> c,
		bool deletable,
		QWidget* parent
) :
    QWidget{parent},
    token{token},
    color_store{std::move(colorStore)},
    c_idx{cIdx},
    deletable{deletable}
{
    setFixedSize(25, 25);
    QPalette p{};
    if (c) {
        p.setColor(QPalette::Window, *c);
        color_store->setColor(c_idx, *c);
    } else if (let color{ color_store->getColor(c_idx) }; color) {
        p.setColor(QPalette::Window, *color);
    } else {
        qWarning() << "could not read color information for LinkTokenWidget, defaulting to transparent";
        p.setColor(QPalette::Window, QColor{0, 0, 0, 0});
    }
    connect(&*color_store, &ColorStore::colorChanged, this, &LinkTokenWidget::applyColor);

    icon = new QLabel{this};
    icon->setAutoFillBackground(true);
    icon->setPixmap(QPixmap{resourceFromLinkToken(token)});
    icon->setAttribute(Qt::WA_DeleteOnClose);
    icon->show();
    icon->setContentsMargins(0, 0, 0, 0);
    icon->setPalette(p);
    setContentsMargins(0, 0, 0, 0);
    setToolTip(humanName(token.linkType()));
}

fn LinkTokenWidget::mouseReleaseEvent(QMouseEvent* event) -> void {
    if (event->button() == Qt::RightButton) {
        if (deletable) {
            emit queryRemovable(event->globalPosition().toPoint(), token);
        } else {
            color_store->openColorDialog(c_idx);
        }
    } else {
        QWidget::mouseReleaseEvent(event);
    }
}

fn LinkTokenWidget::removeQueryResult(QPoint pos, bool removable, LinkToken t) -> void {
    if (token == t) {
        QMenu contextMenu{};
        contextMenu.addAction("change color");
        let removeAction{ new QAction{} };
        removeAction->setText("remove");
        removeAction->setEnabled(removable);
        contextMenu.addAction(removeAction);


        if (let action{ contextMenu.exec(pos) }; action) {
            if (action->text() == "change color") {
                color_store->openColorDialog(c_idx);
            } else if (action->text() == "remove") {
                emit removeToken(this, token);
            }
        }
    }
}

fn LinkTokenWidget::applyColor(ColorIndex cIdx, QColor c) -> void {
    if (cIdx == c_idx) {
        QPalette p{};
        p.setColor(QPalette::Window, c);
        icon->setPalette(p);
    }
}

fn LinkTokenWidget::clone(QWidget* parent) -> LinkTokenWidget* {
    return new LinkTokenWidget{c_idx, color_store, token, None, false, parent};
}

fn LinkTokenWidget::mousePressEvent(QMouseEvent* event) -> void {
    if (event->button() == Qt::LeftButton) {
        emit dragStart(this);
    }
}

fn LinkTokenWidget::serdeRepresentation() -> LinkTokenWidgetSerde {
    return LinkTokenWidgetSerde{token, c_idx};
}

fn LinkTokenWidget::pixmap() -> QPixmap {
    QColor c{};
    if (let color{ color_store->getColor(c_idx) }; color) {
        c = *color;
    } else {
        qWarning() << "could not read color information for LinkTokenWidget, defaulting to transparent";
        c = QColor{0, 0, 0, 0};
    }
    QPainter painter{};
    QPixmap tmpMap{ 25, 25 };
    painter.begin(&tmpMap);
    painter.fillRect(tmpMap.rect(), c);
    painter.drawPixmap(0, 0, QPixmap{resourceFromLinkToken(token)});
    painter.end();
    return tmpMap;
}

fn LinkTokenWidget::tokenType() const -> PropertyLinkType {
    return token.linkType();
}

fn ColorStore::requestNew() -> ColorIndex {
    for (auto[i, entry] : color_entries | enumerate | FILTER(it.second.free)) {
        entry.free = false;
        colors[i] = QColor{0, 0, 0, 0};
        return ColorIndex{ static_cast<u8>(i), entry.generation };
    }
    ColorIndex newCIndex{ static_cast<u8>(color_entries.size()), 0 };
    color_entries.push_back({ newCIndex.generation, false });
    colors.emplace_back();
    colors.back() = QColor{0, 0, 0, 0};
    return newCIndex;
}


fn ColorStore::colorSelected(ColorIndex cIdx, QColor newColor) -> void {
    if (let color{getColorFromIdx(cIdx) }; color) {
        *color = newColor;
        emit colorChanged(cIdx, newColor);
    }
}

fn ColorStore::colorPreviewSelected(ColorIndex cIdx, QColor previewColor) -> void {
    emit colorChanged(cIdx, previewColor);
}

fn ColorStore::colorRestore(ColorIndex cIdx) -> void {
    if (let color{getColorFromIdx(cIdx) }; color) {
        emit colorChanged(cIdx, *color);
    }
}

ColorStore::ColorStore() :
    QObject{nullptr},
    colors{},
    color_entries{}
{ }

fn ColorStore::isValid(ColorIndex cIdx) -> bool {
    return getColorFromIdx(cIdx) != nullptr;
}

fn ColorStore::setColor(ColorIndex cIdx, QColor c) -> void {
    if (let color{getColorFromIdx(cIdx) }; color) {
        *color = c;
        emit colorChanged(cIdx, c);
    }
}

fn ColorStore::getColor(ColorIndex cIdx) -> Option<QColor> {
    if (let color{getColorFromIdx(cIdx) }; color) {
        return *color;
    } else {
        return None;
    }
}

fn ColorStore::getColorFromIdx(ColorIndex cIdx) -> QColor* {
    if (cIdx.index < color_entries.size() && !color_entries[cIdx.index].free && color_entries[cIdx.index].generation == cIdx.generation) {
        return &colors[cIdx.index];
    } else {
        return nullptr;
    }
}

fn ColorStore::openColorDialog(ColorIndex cIdx) -> void {
    if (let color{getColorFromIdx(cIdx) }; color) {
        let colorDialog{ new QColorDialog{*color}};
        connect(colorDialog, &QColorDialog::colorSelected      , [this, cIdx](QColor newColor    ){ colorSelected(cIdx, newColor);            });
        connect(colorDialog, &QColorDialog::currentColorChanged, [this, cIdx](QColor previewColor){ colorPreviewSelected(cIdx, previewColor); });
        connect(colorDialog, &QColorDialog::rejected           , [this, cIdx](                   ){ colorRestore(cIdx);                       });
        colorDialog->open();
    }
}
