//
// Created by janek on 08.03.22.
//

#include <span>
#include <array>

#include <QPainter>
#include <QColorDialog>
#include "TransferFnWidget.h"

TransferFnWidget::TransferFnWidget(std::shared_ptr<UpdateDispatcher> dispatcher, std::shared_ptr<SharedProperties> properties, QWidget* parent, Qt::WindowFlags f) :
    QOpenGLWidget{parent, f},
    trans_fn{nullptr},
    dispatcher{std::move(dispatcher)},
    properties{std::move(properties)}
{
    setMouseTracking(true);
}

fn TransferFnWidget::setTransferFunction(TransFnIndex transFn, Option<LinkToken> tFnLinkToken) -> void {
    trans_fn = dispatcher->getTransFn(transFn);
    trans_fn_index = transFn;
    trans_fn_link_token = tFnLinkToken;
    if (trans_fn) {
        trans_fn->resize(width(), height());
        if (trans_fn->segmentToAdd()) {
            emit segmentsEmpty();
        } else {
            emit addedSegment();
        }
    }

    update2();
}

fn TransferFnWidget::initializeGL() -> void {
    initializeOpenGLFunctions();
}

fn TransferFnWidget::resizeGL(i32 w, i32 h) -> void {
    TRY(trans_fn)->resize(w, h);
}

fn TransferFnWidget::paintGL() -> void {
    if (trans_fn) {
        QPainter p{this};
        p.setRenderHint(QPainter::Antialiasing);
        p.setCompositionMode(QPainter::CompositionMode_Source);
        p.beginNativePainting();
        trans_fn->renderHistogram();
        p.endNativePainting();
        trans_fn->renderCurves(p);
    }
}

fn TransferFnWidget::update2() -> void{
    update();
}

fn TransferFnWidget::mousePressEvent(QMouseEvent* event) -> void {
    if (trans_fn) {
        let action { trans_fn->mousePress(event) };
        if (action == MousePressAction::Redraw) {
            update2();
        }
        if (action == MousePressAction::OpenColor) {
            let colorDialog{ new QColorDialog{QColor{0, 0, 0}}};
            connect(colorDialog, &QColorDialog::colorSelected, this, &TransferFnWidget::colorSelected);
            colorDialog->open();
        }
        if (action == MousePressAction::AddedSegment) {
            emit addedSegment();
        }
        if (action == MousePressAction::SegmentsEmpty) {
            emit segmentsEmpty();
        }
    }
}

fn TransferFnWidget::colorSelected(const QColor& color) -> void {
    if (trans_fn && trans_fn->colorSelected(color)) {
        update2();
    }
}

fn TransferFnWidget::mouseReleaseEvent(QMouseEvent* event) -> void {
    if (trans_fn && trans_fn->mouseRelease(event)) {
        update2();
    }
}

fn TransferFnWidget::mouseMoveEvent(QMouseEvent* event) -> void {
    if (trans_fn && trans_fn->mouseMove(event)) {
        update2();
    }
}

fn TransferFnWidget::addSegment() -> void {
    TRY(trans_fn)->addSegment();
    else emit addedSegment();
}

fn TransferFnWidget::sampleTransFn() -> void {
    if (trans_fn) {
        dispatcher->sampleTransFn(trans_fn_index);

        if (let data_renderer{ trans_fn_index.convertToCorrespondingRendererIndex() }; data_renderer) {
            // if this succeeds, the transfer-function shown is basic-transfer-function and more importantly not shared.
            std::array<RendererIndex, 1> dr{ *data_renderer };
            dispatcher->markForRedraw(std::span<RendererIndex>{ dr });
            dispatcher->updateAll();
        } else {
            // it is shared, and the link_token has to exist
            assert(trans_fn_link_token);
            if (let linkedRenderers{ properties->getLinkedRenderers(*trans_fn_link_token) }; linkedRenderers.has_value() && !(*linkedRenderers)->empty()) {
                dispatcher->markForRedraw(**linkedRenderers);
                dispatcher->updateAll();
            }
        }
    }
}

fn TransferFnWidget::enterEvent(QEnterEvent* event) -> void {
    if (trans_fn && trans_fn->mouseEnter(event)) {
        update2();
    }
}

fn TransferFnWidget::leaveEvent(QEvent* event) -> void {
    if (trans_fn && trans_fn->mouseLeave(event)) {
        update2();
    }
}
