//
// Created by janek on 12.02.22.
//

#include <random>
#include <QVBoxLayout>
#include <QPushButton>

#include "ViewArea.h"
#include "InstanceSplitter.h"
#include "VolumeWidget.h"

ViewArea::ViewArea(
        std::shared_ptr<UpdateDispatcher> dispatcher,
		std::shared_ptr<SharedProperties> properties,
		std::shared_ptr<ColorStore> colorStore,
        RenderSettings* renderSettings,
		QWidget* parent
) :
    InstanceSplitter{{
        .parent = parent,
        .orientation = Qt::Orientation::Horizontal,
        .maximum = 3
    }},
    dispatcher{std::move(dispatcher)},
    properties{std::move(properties)},
    color_store{std::move(colorStore)},
    render_settings{renderSettings},
    initial_widget{nullptr},
    need_initial_widget{false}
{
    connect(this, &InstanceSplitter::contentEmpty, this, &ViewArea::addSingleElement);

    transfer_function_group = new QButtonGroup{this};

    setAutoFillBackground(true);
    QPalette p{};
    p.setColor(QPalette::Window, QColor{170, 170, 127});
    setPalette(p);
    setHandleWidth(10);
}

fn ViewArea::newElement() -> InstanceElement* {
    return new VerticalElement{dispatcher, properties, color_store, this, render_settings};
}

fn ViewArea::addSingleElement() -> void {
    let elem{ newElement() };
    instantiateElement(elem);
    insertWidget(1, elem);
}

fn ViewArea::setInitialWidget(VolumeWidget* widget) -> void{
    if (!initial_widget) {
        initial_widget = widget;
    }
}

fn ViewArea::initialWidget() -> VolumeWidget* {
    TRY(initial_widget)->show();
    return initial_widget;
}

fn ViewArea::takeNeedInitialWidget() -> bool {
    let res{ need_initial_widget };
    need_initial_widget = false;
    return res;
}

fn ViewArea::saveVolumeWidget(VolumeWidget *volWid) -> void {
    save_layout->addWidget(volWid);
    need_initial_widget = true;
    volWid->hide();
}

fn ViewArea::volumeWidgetReady() -> void {
    emit ready();
}

fn ViewArea::setSaveLayout(QVBoxLayout* saveLayout) -> void {
    save_layout = saveLayout;
}

fn ViewArea::transferFunctionGroup() -> QButtonGroup* {
    return transfer_function_group;
}

VerticalElement::VerticalElement(
        std::shared_ptr<UpdateDispatcher> dispatcher,
	    std::shared_ptr<SharedProperties> properties,
	    std::shared_ptr<ColorStore> colorStore,
	    ViewArea* area,
        RenderSettings* renderSettings
) :
    InstanceElement{area},
    dispatcher{std::move(dispatcher)},
    properties{std::move(properties)},
    color_store{std::move(colorStore)},
    area{area},
    render_settings{renderSettings}
{
    setContentsMargins(0, 0, 0, 0);
    setAutoFillBackground(true);
    QPalette p{};
    p.setColor(QPalette::Window, QColor{127, 170, 170});
    setPalette(p);
}

fn VerticalElement::instantiate() -> void {
    let layout{ new QVBoxLayout{ this } };
    layout->setContentsMargins(0, 0, 0, 0);
    let newArea{ new VerticalArea{ dispatcher, properties, color_store, area, render_settings, this} };
    connect(newArea, &InstanceSplitter::contentEmpty, this, &VerticalElement::remove);
    layout->addWidget(newArea);
    setLayout(layout);
}

VerticalElement::~VerticalElement() {
//    qDebug() << "delete vertical element";
}

VerticalArea::VerticalArea(
        std::shared_ptr<UpdateDispatcher> dispatcher,
		std::shared_ptr<SharedProperties> properties,
		std::shared_ptr<ColorStore> colorStore,
		ViewArea* area,
        RenderSettings* renderSettings,
		QWidget *parent
) :
    InstanceSplitter{{ .parent = parent, .maximum = 2}},
    dispatcher{std::move(dispatcher)},
    properties{std::move(properties)},
    color_store{std::move(colorStore)},
    area{area},
    render_settings{renderSettings}
{
    setContentsMargins(0, 0, 0, 0);
    setHandleWidth(10);
}


fn VerticalArea::newElement() -> InstanceElement* {
    let volumeElement{ new VolumeElement{dispatcher, properties, color_store, area, render_settings, this} };
    connect(this, &InstanceSplitter::contentPartial, volumeElement, &VolumeElement::putHeaderInPlace);
    connect(this, &InstanceSplitter::contentFull, volumeElement, &VolumeElement::putHeaderInPlace);
    return volumeElement;
}

VolumeElement::VolumeElement(
        std::shared_ptr<UpdateDispatcher> dispatcher,
		std::shared_ptr<SharedProperties> properties,
		std::shared_ptr<ColorStore> colorStore,
		ViewArea* area,
        RenderSettings* renderSettings,
		QWidget* parent
) :
    InstanceElement{ parent },
    dispatcher{std::move(dispatcher)},
    properties{std::move(properties)},
    color_store{std::move(colorStore)},
    area{area},
    render_settings{renderSettings},
    volume_header{nullptr}
{
    setAutoFillBackground(true);
    setContentsMargins(0, 0, 0, 0);
}

fn VolumeElement::putHeaderInPlace() -> void {
    let index{ indexInSplitter() };
    if (let l{ qobject_cast<QVBoxLayout*>(layout())}; l) {
        if (index == 0) {
            if (l->indexOf(volume_header) != 0) {
                l->removeWidget(volume_header);
                l->insertWidget(0, volume_header);
            }
        } else {
            if (l->indexOf(volume_header) == 0) {
                l->removeWidget(volume_header);
                l->addWidget(volume_header);
            }
        }
    }
}

fn VolumeElement::instantiate() -> void {
    QPalette p{};
    p.setColor(QPalette::Window, QColor{240, 240, 240});
    setPalette(p);

    volume_header = new VolumeHeader{ color_store, area->transferFunctionGroup() };

    let vol{ area->takeNeedInitialWidget() ? area->initialWidget() : new VolumeWidget{dispatcher, properties} };
    connect(vol, &VolumeWidget::ready, area, &ViewArea::volumeWidgetReady);
    // only sets as initial widget the first time it is called
    area->setInitialWidget(vol);

    let layout{ new QVBoxLayout{} };
    layout->addWidget(volume_header);
    layout->addWidget(new VolumeWidgetWrapper{vol, this});
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    setLayout(layout);
    connect(volume_header, &VolumeHeader::rendererChanges   , vol, &VolumeWidget::setRenderer);
    connect(volume_header, &VolumeHeader::linkTokenAdded    , vol, &VolumeWidget::link  );
    connect(volume_header, &VolumeHeader::linkTokenRemoved  , vol, &VolumeWidget::unlink);
    connect(volume_header, &VolumeHeader::removeVolumeWidget, [this, vol](){
        vol->detachFromRenderer();
        if (area->initialWidget() == vol) {
            area->saveVolumeWidget(vol);
        } else {
            dispatcher->removeRender(vol->dataRenderer());
        }
        if (volume_header->hasTransFnSelected()) {
            emit area->selectTransferFunction(TransFnIndex{}, None);
            render_settings->updateSettings(RendererIndex{}, SettingsUpdateSignal{});
        }
        remove();
    });
    connect(volume_header, &VolumeHeader::transferFunctionSelected, vol, &VolumeWidget::transferFunctionSelected);
    connect(volume_header, &VolumeHeader::transferFunctionDeselected, vol, &VolumeWidget::transferFunctionDeselected);
    connect(vol, &VolumeWidget::showTransferFunction, area, &ViewArea::selectTransferFunction);
    connect(vol, &VolumeWidget::showRenderSettings, render_settings, &RenderSettings::updateSettings);
    connect(render_settings, &RenderSettings::updateQuality, vol, &VolumeWidget::updateQuality);
    connect(render_settings, &RenderSettings::updateXray, vol, &VolumeWidget::updateXray);
    connect(render_settings, &RenderSettings::updateVolume, vol, &VolumeWidget::updateVolume);
    connect(render_settings, &RenderSettings::updateSlice, vol, &VolumeWidget::updateSlice);
    connect(render_settings, &RenderSettings::updateLight, vol, &VolumeWidget::updateLight);

    connect(vol, &VolumeWidget::updateTransFnWidgetIfCurrentlyDisplayed, [this](TransFnIndex tIdx, Option<LinkToken> tFnLinkToken){
        if (volume_header->hasTransFnSelected()) {
            emit area->selectTransferFunction(tIdx, tFnLinkToken);
        }
    });
    connect(vol, &VolumeWidget::updateRenderSettingsIfCurrentlyDisplayed, [this](RendererIndex rIdx, SettingsUpdateSignal sig){
        if (volume_header->hasTransFnSelected()) {
            render_settings->updateSettings(rIdx, sig);
        }
    });
}
