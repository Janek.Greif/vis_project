#include <array>
#include <QVBoxLayout>
#include <QToolButton>
#include "MainApp.h"
#include "ui_MainApp.h"
#include "MainHeaderBar.h"
#include "SideCard.h"
#include "LinkTokenWidget.h"
#include "../volume/dat_reader.h"

MainApp::MainApp(Option<QString> fileToLoad, QWidget *parent) :
        QMainWindow{parent},
        ui{new Ui::MainApp},
        file_to_load{std::move(fileToLoad)},
        side_card_ready{false},
        view_area_ready{false},
        volume_reading_future{nullptr}
{
    ui->setupUi(this);
    resize(screen()->availableGeometry().size() * 0.7);
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    let volTex{     std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{     std::make_shared<GeometryEnvironment>() };
    let dispatcher{ std::make_shared<UpdateDispatcher>(geoEnv, volTex, shaderEnv) };
    let properties{ std::make_shared<SharedProperties>() };
    let colorStore{ std::make_shared<ColorStore>() };
    let cameraLinkToken{ properties->requestToken(PropertyLinkType::CAMERA_POS) };
    let zoomLinkToken{ properties->requestToken(PropertyLinkType::CAMERA_ZOOM) };
    let transFnLinkToken{ properties->requestToken(PropertyLinkType::TRANSFER_FUNCTION) };
    let slicePositionsLinkToken{ properties->requestToken(PropertyLinkType::SLICE_POSITIONS) };
    let lightLinkToken{ properties->requestToken(PropertyLinkType::LIGHT) };
    std::array<LinkToken, 5> tokens{ cameraLinkToken, zoomLinkToken, transFnLinkToken, slicePositionsLinkToken, lightLinkToken };

    let mainHeaderBar{ new MainHeaderBar{dispatcher, properties, colorStore, std::span{tokens}} };

    let sideCardWidget{ new SideCard{dispatcher, properties, geoEnv, shaderEnv, volTex} };
    view_area = new ViewArea{ dispatcher, properties, colorStore, sideCardWidget->renderSettings() };


    let layout{ new QVBoxLayout{} };
    layout->setSpacing(0);
    layout->addWidget(mainHeaderBar);
    layout->addWidget(view_area);
    layout->setContentsMargins(0, 0, 0, 0);

    view_area->setSaveLayout(layout);
#ifdef QT_DEBUG
    connect(mainHeaderBar, &MainHeaderBar::reloadShader, &*shaderEnv, &ShaderEnvironment::reload);
#endif
    connect(mainHeaderBar , &MainHeaderBar::loadData         , this          , &MainApp::loadDat);
    connect(this          , &MainApp::datLoaded              , &*volTex      , &VolumeTexture::overrideVolume);
    connect(this          , &MainApp::closeRequest           , &*volTex      , &VolumeTexture::deleteTexture);
    connect(this          , &MainApp::closeRequest           , &*dispatcher      , &UpdateDispatcher::deleteTextures);
    connect(view_area     , &ViewArea::ready                 , this          , &MainApp::viewAreaReady);
    connect(sideCardWidget, &SideCard::ready                 , this          , &MainApp::sideCardReady);
    connect(view_area     , &ViewArea::selectTransferFunction, sideCardWidget, &SideCard::setTransferFunction);
    connect(&*volTex      , &VolumeTexture::newVolumeTransFnUpdate, [volTex, sideCardWidget](){
        if (let& vol{ volTex->getVolume() }; vol) {
            emit sideCardWidget->showVolumeInfo(
                    vol->name,
                    static_cast<u32>(static_cast<f32>(vol->width) * vol->spacing_x),
                    static_cast<u32>(static_cast<f32>(vol->height) * vol->spacing_y),
                    static_cast<u32>(static_cast<f32>(vol->depth) * vol->spacing_z)
            );
        }
    });
    connect(&*volTex, &VolumeTexture::setCursor, [this](Qt::CursorShape cursor){
        setCursor(cursor);
    });

    let headerLayout{ new QVBoxLayout{} };
    headerLayout->setContentsMargins(0, 0, 0, 0);
    headerLayout->setSpacing(2);

    let areaWidget{ new QWidget{} };
    areaWidget->setLayout(layout);

    let mainLayout{ new QSplitter{} };
    mainLayout->setHandleWidth(10);
    mainLayout->addWidget(areaWidget);
    mainLayout->addWidget(sideCardWidget);
    let w{ static_cast<f32>(width()) };
    mainLayout->setSizes(QList<i32>{static_cast<i32>(w * 0.7), static_cast<i32>(w * 0.3)});
    mainLayout->setAutoFillBackground(true);

    headerLayout->addWidget(mainHeaderBar);
    let line{ new QFrame{} };
    line->setObjectName(QString::fromUtf8("line"));
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    headerLayout->addWidget(line);
    headerLayout->addWidget(mainLayout);

    QPalette p{};
    p.setColor(QPalette::Window, QColor{170, 127, 170});
    mainLayout->setPalette(p);

    let headerLayoutWidget{ new QWidget{} };
    headerLayoutWidget->setLayout(headerLayout);
    setCentralWidget(headerLayoutWidget);

    showMaximized();
}

MainApp::~MainApp() {
    delete ui;
}

fn MainApp::loadDat() -> void {
    if (volume_reading_future) {
        delete volume_reading_future;
        volume_reading_future = nullptr;
    }
    volume_reading_future = new QFutureWatcher<Result<MoveCell<Volume>, ReadVolumeDatError>>{};
    connect(volume_reading_future, &QFutureWatcher<Result<MoveCell<Volume>, ReadVolumeDatError>>::finished, this, &MainApp::volumeLoaded);
    if (read_volume_dat_with_dialog(*volume_reading_future, this)) {
        setCursor(Qt::CursorShape::WaitCursor);
    }
}

fn MainApp::volumeLoaded() -> void {
    setCursor(Qt::CursorShape::ArrowCursor);
    if (volume_reading_future) {
        let vol{ volume_reading_future->result() };
        if (vol) {
            emit datLoaded(*vol);
        } else {
            switch (vol.error()) {
                case ReadVolumeDatError::DAT_FILE_NOT_FOUND:
                    qWarning() << "Dat file not found";
                    break;
                case ReadVolumeDatError::INI_FILE_NOT_FOUND:
                    qWarning() << "Ini file not found";
                    break;
                case ReadVolumeDatError::ERROR_READING_FILE:
                    qWarning() << "error reading file";
                    break;
                case ReadVolumeDatError::FILE_TOO_BIG:
                    qWarning() << "file too big";
                    break;
            }
        }
        delete volume_reading_future;
        volume_reading_future = nullptr;
    }
}

fn MainApp::sideCardReady() -> void {
    side_card_ready = true;
//    qDebug() << "side card ready";
    loadInitialFile();
}

fn MainApp::viewAreaReady() -> void {
    view_area_ready = true;
//    qDebug() << "view area ready";
    loadInitialFile();
}

fn MainApp::closeEvent(QCloseEvent* event) -> void {
    emit closeRequest();
}

fn MainApp::loadInitialFile() -> void {
    if (side_card_ready && view_area_ready && file_to_load) {
//        qDebug() << "load init file";
        auto vol{ read_volume_dat(std::filesystem::path{file_to_load->toStdString()}) };
        if (vol) {
            emit datLoaded(MoveCell{std::move(*vol)});
            file_to_load = None;
        } else {
            switch (vol.error()) {
                case ReadVolumeDatError::DAT_FILE_NOT_FOUND:
                    qWarning() << "Dat file not found";
                    break;
                case ReadVolumeDatError::INI_FILE_NOT_FOUND:
                    qWarning() << "Ini file not found";
                    break;
                case ReadVolumeDatError::ERROR_READING_FILE:
                    qWarning() << "error reading file";
                    break;
                case ReadVolumeDatError::FILE_TOO_BIG:
                    qWarning() << "file too big";
                    break;
            }
        }
    }
}

