//
// Created by janek on 06.05.22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_RenderSettings.h" resolved

#include "RenderSettings.h"
#include "ui_RenderSettings.h"


RenderSettings::RenderSettings(QWidget* parent) :
        QWidget{parent}, ui{new Ui::RenderSettings}
{
    ui->setupUi(this);
    connect(ui->s_quality, &QSlider::valueChanged, this, &RenderSettings::qualityUIUpdate);
    connect(ui->r_projection_prespective, &QRadioButton::toggled, this, &RenderSettings::qualityUIUpdate);
    connect(ui->r_projection_ortho, &QRadioButton::toggled, this, &RenderSettings::qualityUIUpdate);
    connect(ui->s_quality, &QSlider::valueChanged, this, &RenderSettings::qualityUIUpdate);
    connect(ui->r_xray_maximum, &QRadioButton::toggled, this, &RenderSettings::xRayUIUpdate);
    connect(ui->s_volume_color_density, &QSlider::valueChanged, this, &RenderSettings::volumeUIUpdate);
    connect(ui->c_slice_x_plane, &QCheckBox::toggled, this, &RenderSettings::sliceUIUpdate);
    connect(ui->c_slice_y_plane, &QCheckBox::toggled, this, &RenderSettings::sliceUIUpdate);
    connect(ui->c_slice_z_plane, &QCheckBox::toggled, this, &RenderSettings::sliceUIUpdate);
    connect(ui->c_light_enabled, &QCheckBox::toggled, this, &RenderSettings::lightUIUpdate);
    connect(ui->c_light_enabled, &QCheckBox::toggled, ui->s_light_power, &QGroupBox::setEnabled);
    connect(ui->c_light_enabled, &QCheckBox::toggled, ui->s_light_ambient, &QSlider::setEnabled);
    connect(ui->c_light_enabled, &QCheckBox::toggled, ui->s_light_diffuse, &QGroupBox::setEnabled);
    connect(ui->c_light_enabled, &QCheckBox::toggled, ui->s_light_specular, &QGroupBox::setEnabled);
    connect(ui->c_light_enabled, &QCheckBox::toggled, ui->s_light_shininess, &QGroupBox::setEnabled);
    connect(ui->s_light_power, &QSlider::valueChanged, this, &RenderSettings::lightUIUpdate);
    connect(ui->s_light_ambient, &QSlider::valueChanged, this, &RenderSettings::lightUIUpdate);
    connect(ui->s_light_diffuse, &QSlider::valueChanged, this, &RenderSettings::lightUIUpdate);
    connect(ui->s_light_specular, &QSlider::valueChanged, this, &RenderSettings::lightUIUpdate);
    connect(ui->s_light_shininess, &QSlider::valueChanged, this, &RenderSettings::lightUIUpdate);
}

RenderSettings::~RenderSettings() {
    delete ui;
}

fn RenderSettings::showVolumeInfo(QString name, u32 x, u32 y, u32 z) -> void {
    ui->e_volume_name->setText(name);
    ui->e_dimensions->setText(QString{"%1×%2×%3"}.arg(x).arg(y).arg(z));
}

fn RenderSettings::updateSettings(RendererIndex idx, SettingsUpdateSignal sig) -> void {
    r_idx = idx;
    if (idx) {
        ui->s_quality->setValue(static_cast<i32>(std::round(-10.0f * (-5.0f+2.0f*std::log(sig.quality.quality)))));
        ui->r_projection_prespective->setChecked(static_cast<bool>(sig.quality.projection));
        ui->r_xray_average->setChecked(static_cast<bool>(sig.xray.mode));
        ui->r_xray_maximum->setChecked(!static_cast<bool>(sig.xray.mode));
        ui->s_volume_color_density->setValue(static_cast<i32>(std::round((20.0f - sig.volume.color_density + 1.0f)*10.0f)));
        ui->c_slice_x_plane->setChecked(sig.slice.show_plane_x);
        ui->c_slice_y_plane->setChecked(sig.slice.show_plane_y);
        ui->c_slice_z_plane->setChecked(sig.slice.show_plane_z);
        ui->c_light_enabled->setChecked(sig.light.status);
        ui->s_light_power->setValue(static_cast<i32>(std::round(sig.light.power)));
        ui->s_light_ambient->setValue(static_cast<i32>(std::round(sig.light.ambient*100.0f)));
        ui->s_light_diffuse->setValue(static_cast<i32>(std::round(sig.light.diffuse*100.0f)));
        ui->s_light_specular->setValue(static_cast<i32>(std::round(sig.light.specular*100.0f)));
        ui->s_light_shininess->setValue(static_cast<i32>(std::round(sig.light.shininess)));
        enableAll(true);
    } else {
        enableAll(false);
    }
}

fn RenderSettings::qualityUIUpdate() -> void {
    GeneralData event{
        .quality = std::exp((50.0f-static_cast<f32>(ui->s_quality->value())) / 20.0f),
        .projection = ui->r_projection_prespective->isChecked() ? Projection::PERSPECTIVE : Projection::ORTHOGRAPHIC,
    };
    emit updateQuality(r_idx, event);
}

fn RenderSettings::xRayUIUpdate() -> void {
    XRayData event {
        .mode = ui->r_xray_maximum->isChecked() ? Mode::MAXIMUM : Mode::AVERAGE,
    };
    emit updateXray(r_idx, event);
}

fn RenderSettings::volumeUIUpdate() -> void {
    VolumeData event {
        .color_density = 20.0f - static_cast<f32>(ui->s_volume_color_density->value()) / 10.0f + 1.0f,
    };
    emit updateVolume(r_idx, event);
}

fn RenderSettings::sliceUIUpdate() -> void {
    SliceData event {
        .show_plane_x = ui->c_slice_x_plane->isChecked(),
        .show_plane_y = ui->c_slice_y_plane->isChecked(),
        .show_plane_z = ui->c_slice_z_plane->isChecked(),
    };
    emit updateSlice(r_idx, event);
}

fn RenderSettings::lightUIUpdate() -> void {
    LightData event {
        .status = ui->c_light_enabled->isChecked(),
        .power = static_cast<f32>(ui->s_light_power->value()),
        .ambient = static_cast<f32>(ui->s_light_ambient->value()) / 100.0f,
        .diffuse = static_cast<f32>(ui->s_light_diffuse->value()) / 100.0f,
        .specular = static_cast<f32>(ui->s_light_specular->value()) / 100.0f,
        .shininess = static_cast<f32>(ui->s_light_shininess->value()),
    };
    emit updateLight(r_idx, event);
}

fn RenderSettings::enableAll(bool enabled) -> void {
    ui->s_quality->setEnabled(enabled);
    ui->l_quality->setEnabled(enabled);
    ui->l_projection->setEnabled(enabled);
    ui->r_projection_prespective->setEnabled(enabled);
    ui->r_projection_ortho->setEnabled(enabled);
    ui->r_xray_maximum->setEnabled(enabled);
    ui->r_xray_average->setEnabled(enabled);
    ui->l_xray_mode->setEnabled(enabled);
    ui->s_volume_color_density->setEnabled(enabled);
    ui->l_volume_color_density->setEnabled(enabled);
    ui->c_slice_x_plane->setEnabled(enabled);
    ui->c_slice_y_plane->setEnabled(enabled);
    ui->c_slice_z_plane->setEnabled(enabled);
    ui->l_slice_show_planes->setEnabled(enabled);
    ui->l_light_status->setEnabled(enabled);
    ui->l_light_power->setEnabled(enabled);
    ui->l_light_ambient->setEnabled(enabled);
    ui->l_light_diffuse->setEnabled(enabled);
    ui->l_light_specular->setEnabled(enabled);
    ui->l_light_shininess->setEnabled(enabled);
    ui->c_light_enabled->setEnabled(enabled);
    ui->s_light_power->setEnabled(enabled && ui->c_light_enabled->isChecked());
    ui->s_light_ambient->setEnabled(enabled && ui->c_light_enabled->isChecked());
    ui->s_light_diffuse->setEnabled(enabled && ui->c_light_enabled->isChecked());
    ui->s_light_specular->setEnabled(enabled && ui->c_light_enabled->isChecked());
    ui->s_light_shininess->setEnabled(enabled && ui->c_light_enabled->isChecked());
}
