//
// Created by janek on 04.02.22.
//
#include <utility>
#include <span>
#include <array>

#include <QVBoxLayout>
#include <QtImGui.h>
#include <QTimer>

#include "VolumeWidget.h"


VolumeWidgetWrapper::VolumeWidgetWrapper(VolumeWidget* volWid, QWidget* parent) :
    QWidget{parent}
{
    let layout{ new QVBoxLayout{} };
    layout->addWidget(volWid);
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);
    setLayout(layout);
}

VolumeWidget::VolumeWidget(std::shared_ptr<UpdateDispatcher> dispatcher, std::shared_ptr<SharedProperties> properties, QWidget *parent, Qt::WindowFlags f) :
    QOpenGLWidget{parent, f},
    data_renderer{},
    dispatcher{std::move(dispatcher)},
    properties{std::move(properties)},
    camera_token{},
    zoom_token{},
    ref{nullptr}
{
    setMouseTracking(true);
//    qDebug() << "create volume widget";
}

fn VolumeWidget::initializeGL() -> void {
//    qDebug() << "init volume widget";
    initializeOpenGLFunctions();
    ref = QtImGui::initialize(this, false);
    if (let idx{ dispatcher->requestRenderer(this) }; idx) {
        data_renderer = *idx;
        sampleTransferFunction();
    }
    emit ready();
}

fn VolumeWidget::resizeGL(i32 w, i32 h) -> void {
    dispatcher->resize(data_renderer, w, h);
}

fn VolumeWidget::paintGL() -> void {
    dispatcher->render(data_renderer);
}

fn VolumeWidget::mouseReleaseEvent(QMouseEvent* event) -> void {
    dispatcher->mouseRelease(data_renderer, event);
}

fn VolumeWidget::mousePressEvent(QMouseEvent* event) -> void {
    dispatcher->mousePress(data_renderer, event);
}

fn VolumeWidget::mouseMoveEvent(QMouseEvent* event) -> void {
    setFocus();
    dispatcher->mouseMove(data_renderer, event);
}

fn VolumeWidget::wheelEvent(QWheelEvent* event) -> void {
    dispatcher->mouseScroll(data_renderer, event);
}

fn VolumeWidget::keyPressEvent(QKeyEvent* event) -> void {
    dispatcher->keyPress(data_renderer, event);
}

fn VolumeWidget::keyReleaseEvent(QKeyEvent* event) -> void {
    dispatcher->keyRelease(data_renderer, event);
}

fn VolumeWidget::update2(PropertyLinkType link) -> void {
    if (let linkedRenderers{ properties->getLinkedRenderers(getTokenByType(link)) }; linkedRenderers.has_value() && !(*linkedRenderers)->empty()) {
        dispatcher->markForRedraw(**linkedRenderers);
    } else {
        std::array<RendererIndex, 1> dr{ data_renderer };
        dispatcher->markForRedraw(std::span<RendererIndex>{ dr });
    }
    dispatcher->globalUpdate(data_renderer);
}

fn VolumeWidget::getTokenByType(PropertyLinkType linkType) -> LinkToken {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing cases");
    switch(linkType) {
        case PropertyLinkType::CAMERA_POS:
            return camera_token;
        case PropertyLinkType::CAMERA_ZOOM:
            return zoom_token;
        case PropertyLinkType::TRANSFER_FUNCTION:
            return trans_fn_token;
        case PropertyLinkType::SLICE_POSITIONS:
            return slice_position_token;
        case PropertyLinkType::LIGHT:
            return light_token;
        default:
            qFatal("switch-case is missing cases");
    }
}

fn VolumeWidget::link(LinkToken token) -> void {
    if (let res{ properties->addLink(token, data_renderer) }; res){
        auto[property, firstLink]{ *res };
        static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing cases");
        switch(token.linkType()) {
            case PropertyLinkType::CAMERA_POS:
                camera_token = token;
                break;
            case PropertyLinkType::CAMERA_ZOOM:
                zoom_token = token;
                break;
            case PropertyLinkType::TRANSFER_FUNCTION:
                trans_fn_token = token;
                break;
            case PropertyLinkType::SLICE_POSITIONS:
                slice_position_token = token;
                break;
            case PropertyLinkType::LIGHT:
                light_token = token;
                break;
            default:
                qFatal("switch-case is missing cases");
        }
        dispatcher->link(data_renderer, std::move(property), firstLink);
    }
}

fn VolumeWidget::unlink(LinkToken token) -> void {
    static_assert(PROPERTY_LINK_COUNT == 5, "switch-case is missing cases");
    switch(token.linkType()) {
        case PropertyLinkType::CAMERA_POS:
            camera_token = LinkToken{};
            break;
        case PropertyLinkType::CAMERA_ZOOM:
            zoom_token = LinkToken{};
            break;
        case PropertyLinkType::TRANSFER_FUNCTION:
            trans_fn_token = LinkToken{};
            break;
        case PropertyLinkType::SLICE_POSITIONS:
            slice_position_token = LinkToken{};
            break;
        case PropertyLinkType::LIGHT:
            light_token = LinkToken{};
            break;
        default:
            qFatal("switch-case is missing cases");
    }
    let lastLink{ properties->removeLink(token, data_renderer) };
    dispatcher->unlink(data_renderer, token.linkType(), lastLink);
}

fn VolumeWidget::detachFromRenderer() -> void {
    dispatcher->detachFromRenderer(data_renderer);
    properties->removeAllLinks(data_renderer);
}

fn VolumeWidget::dataRenderer() -> RendererIndex {
    return data_renderer;
}

fn VolumeWidget::transferFunction() -> TransFnIndex {
    let transFnOpt{ dispatcher->getTransferFunctionIndexForRenderer(data_renderer) };
    assert(transFnOpt);
    return *transFnOpt;
}

fn VolumeWidget::transferFunctionByIdx(TransFnIndex tIdx) -> TransferFunction* {
    return dispatcher->getTransFn(tIdx);
}

fn VolumeWidget::startAnimation(PropertyLinkType linkType) -> void {
    dispatcher->startAnimation(data_renderer, linkType);
}

fn VolumeWidget::stopAnimation(PropertyLinkType linkType) -> void {
    dispatcher->stopAnimation(data_renderer, linkType);
}

fn VolumeWidget::setRenderer(Renderer renderer) -> void {
    dispatcher->setRenderer(data_renderer, renderer);
}

fn VolumeWidget::transferFunctionSelected() -> void {
    let transFnLinkToken{ getTokenByType(PropertyLinkType::TRANSFER_FUNCTION) };
    emit showTransferFunction(transferFunction(), transFnLinkToken ? Option<LinkToken>{transFnLinkToken} : None);
    let sig{ dispatcher->createSettingsUpdateSignal(data_renderer) };
    assert(sig);
    emit showRenderSettings(data_renderer, *sig);
}

fn VolumeWidget::transferFunctionDeselected() -> void {
    emit showTransferFunction(TransFnIndex{}, None);
}

fn VolumeWidget::cloneTransFn(TransFnIndex tIdx) -> Option<TransFnIndex> {
    return dispatcher->cloneTransFn(tIdx);
}

fn VolumeWidget::removeTransFn(TransFnIndex tIdx) -> void {
    dispatcher->removeTransFn(tIdx);
}

fn VolumeWidget::sampleTransferFunction() -> void {
    dispatcher->sampleTransFn(transferFunction());
}

fn VolumeWidget::notifyNewTransferFunction(TransFnIndex tIdx) -> void {
    emit updateTransFnWidgetIfCurrentlyDisplayed(tIdx, trans_fn_token ? Option<LinkToken>{trans_fn_token} : None);
}

fn VolumeWidget::notifyNewRenderSettings(SettingsUpdateSignal sig) -> void {
    emit updateRenderSettingsIfCurrentlyDisplayed(data_renderer, sig);
}

fn VolumeWidget::updateQuality(RendererIndex rIdx, GeneralData data) -> void {
    if (rIdx == data_renderer) {
        dispatcher->qualityUpdate(data_renderer, data);
    }
}

fn VolumeWidget::updateXray(RendererIndex rIdx, XRayData data) -> void {
    if (rIdx == data_renderer) {
        dispatcher->xRayUpdate(data_renderer, data);
    }
}

fn VolumeWidget::updateVolume(RendererIndex rIdx, VolumeData data) -> void {
    if (rIdx == data_renderer) {
        dispatcher->volumeUpdate(data_renderer, data);
    }
}

fn VolumeWidget::updateSlice(RendererIndex rIdx, SliceData data) -> void {
    if (rIdx == data_renderer) {
        dispatcher->sliceUpdate(data_renderer, data);
    }
}

fn VolumeWidget::updateLight(RendererIndex rIdx, LightData data) -> void {
    if (rIdx == data_renderer) {
        dispatcher->lightUpdate(data_renderer, data);
    }
}
