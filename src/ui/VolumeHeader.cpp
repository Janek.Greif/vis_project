//
// Created by janek on 14.04.22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_VolumeHeader.h" resolved
#include <QDragEnterEvent>
#include <QMimeData>
#include <QDrag>

#include "VolumeHeader.h"
#include "ui_VolumeHeader.h"


VolumeHeader::VolumeHeader(std::shared_ptr<ColorStore> colorStore, QButtonGroup* transferFunctionGroup, QWidget *parent) :
        QWidget{parent}, ui{new Ui::VolumeHeader}, color_store{std::move(colorStore)} {
    ui->setupUi(this);
    token_layout = new FlowLayout{0, 1, 1};
    token_layout->setContentsMargins(1, 1, 1, 0);
    ui->link_token_stack->setLayout(token_layout);
    connect(ui->volme_selector, &QCheckBox::toggled, this, &VolumeHeader::transferFunctionToggled);
    connect(ui->remove, &QToolButton::clicked, this, &VolumeHeader::removeVolumeWidget);
    connect(ui->volume, &QRadioButton::clicked, [this](){ emit rendererChanges(VOLUME); });
    connect(ui->slice, &QRadioButton::clicked, [this](){ emit rendererChanges(SLICE); });
    connect(ui->xray, &QRadioButton::clicked, [this](){ emit rendererChanges(XRAY); });
    transferFunctionGroup->addButton(ui->volme_selector);
    setAcceptDrops(true);
    setAutoFillBackground(true);
    QPalette p{};
    p.setColor(QPalette::Window, QColor{241, 241, 241});
    setPalette(p);
}

VolumeHeader::~VolumeHeader() {
    delete ui;
}

fn VolumeHeader::hasTransFnSelected() -> bool {
    return ui->volme_selector->isChecked();
}

fn VolumeHeader::transferFunctionToggled(bool checked) -> void {
    if (checked) {
        emit transferFunctionSelected();
    } else {
        emit transferFunctionDeselected();
    }
}

fn VolumeHeader::checkDropEvent(QDropEvent* event) -> Option<QColor> {
    let mime{ event->mimeData() };
    if (mime->hasFormat("application/x-linkTokenWidgetData")) {
        auto data{ mime->data("application/x-linkTokenWidgetData") };
        QDataStream dataStream{&data, QIODevice::ReadOnly};
        LinkTokenWidgetSerde serde{};
        dataStream >> serde;
        for (let existingToken : ints(0, token_layout->count())
                               | MAP(qobject_cast<LinkTokenWidget*>(token_layout->itemAt(it)->widget()))
                               | FILTER(it != nullptr)
        ) {
            if (existingToken->tokenType() == serde.token.linkType()) {
                event->ignore();
                return None;
            }
        }
        if (color_store->isValid(serde.c_idx)) {
            return color_store->getColor(serde.c_idx);
        }
    }
    return None;
}

fn VolumeHeader::dragEnterEvent(QDragEnterEvent* event) -> void {
    QPalette p{};
    if (let c{ checkDropEvent(event) }; c) {
        p.setColor(QPalette::Window, *c);
        ui->link_token_stack->setPalette(p);
        event->acceptProposedAction();
    } else {
        p.setColor(QPalette::Window, QColor{241, 241, 241});
        ui->link_token_stack->setPalette(p);
        event->ignore();
    }
}

fn VolumeHeader::dragMoveEvent(QDragMoveEvent* event) -> void {
    QPalette p{};
    if (let c{ checkDropEvent(event) }; c) {
        p.setColor(QPalette::Window, *c);
        ui->link_token_stack->setPalette(p);
        event->acceptProposedAction();
    } else {
        p.setColor(QPalette::Window, QColor{241, 241, 241});
        ui->link_token_stack->setPalette(p);
        event->ignore();
    }
}

fn VolumeHeader::dragLeaveEvent(QDragLeaveEvent* event) -> void {
    QPalette p{};
    p.setColor(QPalette::Window, QColor{241, 241, 241});
    ui->link_token_stack->setPalette(p);
    event->accept();
}

fn VolumeHeader::dropEvent(QDropEvent* event) -> void {
    QPalette p{};
    p.setColor(QPalette::Window, QColor{241, 241, 241});
    ui->link_token_stack->setPalette(p);
    auto mime{ event->mimeData() };
    if (mime->hasFormat("application/x-linkTokenWidgetData")) {
        if (event->proposedAction() == Qt::CopyAction || event->proposedAction() == Qt::MoveAction) {
            // copy action originates from the global token stash, where tokens are copy on drag.
            // move action originates from another volume header, and the token was already removed from the original
            // header, so now it has to be inserted into this

            auto data{ mime->data("application/x-linkTokenWidgetData") };
            QDataStream dataStream{&data, QIODevice::ReadOnly};
            LinkTokenWidgetSerde serde{};
            dataStream >> serde;

            if (!color_store->isValid(serde.c_idx)) {
                return;
            }

            for (let existingToken : ints(0, token_layout->count())
                                    | MAP(qobject_cast<LinkTokenWidget*>(token_layout->itemAt(it)->widget()))
                                    | FILTER(it != nullptr)
            ) {
                if (existingToken->tokenType() == serde.token.linkType()) {
                    event->ignore();
                    return;
                }
            }

            event->acceptProposedAction();

            let linkTokenWidget{ new LinkTokenWidget{serde.c_idx, color_store, serde.token} };
            connect(linkTokenWidget, &LinkTokenWidget::dragStart, this, &VolumeHeader::dragStart);
            token_layout->addWidget(linkTokenWidget);
            emit linkTokenAdded(serde.token);
            return;
        }
    }
    event->ignore();
}

fn VolumeHeader::dragStart(LinkTokenWidget* origin) -> void {
    QByteArray linkTokenWidgetData{};
    QDataStream dataStream{&linkTokenWidgetData, QIODevice::WriteOnly};
    let serde{ origin->serdeRepresentation() };
    dataStream << serde;

    let mime{ new QMimeData{} };
    mime->setData("application/x-linkTokenWidgetData", linkTokenWidgetData);

    let drag{ new QDrag{this} };
    drag->setMimeData(mime);
    drag->setPixmap(origin->pixmap());

    delete origin;
    emit linkTokenRemoved(serde.token);

    drag->exec(Qt::MoveAction, Qt::MoveAction);
}
