//
// Created by janek on 14.04.22.
//

#ifndef VIS_PROJECT_VOLUMEHEADER_H
#define VIS_PROJECT_VOLUMEHEADER_H

#include <QWidget>
#include <QButtonGroup>

#include "FlowLayout.h"
#include "LinkTokenWidget.h"
#include "../volume/renderer_types.h"
#include "../oxidize.h"

QT_BEGIN_NAMESPACE
namespace Ui { class VolumeHeader; }
QT_END_NAMESPACE

class VolumeHeader : public QWidget {
Q_OBJECT

public:
    VolumeHeader(std::shared_ptr<ColorStore> colorStore, QButtonGroup* transferFunctionGroup, QWidget *parent = nullptr);

    ~VolumeHeader() override;

    fn hasTransFnSelected() -> bool;

signals:
    fn transferFunctionSelected() -> void;
    fn transferFunctionDeselected() -> void;
    fn removeVolumeWidget() -> void;
    fn linkTokenAdded(LinkToken token) -> void;
    fn linkTokenRemoved(LinkToken token) -> void;
    fn rendererChanges(Renderer renderer) -> void;

protected:
    fn dragEnterEvent(QDragEnterEvent* event) -> void override;
    fn dragMoveEvent(QDragMoveEvent* event) -> void override;
    fn dragLeaveEvent(QDragLeaveEvent* event) -> void override;
    fn dropEvent(QDropEvent* event) -> void override;

private slots:
    fn transferFunctionToggled(bool checked) -> void;
    fn dragStart(LinkTokenWidget* origin) -> void;

private:
    fn checkDropEvent(QDropEvent* event) -> Option<QColor>;

private:
    Ui::VolumeHeader *ui;

    FlowLayout* token_layout;

    std::shared_ptr<ColorStore> color_store;
};


#endif //VIS_PROJECT_VOLUMEHEADER_H
