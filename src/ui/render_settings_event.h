//
// Created by janek on 07.05.22.
//

#ifndef VIS_PROJECT_RENDER_SETTINGS_EVENT_H
#define VIS_PROJECT_RENDER_SETTINGS_EVENT_H

#include "../oxidize.h"

struct LightData{
    bool status;
    /// no difference between shader and ui value;
    f32 power;
    /// a normalized in shader
    f32 ambient;
    /// a normalized in shader
    f32 diffuse;
    /// a normalized in shader
    f32 specular;
    /// no difference between shader and ui value;
    f32 shininess;
};

class Mode {
public:
    /// true
    static const Mode AVERAGE;
    /// false
    static const Mode MAXIMUM;

private:
    consteval inline explicit Mode(bool mode) : mode{mode} {};

public:
    inline constexpr explicit operator bool() const noexcept {
        return mode;
    };

    inline constexpr fn toggle() noexcept -> void {
        mode = !mode;
    };

    inline constexpr bool operator==(const Mode& other) const noexcept = default;
    inline constexpr bool operator!=(const Mode& other) const noexcept = default;

private:
    bool mode;
};

class Projection {
public:
    /// true
    static const Projection PERSPECTIVE;
    /// false
    static const Projection ORTHOGRAPHIC;

private:
    consteval inline explicit Projection(bool mode) : mode{mode} {};

public:
    inline constexpr explicit operator bool() const noexcept {
        return mode;
    };

    inline constexpr fn toggle() noexcept -> void {
        mode = !mode;
    };

    inline constexpr bool operator==(const Projection& other) const noexcept = default;
    inline constexpr bool operator!=(const Projection& other) const noexcept = default;

private:
    bool mode;
};

struct XRayData{
    Mode mode = Mode::MAXIMUM;
};

struct VolumeData{
    /// this is the invers density-factor and divided by in the color_correction in the shader.
    /// color_density (ui) -> color_density (shader): cd |-> (20 - cd + 1)/10
    f32 color_density;
};

struct SliceData{
    bool show_plane_x;
    bool show_plane_y;
    bool show_plane_z;
};

struct GeneralData{
    /// quality -> step_distance: q |-> e^((50-x)/20)
    f32 quality;
    Projection projection = Projection::PERSPECTIVE;
};

struct SettingsUpdateSignal{
    GeneralData quality;
    XRayData xray;
    VolumeData volume;
    SliceData slice;
    LightData light;
};

#endif //VIS_PROJECT_RENDER_SETTINGS_EVENT_H
