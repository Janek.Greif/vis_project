//
// Created by janek on 10.05.22.
//

#ifndef VIS_PROJECT_LINKTOKENDIALOG_H
#define VIS_PROJECT_LINKTOKENDIALOG_H


#include <QDialog>
#include <QPushButton>
#include <QColorDialog>
#include <QComboBox>
#include "../volume/property_link.h"
#include "../oxidize.h"

class SelectColorButton : public QPushButton {
    Q_OBJECT
public:
    SelectColorButton(QWidget* parent);

    fn getColor() const -> const QColor&;

public slots:
    fn setColor(QColor c) -> void;
    fn updateColor() -> void;
    fn changeColor() -> void;

private:
    QColor color;
};

class LinkTokenDialog : public QDialog{
    Q_OBJECT
public:
    explicit LinkTokenDialog(QColor startColor, QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags{});

    fn getData() -> Option<std::pair<PropertyLinkType, QColor>>;

private:
    SelectColorButton* color_btn;
    QComboBox* property_box;
};


#endif //VIS_PROJECT_LINKTOKENDIALOG_H
