//
// Created by janek on 10.05.22.
//

#include "LinkTokenDialog.h"
#include <QFormLayout>
#include <QDialogButtonBox>

SelectColorButton::SelectColorButton(QWidget* parent) : QPushButton{parent} {
    connect(this, &QPushButton::clicked, this, &SelectColorButton::changeColor);
    setAutoFillBackground(true);
};

fn SelectColorButton::setColor(QColor c) -> void {
    color = c;
    updateColor();
};

fn SelectColorButton::getColor() const -> const QColor& {
    return color;
};

fn SelectColorButton::updateColor() -> void {
    setStyleSheet("background-color: " + color.name() + "; border: 1px solid black;");
};

fn SelectColorButton::changeColor() -> void {
    let colorDialog{ new QColorDialog{color, parentWidget()} };
    connect(colorDialog, &QColorDialog::colorSelected, this, &SelectColorButton::setColor);
    colorDialog->open();
};

LinkTokenDialog::LinkTokenDialog(QColor startColor, QWidget* parent, Qt::WindowFlags f) : QDialog{parent, f} {
    setModal(true);
    setMinimumWidth(350);

    let layout{ new QFormLayout{}};
    let mainLayout{ new QVBoxLayout{} };

    property_box = new QComboBox{};
    property_box->setEditable(false);
    property_box->addItem(humanName(PropertyLinkType::CAMERA_POS), QVariant{static_cast<u8>(PropertyLinkType::CAMERA_POS)});
    property_box->addItem(humanName(PropertyLinkType::CAMERA_ZOOM), QVariant{static_cast<u8>(PropertyLinkType::CAMERA_ZOOM)});
    property_box->addItem(humanName(PropertyLinkType::TRANSFER_FUNCTION), QVariant{static_cast<u8>(PropertyLinkType::TRANSFER_FUNCTION)});
    property_box->addItem(humanName(PropertyLinkType::SLICE_POSITIONS), QVariant{static_cast<u8>(PropertyLinkType::SLICE_POSITIONS)});
    property_box->addItem(humanName(PropertyLinkType::LIGHT), QVariant{static_cast<u8>(PropertyLinkType::LIGHT)});
    color_btn = new SelectColorButton{this};
    let btns{ new QDialogButtonBox{QDialogButtonBox::Ok | QDialogButtonBox::Cancel} };

    color_btn->setColor(startColor);

    layout->addRow("Token-Type:", property_box);
    layout->addRow("Color:", color_btn);

    mainLayout->addLayout(layout);
    mainLayout->addWidget(btns);
    setLayout(mainLayout);

    connect(btns, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(btns, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

fn LinkTokenDialog::getData() -> Option<std::pair<PropertyLinkType, QColor>> {
    let comboIdx{ property_box->currentIndex() };
    if (comboIdx >= 0) {
        let linkTypeVariant{ property_box->itemData(comboIdx) };
        let linkType{ linkTypeVariant.value<u8>() };
        if (linkType < PROPERTY_LINK_COUNT) {
            return std::make_pair(static_cast<PropertyLinkType>(comboIdx), color_btn->getColor());
        }
    }
    return None;
}
