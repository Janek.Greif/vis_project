//
// Created by janek on 16.04.22.
//

#ifndef VIS_PROJECT_MAINHEADERBAR_H
#define VIS_PROJECT_MAINHEADERBAR_H

#include <span>

#include <QWidget>

#include "LinkTokenWidget.h"
#include "../volume/UpdateDispatcher.h"
#include "../volume/SharedProperties.h"
#include "../oxidize.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainHeaderBar; }
QT_END_NAMESPACE

class MainHeaderBar : public QWidget {
Q_OBJECT

public:
    explicit MainHeaderBar(
        std::shared_ptr<UpdateDispatcher> dispatcher,
        std::shared_ptr<SharedProperties> properties,
		std::shared_ptr<ColorStore> colorStore,
		std::span<LinkToken> tokens,
		QWidget* parent = nullptr
    );

    ~MainHeaderBar() override;

public slots:
    fn dragStart(LinkTokenWidget* origin) -> void;
    fn removeToken(LinkTokenWidget* origin, LinkToken token) -> void;
    fn computeRemovable(QPoint pos, LinkToken token) -> void;
    fn createNewToken() -> void;

signals:
    fn loadData() -> void;
    fn answerRemovable(QPoint pos, bool removable, LinkToken token) -> void;

#ifdef QT_DEBUG
    fn reloadShader() -> void;
#endif

private:
    fn colorForType(PropertyLinkType type) -> QColor;

private:
    Ui::MainHeaderBar *ui;
    std::shared_ptr<UpdateDispatcher> dispatcher;
    std::shared_ptr<SharedProperties> properties;
    std::shared_ptr<ColorStore> color_store;
};


#endif //VIS_PROJECT_MAINHEADERBAR_H
