//
// Created by janek on 12.02.22.
//
#include <QColor>
#include <QChildEvent>

#include "InstanceSplitter.h"

InstanceSplitter::InstanceSplitter(Param param) :
   QSplitter{param.orientation, param.parent},
   maximum{param.maximum},
   instantiated_elements{0},
   creators{None},
   last_content_signal_was_partial{false}
{
    setChildrenCollapsible(false);
    connect(this, &QSplitter::splitterMoved, this, &InstanceSplitter::handleSplitterMoved);
    QMetaObject::invokeMethod(this, &InstanceSplitter::init, Qt::QueuedConnection);
}

fn InstanceSplitter::handleSplitterMoved(i32 pos, i32 index) -> void {
    bool addedElement{ false };
    if (creators) {
        if (index == 1) {
            instantiateElement(creators->left);
            creators->left = newElementInit();
            insertWidget(0, creators->left);
            updateElementIndices();
            addedElement = true;
        } else if (index == count()-1) {
            instantiateElement(creators->right);
            creators->right = newElementInit();
            addWidget(creators->right);
            updateElementIndices();
            addedElement = true;
        }
    }

    if (instantiated_elements >= maximum && creators) {
        creators->deleteLater();
        creators = None;
        emit contentFull();
        last_content_signal_was_partial = false;
    } else if (addedElement) {
        if (!last_content_signal_was_partial) {
            emit contentPartial();
            last_content_signal_was_partial = true;
        }
    }
}

fn InstanceSplitter::init() -> void {
    creators = Creators {
        .left = newElementInit(),
        .right = newElementInit(),
    };
    let center_element{ newElementInit() };
    addWidget(center_element);
    insertWidget(0, creators->left);
    addWidget(creators->right);
    instantiateElement(center_element);
}

fn InstanceSplitter::removeWidget(InstanceElement* widget) -> void {
    if (indexOf(widget) >= 0) {
        instantiated_elements--;
        delete widget;
        updateElementIndices();
        if (instantiated_elements == 0) {
            emit contentEmpty();
            last_content_signal_was_partial = false;
        } else if (!creators) {
            // maximum amount of elements instantiated
            creators = Creators {
                .left = newElementInit(),
                .right = newElementInit(),
            };
            insertWidget(0, creators->left);
            addWidget(creators->right);
            if (!last_content_signal_was_partial) {
                emit contentPartial();
                last_content_signal_was_partial = true;
            }
        }
    }
}

fn InstanceSplitter::instantiateElement(InstanceElement* element) -> void {
    element->instantiateView(this);
    element->instantiated = true;
    instantiated_elements++;
}

fn InstanceSplitter::newElementInit() -> InstanceElement* {
    let newElem{ newElement() };
    newElem->init(orientation());
    return newElem;
}

fn InstanceSplitter::updateElementIndices() -> void {
    for (let[idx, ch] : ints(0, count()) | MAP(qobject_cast<InstanceElement*>(widget(it))) | FILTER(it != nullptr && it->instantiated) | enumerate ) {
        ch->index_in_splitter = idx;
    }
}

InstanceElement::InstanceElement(QWidget* parent) :
    QWidget{parent},
    orientation{Qt::Orientation::Horizontal},
    index_in_splitter{0},
    instantiated{false}
{
}

fn InstanceElement::init(Qt::Orientation orientation) -> void {
    this->orientation = orientation;
    switch (orientation) {
        case Qt::Horizontal:
            setMinimumWidth(0);
            setMaximumWidth(0);
            break;
        case Qt::Vertical:
            setMinimumHeight(0);
            setMaximumHeight(0);
            break;
    }
}

fn InstanceElement::instantiateView(InstanceSplitter* mvs) -> void {
    switch (orientation) {
        case Qt::Horizontal:
            setMaximumWidth(16777215);
            break;
        case Qt::Vertical:
            setMaximumHeight(16777215);
            break;
    }
    connect(this, &InstanceElement::removeWidget, mvs, &InstanceSplitter::removeWidget);
    instantiate();
}

fn InstanceElement::remove() -> void {
    emit removeWidget(this);
}

fn InstanceElement::indexInSplitter() -> u32 {
    return index_in_splitter;
}
