//
// Created by janek on 12.02.22.
//

#ifndef VIS_PROJECT_INSTANCESPLITTER_H
#define VIS_PROJECT_INSTANCESPLITTER_H

#include <QWidget>
#include <QSplitter>
#include "../oxidize.h"

class InstanceSplitter;

class InstanceElement : public QWidget {
    Q_OBJECT
public:
    explicit InstanceElement(QWidget* parent = nullptr);

signals:
    fn removeWidget(InstanceElement* self) -> void;

public slots:
    fn remove() -> void;

protected:
    virtual fn instantiate() -> void = 0;
    fn indexInSplitter() -> u32;

private:
    fn instantiateView(InstanceSplitter* mvs) -> void;
    fn init(Qt::Orientation orientation) -> void;

private:
    Qt::Orientation orientation;
    u32 index_in_splitter;
    bool instantiated;

    friend class InstanceSplitter;
};

class InstanceSplitter : public QSplitter {
Q_OBJECT

struct Creators {
    InstanceElement* left;
    InstanceElement* right;

    inline fn deleteLater() const -> void {
        left->deleteLater();
        right->deleteLater();
    }
};

public:
    struct Param {
        QWidget* parent = nullptr;
        Qt::Orientation orientation = Qt::Orientation::Vertical;
        usize maximum = std::numeric_limits<usize>::max();
    };

    explicit InstanceSplitter(Param param);

signals:
    fn contentEmpty() -> void;
    fn contentFull() -> void;
    fn contentPartial() -> void;

public slots:
    fn removeWidget(InstanceElement* widget) -> void;

protected:
    virtual fn newElement() -> InstanceElement* = 0;
    fn instantiateElement(InstanceElement* element) -> void;

private slots:
    fn handleSplitterMoved(i32 pos, i32 index) -> void;
    fn init() -> void;

private:
    fn newElementInit() -> InstanceElement*;
    fn updateElementIndices() -> void;

private:
    usize maximum;
    usize instantiated_elements;
    Option<Creators> creators;
    bool last_content_signal_was_partial;

    friend class InstanceElement;
};

#endif //VIS_PROJECT_INSTANCESPLITTER_H
