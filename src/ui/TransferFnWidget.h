//
// Created by janek on 08.03.22.
//

#ifndef VIS_PROJECT_TRANSFERFNWIDGET_H
#define VIS_PROJECT_TRANSFERFNWIDGET_H

#include <QOpenGLWidget>
#include "../volume/TransferFunction.h"
#include "../volume/RendererIndex.h"
#include "../volume/UpdateDispatcher.h"
#include "../volume/SharedProperties.h"
#include "../oxidize.h"

class TransferFnWidget : public QOpenGLWidget, protected QOpenGLExtraFunctions{
    Q_OBJECT
public:
    TransferFnWidget(std::shared_ptr<UpdateDispatcher> dispatcher, std::shared_ptr<SharedProperties> properties, QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags{});

public slots:
    fn setTransferFunction(TransFnIndex transFn, Option<LinkToken> tFnLinkToken) -> void;
    fn update2() -> void;
    fn addSegment() -> void;
    fn sampleTransFn() -> void;

signals:
    fn addedSegment() -> void;
    fn segmentsEmpty() -> void;


private slots:
    fn colorSelected(const QColor& color) -> void;

protected:
    fn initializeGL() -> void override;
    fn resizeGL(i32 w, i32 h) -> void override;
    fn paintGL() -> void override;

    fn mousePressEvent(QMouseEvent* event) -> void override;
    fn mouseReleaseEvent(QMouseEvent* event) -> void override;
    fn mouseMoveEvent(QMouseEvent* event) -> void override;
    fn enterEvent(QEnterEvent* event) -> void override;
    fn leaveEvent(QEvent* event) -> void override;



private:
    TransferFunction* trans_fn;
    TransFnIndex trans_fn_index;
    Option<LinkToken> trans_fn_link_token;
    std::shared_ptr<UpdateDispatcher> dispatcher;
    std::shared_ptr<SharedProperties> properties;
};


#endif //VIS_PROJECT_TRANSFERFNWIDGET_H
