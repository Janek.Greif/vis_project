//
// Created by janek on 04.02.22.
//

#ifndef VIS_PROJECT_VOLUMEWIDGET_H
#define VIS_PROJECT_VOLUMEWIDGET_H


#include <QOpenGLWidget>
#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>

#include <QtImGui.h>

#include "render_settings_event.h"
#include "../volume/VolumeTexture.h"
#include "../volume/DataRenderer.h"
#include "../volume/UpdateDispatcher.h"
#include "../volume/property_link.h"
#include "../volume/SharedProperties.h"
#include "../oxidize.h"

class VolumeWidget : public QOpenGLWidget, protected QOpenGLExtraFunctions {
    Q_OBJECT
public:
    VolumeWidget(std::shared_ptr<UpdateDispatcher> dispatcher, std::shared_ptr<SharedProperties> properties, QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags{});

    inline fn getImGuiRef() -> QtImGui::RenderRef {
        return ref;
    };
    fn dataRenderer() -> RendererIndex;
    fn transferFunction() -> TransFnIndex;
    fn transferFunctionByIdx(TransFnIndex tIdx) -> TransferFunction*;
    fn cloneTransFn(TransFnIndex tIdx) -> Option<TransFnIndex>;
    fn removeTransFn(TransFnIndex tIdx) -> void;
    fn sampleTransferFunction() -> void;
    fn getTokenByType(PropertyLinkType linkType) -> LinkToken;
    fn notifyNewTransferFunction(TransFnIndex tIdx) -> void;
    fn notifyNewRenderSettings(SettingsUpdateSignal sig) -> void;

signals:
    fn ready() -> void;
    fn updateTransFnWidgetIfCurrentlyDisplayed(TransFnIndex tIdx, Option<LinkToken> tFnLinkToken) -> void;
    fn updateRenderSettingsIfCurrentlyDisplayed(RendererIndex rIdx, SettingsUpdateSignal sig) -> void;
    fn showTransferFunction(TransFnIndex tIdx, Option<LinkToken> tFnLinkToken) -> void;
    fn showRenderSettings(RendererIndex rIdx, SettingsUpdateSignal sig) -> void;

public slots:
    fn update2(PropertyLinkType link) -> void;

    fn link(LinkToken token) -> void;
    fn unlink(LinkToken token) -> void;
    fn detachFromRenderer() -> void;
    fn startAnimation(PropertyLinkType linkType) -> void;
    fn stopAnimation(PropertyLinkType linkType) -> void;
    fn setRenderer(Renderer renderer) -> void;
    fn transferFunctionSelected() -> void;
    fn transferFunctionDeselected() -> void;

    fn updateQuality(RendererIndex idx, GeneralData event) -> void;
    fn updateXray(RendererIndex idx, XRayData event) -> void;
    fn updateVolume(RendererIndex idx, VolumeData event) -> void;
    fn updateSlice(RendererIndex idx, SliceData event) -> void;
    fn updateLight(RendererIndex idx, LightData event) -> void;

protected:
    fn initializeGL() -> void override;
    fn resizeGL(int w, int h) -> void override;
    fn paintGL() -> void override;
    fn mousePressEvent(QMouseEvent* event) -> void override;
    fn mouseReleaseEvent(QMouseEvent* event) -> void override;
    fn mouseMoveEvent(QMouseEvent* event) -> void override;
    fn wheelEvent(QWheelEvent* event) -> void override;
    fn keyPressEvent(QKeyEvent* event) -> void override;
    fn keyReleaseEvent(QKeyEvent* event) -> void override;

private:

    std::shared_ptr<UpdateDispatcher> dispatcher;
    std::shared_ptr<SharedProperties> properties;
    RendererIndex data_renderer;

    static_assert(PROPERTY_LINK_COUNT == 5, "LinkTokens are missing");
    LinkToken camera_token;
    LinkToken zoom_token;
    LinkToken trans_fn_token;
    LinkToken slice_position_token;
    LinkToken light_token;

    QtImGui::RenderRef ref;
};

class VolumeWidgetWrapper : public QWidget {
public:
    explicit VolumeWidgetWrapper(VolumeWidget* volWid, QWidget* parent = nullptr);

};

#endif //VIS_PROJECT_VOLUMEWIDGET_H
