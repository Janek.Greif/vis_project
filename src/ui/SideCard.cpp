//
// Created by janek on 08.03.22.
//

#include <iostream>

#include <QVBoxLayout>
#include "RenderSettings.h"
#include "SideCard.h"


SideCard::SideCard(std::shared_ptr<UpdateDispatcher> dispatcher, std::shared_ptr<SharedProperties> properties, std::shared_ptr<GeometryEnvironment> geoEnv, std::shared_ptr<ShaderEnvironment> shaderEnv, std::shared_ptr<VolumeTexture> volume, QWidget* parent) :
    QWidget{parent},
    volume_tex{std::move(volume)},
    geo_env{std::move(geoEnv)},
    shader_env{std::move(shaderEnv)},
    dispatcher{std::move(dispatcher)},
    properties{std::move(properties)},
    add_segment_btn{nullptr}
{
    setAutoFillBackground(true);
    QPalette p{};
    p.setColor(QPalette::Window, QColor{241, 241, 241});
    setPalette(p);
    QMetaObject::invokeMethod(this, &SideCard::init, Qt::QueuedConnection);
    render_settings = new RenderSettings{};
}

fn SideCard::renderSettings() -> RenderSettings* {
    return render_settings;
}

fn SideCard::addedSegment() -> void {
    add_segment_btn->setDisabled(false);
}

fn SideCard::addSegment() -> void {
    add_segment_btn->setDisabled(true);
}

fn SideCard::init() -> void {
    let mainLayout{ new QVBoxLayout{} };
    let btnLayout{ new QHBoxLayout{} };
    let layout{ new QVBoxLayout{} };
    transfer_fn_widget = new TransferFnWidget{dispatcher, properties, this};
    add_segment_btn = { new QPushButton{"New Segment"} };
    add_segment_btn->setToolTip("Add a new Segment to the Transfer-Function. Press this Button, then click in the Transfer-Function-Area to set the first point.");
    let sampleBtn{ new QPushButton{"Sample"} };
    sampleBtn->setToolTip("Applies the Transfer-Function to all connected Renderers.");
    btnLayout->addWidget(add_segment_btn);
    btnLayout->addWidget(sampleBtn);

    add_segment_btn->setDisabled(true);

    layout->addLayout(btnLayout);
    layout->addWidget(transfer_fn_widget);


    connect(this, &SideCard::renderTransFn, transfer_fn_widget, &TransferFnWidget::update2);
    connect(add_segment_btn, &QPushButton::pressed, this, &SideCard::addSegment);
    connect(add_segment_btn, &QPushButton::pressed, transfer_fn_widget, &TransferFnWidget::addSegment);
    connect(transfer_fn_widget, &TransferFnWidget::addedSegment, this, &SideCard::addedSegment);
    connect(transfer_fn_widget, &TransferFnWidget::segmentsEmpty, this, &SideCard::addSegment);
    connect(this, &SideCard::setTransferFunction, transfer_fn_widget, &TransferFnWidget::setTransferFunction);
    connect(sampleBtn, &QPushButton::pressed, transfer_fn_widget, &TransferFnWidget::sampleTransFn);

    connect(this, &SideCard::showVolumeInfo, render_settings, &RenderSettings::showVolumeInfo);
    connect(this, &SideCard::updateSettings, render_settings, &RenderSettings::updateSettings);
    connect(render_settings, &RenderSettings::updateQuality, this, &SideCard::updateQuality);
    connect(render_settings, &RenderSettings::updateLight, this, &SideCard::updateLight);
    connect(render_settings, &RenderSettings::updateSlice, this, &SideCard::updateSlice);
    connect(render_settings, &RenderSettings::updateVolume, this, &SideCard::updateVolume);
    connect(render_settings, &RenderSettings::updateXray, this, &SideCard::updateXray);

    mainLayout->addLayout(layout, 1);
    mainLayout->addWidget(render_settings, 1);

    setLayout(mainLayout);
    emit ready();
}
