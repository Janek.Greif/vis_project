//
// Created by janek on 12.02.22.
//

#ifndef VIS_PROJECT_VIEWAREA_H
#define VIS_PROJECT_VIEWAREA_H

#include <QWidget>
#include <QVBoxLayout>
#include <QButtonGroup>

#include "InstanceSplitter.h"
#include "VolumeHeader.h"
#include "LinkTokenWidget.h"
#include "RenderSettings.h"
#include "../volume/VolumeTexture.h"
#include "../volume/UpdateDispatcher.h"
#include "../volume/SharedProperties.h"
#include "../oxidize.h"

class VolumeWidget;
class ViewArea;

class VolumeElement : public InstanceElement {
    Q_OBJECT
public:
    explicit VolumeElement(
            std::shared_ptr<UpdateDispatcher> dispatcher,
            std::shared_ptr<SharedProperties> properties,
            std::shared_ptr<ColorStore> colorStore,
            ViewArea* area,
            RenderSettings* renderSettings,
            QWidget* parent
    );

public slots:
    fn putHeaderInPlace() -> void;

signals:
    fn reloadShader() -> void;

protected:
    fn instantiate() -> void override;

private:
    std::shared_ptr<UpdateDispatcher> dispatcher;
    std::shared_ptr<SharedProperties> properties;
    std::shared_ptr<ColorStore> color_store;
    ViewArea* area;
    RenderSettings* render_settings;
    VolumeHeader* volume_header;
};

class VerticalArea : public InstanceSplitter {
    Q_OBJECT
public:
    explicit VerticalArea(
            std::shared_ptr<UpdateDispatcher> dispatcher,
		    std::shared_ptr<SharedProperties> properties,
		    std::shared_ptr<ColorStore> colorStore,
		    ViewArea* area,
            RenderSettings* renderSettings,
		    QWidget* parent = nullptr
    );

protected:
    fn newElement() -> InstanceElement* override;

private:
    std::shared_ptr<UpdateDispatcher> dispatcher;
    std::shared_ptr<SharedProperties> properties;
    std::shared_ptr<ColorStore> color_store;
    ViewArea* area;
    RenderSettings* render_settings;
};

class VerticalElement : public InstanceElement {
    Q_OBJECT
public:
    explicit VerticalElement(
            std::shared_ptr<UpdateDispatcher> dispatcher,
			std::shared_ptr<SharedProperties> properties,
			std::shared_ptr<ColorStore> colorStore,
			ViewArea* area,
            RenderSettings* renderSettings
    );
    ~VerticalElement() override;

protected:
    fn instantiate() -> void override;

private:
    std::shared_ptr<UpdateDispatcher> dispatcher;
    std::shared_ptr<SharedProperties> properties;
    std::shared_ptr<ColorStore> color_store;
    ViewArea* area;
    RenderSettings* render_settings;
};

class ViewArea : public InstanceSplitter {
Q_OBJECT
public:
    explicit ViewArea(
            std::shared_ptr<UpdateDispatcher> dispatcher,
			std::shared_ptr<SharedProperties> properties,
			std::shared_ptr<ColorStore> colorStore,
            RenderSettings* renderSettings,
			QWidget *parent = nullptr
    );

    fn setInitialWidget(VolumeWidget* widget) -> void;
    fn initialWidget() -> VolumeWidget*;
    fn takeNeedInitialWidget() -> bool;
    fn saveVolumeWidget(VolumeWidget* volWid) -> void;
    fn setSaveLayout(QVBoxLayout* saveLayout) -> void;
    fn transferFunctionGroup() -> QButtonGroup*;

signals:
    fn ready() -> void;
    fn selectTransferFunction(TransFnIndex tIdx, Option<LinkToken> tFnLinkToken) -> void;

public slots:
    fn addSingleElement() -> void;
    fn volumeWidgetReady() -> void;

protected:
    fn newElement() -> InstanceElement* override;

private:
    std::shared_ptr<UpdateDispatcher> dispatcher;
    std::shared_ptr<SharedProperties> properties;
    std::shared_ptr<ColorStore> color_store;
    RenderSettings* render_settings;
    VolumeWidget* initial_widget;
    bool need_initial_widget;
    QVBoxLayout* save_layout;
    QButtonGroup* transfer_function_group;
};


#endif //VIS_PROJECT_VIEWAREA_H
