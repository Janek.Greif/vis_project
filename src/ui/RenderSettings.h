//
// Created by janek on 06.05.22.
//

#ifndef VIS_PROJECT_RENDERSETTINGS_H
#define VIS_PROJECT_RENDERSETTINGS_H

#include <QWidget>
#include <QString>
#include <QVector3D>
#include "render_settings_event.h"
#include "../volume/RendererIndex.h"
#include "../oxidize.h"

QT_BEGIN_NAMESPACE
namespace Ui { class RenderSettings; }
QT_END_NAMESPACE

class RenderSettings : public QWidget {
Q_OBJECT

public:
    explicit RenderSettings(QWidget* parent = nullptr);

    ~RenderSettings() override;

public slots:
    fn showVolumeInfo(QString name, u32 x, u32 y, u32 z) -> void;
    fn updateSettings(RendererIndex idx, SettingsUpdateSignal sig) -> void;

signals:
    fn updateQuality(RendererIndex idx, GeneralData event) -> void;
    fn updateXray(RendererIndex idx, XRayData event) -> void;
    fn updateVolume(RendererIndex idx, VolumeData event) -> void;
    fn updateSlice(RendererIndex idx, SliceData event) -> void;
    fn updateLight(RendererIndex idx, LightData event) -> void;

private slots:
    fn qualityUIUpdate() -> void;
    fn xRayUIUpdate() -> void;
    fn volumeUIUpdate() -> void;
    fn sliceUIUpdate() -> void;
    fn lightUIUpdate() -> void;

private:
    fn enableAll(bool enabled) -> void;

private:
    Ui::RenderSettings* ui;

    RendererIndex r_idx;
};


#endif //VIS_PROJECT_RENDERSETTINGS_H
