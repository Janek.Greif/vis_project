//
// Created by janek on 16.04.22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_MainHeaderBar.h" resolved
#include <QMimeData>
#include <QDrag>
#include <QRandomGenerator>

#include "FlowLayout.h"
#include "MainHeaderBar.h"
#include "ui_MainHeaderBar.h"
#include "LinkTokenDialog.h"

MainHeaderBar::MainHeaderBar(
        std::shared_ptr<UpdateDispatcher> dispatcher,
        std::shared_ptr<SharedProperties> properties,
		std::shared_ptr<ColorStore> colorStore,
		std::span<LinkToken> tokens,
		QWidget* parent
) :
    QWidget{parent},
    ui{new Ui::MainHeaderBar},
    dispatcher{std::move(dispatcher)},
    properties{std::move(properties)},
    color_store{colorStore}
{
    ui->setupUi(this);
    connect(ui->load_data, &QToolButton::clicked, this, &MainHeaderBar::loadData);
    connect(ui->add_new_token, &QToolButton::clicked, this, &MainHeaderBar::createNewToken);

    for (let token : tokens) {
        auto linkTokenWidget{ new LinkTokenWidget{colorStore, token, colorForType(token.linkType()), true} };
        connect(linkTokenWidget, &LinkTokenWidget::dragStart, this, &MainHeaderBar::dragStart);
        connect(linkTokenWidget, &LinkTokenWidget::removeToken, this, &MainHeaderBar::removeToken);
        connect(linkTokenWidget, &LinkTokenWidget::queryRemovable, this, &MainHeaderBar::computeRemovable);
        connect(this, &MainHeaderBar::answerRemovable, linkTokenWidget, &LinkTokenWidget::removeQueryResult);

        ui->token_layout->insertWidget(ui->token_layout->count()-2, linkTokenWidget);
    }

#ifdef QT_DEBUG
    let line{ new QFrame{} };
    line->setObjectName(QString::fromUtf8("line"));
    line->setFrameShape(QFrame::VLine);
    line->setFrameShadow(QFrame::Sunken);
    line->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
    line->setMinimumHeight(29);
    let reloadShaderButton{ new QToolButton{} };
    reloadShaderButton->setText(QString{"🔄"});
    reloadShaderButton->setContentsMargins(0, 1, 0, 1);
    auto f{ reloadShaderButton->font() };
    f.setPixelSize(16);
    reloadShaderButton->setFont(f);

    ui->layout->insertWidget(1, line);
    ui->layout->insertWidget(2, reloadShaderButton);
    connect(reloadShaderButton, &QToolButton::released, this, &MainHeaderBar::reloadShader);
#endif
}

MainHeaderBar::~MainHeaderBar() {
    delete ui;
}

fn MainHeaderBar::dragStart(LinkTokenWidget* origin) -> void {
    QByteArray linkTokenWidgetData{};
    QDataStream dataStream{&linkTokenWidgetData, QIODevice::WriteOnly};
    dataStream << origin->serdeRepresentation();

    let mime{ new QMimeData{} };
    mime->setData("application/x-linkTokenWidgetData", linkTokenWidgetData);

    let drag{ new QDrag{this} };
    drag->setMimeData(mime);
    drag->setPixmap(origin->pixmap());

    drag->exec(Qt::CopyAction, Qt::CopyAction);
}

fn MainHeaderBar::removeToken(LinkTokenWidget* origin, LinkToken token) -> void {
    properties->removeToken(token);
    origin->deleteLater();
}

fn MainHeaderBar::computeRemovable(QPoint pos, LinkToken token) -> void {
    emit answerRemovable(pos, !properties->tokenInUse(token), token);
}

fn MainHeaderBar::createNewToken() -> void {
    let startColor{ QColor::fromHslF(static_cast<f32>(QRandomGenerator::global()->bounded(1.0f)), 0.9, 0.7) };
    LinkTokenDialog dialog{startColor};
    if (let res{ dialog.exec() }; res == QDialog::Accepted){
        if (let dialogData{ dialog.getData() }; dialogData) {
            let[tokenType, color]{ *dialogData };
            let newToken{ properties->requestToken(tokenType) };

            auto linkTokenWidget{ new LinkTokenWidget{color_store, newToken, color, true} };
            connect(linkTokenWidget, &LinkTokenWidget::dragStart, this, &MainHeaderBar::dragStart);
            connect(linkTokenWidget, &LinkTokenWidget::removeToken, this, &MainHeaderBar::removeToken);
            connect(linkTokenWidget, &LinkTokenWidget::queryRemovable, this, &MainHeaderBar::computeRemovable);
            connect(this, &MainHeaderBar::answerRemovable, linkTokenWidget, &LinkTokenWidget::removeQueryResult);

            ui->token_layout->insertWidget(ui->token_layout->count()-2, linkTokenWidget);
        }
    }
}

fn MainHeaderBar::colorForType(PropertyLinkType type) -> QColor {
    static_assert(PROPERTY_LINK_COUNT == 5, "missing switch case branches.");
    switch(type) {
        case PropertyLinkType::CAMERA_POS:
            return QColor{255, 125, 200};
        case PropertyLinkType::CAMERA_ZOOM:
            return QColor{200, 125, 255};
        case PropertyLinkType::TRANSFER_FUNCTION:
            return QColor{125, 200, 255};
        case PropertyLinkType::SLICE_POSITIONS:
            return QColor{200, 255, 125};
        case PropertyLinkType::LIGHT:
            return QColor{255, 200, 125};
    }
    qFatal("missing switch cas branches.");
}
