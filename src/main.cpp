#include <QApplication>
#include <QSurfaceFormat>
#include <QDir>
#include "ui/MainApp.h"

int main(int argc, char *argv[]) {
    auto f{ QSurfaceFormat::defaultFormat() };
    f.setMajorVersion(4);
    f.setMinorVersion(5);
    f.setRenderableType(QSurfaceFormat::OpenGL);
    f.setProfile(QSurfaceFormat::CompatibilityProfile);
    f.setOption(QSurfaceFormat::DeprecatedFunctions, true);
    QSurfaceFormat::setDefaultFormat(f);
    QApplication a{argc, argv};

#ifdef QT_DEBUG
    QDir dir{ PROJECT_DIR };
    dir.cd("shader_src");
    QDir::setSearchPaths("shader", QStringList(dir.absolutePath()));
#else
    QDir::setSearchPaths("shader", {":/shader_src"});
#endif
    QDir::setSearchPaths("icons", {":/icons"});

    let argList{ QApplication::arguments() };
    auto fileToLoad{
            (argList.size() >= 2) ? std::make_optional(argList[1]) : None
    };
    MainApp app{std::move(fileToLoad)};
    app.show();
    return QApplication::exec();
}
