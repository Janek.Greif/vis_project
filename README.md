# vis_project

## setup
This project uses CMake, C++20, and Qt6.2. You also need to set the environment variable `CMAKE_PREFIX_PATH`
to the cmake folder of your Qt installation (for example on linux: `CMAKE_PREFIX_PATH=~/Qt/6.2.2/gcc_64/lib/cmake`).  
If you are using CLion, then set it in the cmake profiles under Settings -> Build, Execution, Deployment -> CMake for each profile.  

The name of any *.ui file has to be exactly the same as the class-name, otherwise case-sensitive operating-systems cannot find them.  

This project uses git-submodules, so cloning should be done with the `git clone https://git.app.uib.no/Janek.Greif/vis_project --recursive` command, or initialize the submodules with `git submodule update --init --recursive`

The project uses [https://git.app.uib.no/Janek.Greif/vis_project](https://git.app.uib.no/Janek.Greif/vis_project) as its host repository.

### gdb debugger
This project comes with a pretty-printer extension for gdb to improve debugging of Qt-Classes. There is already a `.gdbinit` and `qt.py`
to instruct gdb how to pretty-print (`QVariant` is not supported for now, requires python3), but this project has to be allowed to load custom gdb-extensions. Please follow the guide from JetBrains:
[https://www.jetbrains.com/help/clion/configuring-debugger-options.html#gdb-startup](https://www.jetbrains.com/help/clion/configuring-debugger-options.html#gdb-startup)  
(Note this only works for non MSVC build chains.)

