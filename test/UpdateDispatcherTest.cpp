//
// Created by janek on 22.02.22.
//

#include <qtestcase.h>
#include "UpdateDispatcherTest.h"
#include "../src/volume/SharedProperties.h"

fn update2(SharedProperties& properties, UpdateDispatcher& dispatcher, LinkToken token, RendererIndex index) -> void {
    if (let linkedRenderers{ properties.getLinkedRenderers(token) }; linkedRenderers.has_value() && !(*linkedRenderers)->empty()) {
        dispatcher.markForRedraw(**linkedRenderers);
    } else {
        dispatcher.markForRedraw(std::vector<RendererIndex>{ index });
    }
    dispatcher.globalUpdate(index);
}

fn UpdateDispatcherTest::new_redraw_no_links() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    let dispatcher{ std::make_shared<UpdateDispatcher>(geoEnv, volTex, shaderEnv) };
    let properties{ new SharedProperties{}};
    let token{ properties->requestToken(PropertyLinkType::CAMERA_POS) };


    let uuidOpt{ dispatcher->all_renderers.requestNew(nullptr) };
    QVERIFY(uuidOpt);
    let uuid{ *uuidOpt };
    update2(*properties, *dispatcher, token, uuid);
    QCOMPARE(dispatcher->all_renderers.get(uuid)->render_update_count, 1);
}

fn UpdateDispatcherTest::new_redraw_1_links() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    let dispatcher{ std::make_shared<UpdateDispatcher>(geoEnv, volTex, shaderEnv) };
    let properties{ new SharedProperties{}};

    let token{ properties->requestToken(PropertyLinkType::CAMERA_POS) };

    let uuid1Opt{ dispatcher->all_renderers.requestNew(nullptr) };
    let uuid2Opt{ dispatcher->all_renderers.requestNew(nullptr) };

    QVERIFY(uuid1Opt);
    QVERIFY(uuid2Opt);

    let uuid1{ *uuid1Opt };
    let uuid2{ *uuid2Opt };
    QVERIFY(uuid1 != uuid2);

    let res1{ properties->addLink(token, uuid1) };
    let res2{ properties->addLink(token, uuid2) };

    QVERIFY(res1);
    QVERIFY(res2);

    let[prop1, firstLink1]{ *res1 };
    let[prop2, firstLink2]{ *res2 };

    QVERIFY(firstLink1);
    QVERIFY(!firstLink2);

    dispatcher->link(uuid1, prop1, firstLink1);

    // since firstLink2 is false, linking causes renderer `uuid2` to change its data to the shared data and update its view
    dispatcher->link(uuid2, prop2, firstLink2);
    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 0);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 1);

    // updates both renderers
    update2(*properties, *dispatcher, token, uuid1);

    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 1);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 2);
}

fn UpdateDispatcherTest::add_token_redraw_remove_token_redraw() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    let dispatcher{ std::make_shared<UpdateDispatcher>(geoEnv, volTex, shaderEnv) };
    let properties{ new SharedProperties{}};

    let token{ properties->requestToken(PropertyLinkType::CAMERA_POS) };
    let uuid1Opt{ dispatcher->all_renderers.requestNew(nullptr) };
    let uuid2Opt{ dispatcher->all_renderers.requestNew(nullptr) };
    QVERIFY(uuid1Opt);
    QVERIFY(uuid2Opt);

    let uuid1{ *uuid1Opt };
    let uuid2{ *uuid2Opt };
    QVERIFY(uuid1 != uuid2);

    let res1{ properties->addLink(token, uuid1) };
    let res2{ properties->addLink(token, uuid2) };

    QVERIFY(res1);
    QVERIFY(res2);

    let[prop1, firstLink1]{ *res1 };
    let[prop2, firstLink2]{ *res2 };

    QVERIFY(firstLink1);
    QVERIFY(!firstLink2);

    dispatcher->link(uuid1, prop1, firstLink1);

    // since firstLink2 is false, linking causes renderer `uuid2` to change its data to the shared data and update its view
    dispatcher->link(uuid2, prop2, firstLink2);
    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 0);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 1);

    // updates both renderers
    update2(*properties, *dispatcher, token, uuid2);
    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 1);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 2);

    let inUse1{ properties->removeToken(token) };
    QVERIFY(!inUse1);

    properties->removeLink(token, uuid2);
    dispatcher->unlink(uuid2, PropertyLinkType::CAMERA_POS, false);
    properties->removeLink(token, uuid1);
    dispatcher->unlink(uuid1, PropertyLinkType::CAMERA_POS, true);

    let inUse2{ properties->removeToken(token) };
    QVERIFY(inUse2);

    // update only renderer `uuid1`
    update2(*properties, *dispatcher, token, uuid1);

    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 2);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 2);
}

fn UpdateDispatcherTest::multiple_tokens() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    let dispatcher{ std::make_shared<UpdateDispatcher>(geoEnv, volTex, shaderEnv) };
    let properties{ new SharedProperties{}};

    let token1{ properties->requestToken(PropertyLinkType::CAMERA_POS) };
    let token2{ properties->requestToken(PropertyLinkType::CAMERA_POS) };

    let uuid1Opt{ dispatcher->requestRenderer(nullptr) };
    let uuid2Opt{ dispatcher->requestRenderer(nullptr) };
    let uuid3Opt{ dispatcher->requestRenderer(nullptr) };
    let uuid4Opt{ dispatcher->requestRenderer(nullptr) };
    QVERIFY(uuid1Opt);
    QVERIFY(uuid2Opt);
    QVERIFY(uuid3Opt);
    QVERIFY(uuid4Opt);
    let uuid1{ *uuid1Opt };
    let uuid2{ *uuid2Opt };
    let uuid3{ *uuid3Opt };
    let uuid4{ *uuid4Opt };

    QVERIFY(uuid1 != uuid2);
    QVERIFY(uuid1 != uuid3);
    QVERIFY(uuid1 != uuid4);
    QVERIFY(uuid2 != uuid3);
    QVERIFY(uuid2 != uuid4);
    QVERIFY(uuid3 != uuid4);

    QVERIFY(token1 != token2);

    let res1{ properties->addLink(token1, uuid1) };
    let res2{ properties->addLink(token1, uuid2) };
    let res3{ properties->addLink(token2, uuid3) };
    let res4{ properties->addLink(token2, uuid4) };


    QVERIFY(res1);
    QVERIFY(res2);
    QVERIFY(res3);
    QVERIFY(res4);

    let[prop1, firstLink1]{ *res1 };
    let[prop2, firstLink2]{ *res2 };
    let[prop3, firstLink3]{ *res3 };
    let[prop4, firstLink4]{ *res4 };

    QVERIFY(firstLink1);
    QVERIFY(!firstLink2);
    QVERIFY(firstLink3);
    QVERIFY(!firstLink4);

    dispatcher->link(uuid1, prop1, firstLink1);
    dispatcher->link(uuid2, prop2, firstLink2); // causes a rerender of `uuid2`
    dispatcher->link(uuid3, prop3, firstLink3);
    dispatcher->link(uuid4, prop4, firstLink4); // cases a rerender of `uuid4`

    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 0);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 1);
    QCOMPARE(dispatcher->all_renderers.get(uuid3)->render_update_count, 0);
    QCOMPARE(dispatcher->all_renderers.get(uuid4)->render_update_count, 1);

    let failedRes{ properties->addLink(token1, uuid3) };
    QVERIFY(!failedRes);

    update2(*properties, *dispatcher, token1, uuid1);
    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 1);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 2);
    QCOMPARE(dispatcher->all_renderers.get(uuid3)->render_update_count, 0);
    QCOMPARE(dispatcher->all_renderers.get(uuid4)->render_update_count, 1);

    update2(*properties, *dispatcher, token1, uuid2);
    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 2);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 3);
    QCOMPARE(dispatcher->all_renderers.get(uuid3)->render_update_count, 0);
    QCOMPARE(dispatcher->all_renderers.get(uuid4)->render_update_count, 1);

    update2(*properties, *dispatcher, token2, uuid3);
    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 2);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 3);
    QCOMPARE(dispatcher->all_renderers.get(uuid3)->render_update_count, 1);
    QCOMPARE(dispatcher->all_renderers.get(uuid4)->render_update_count, 2);

    update2(*properties, *dispatcher, token2, uuid4);
    QCOMPARE(dispatcher->all_renderers.get(uuid1)->render_update_count, 2);
    QCOMPARE(dispatcher->all_renderers.get(uuid2)->render_update_count, 3);
    QCOMPARE(dispatcher->all_renderers.get(uuid3)->render_update_count, 2);
    QCOMPARE(dispatcher->all_renderers.get(uuid4)->render_update_count, 3);
}
