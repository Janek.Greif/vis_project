//
// Created by janek on 21.02.22.
//

#include "example_tests.h"

ParameterTest::ParameterTest(bool upper) : upper{upper} {

}

fn ParameterTest::doSomething() -> void {
    QString str{ "Hello" };
    if (upper) {
        QCOMPARE(str.toUpper(), "HELLO");
    } else {
        QCOMPARE(str.toLower(), "hello");
    }
}

fn SimpleTest::add() -> void{
    QCOMPARE(29 + 13, 42);
}
