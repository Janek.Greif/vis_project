//
// Created by janek on 22.02.22.
//

#ifndef VIS_PROJECT_UPDATEDISPATCHERTEST_H
#define VIS_PROJECT_UPDATEDISPATCHERTEST_H

#include <QObject>
#include "../src/oxidize.h"
#include "../src/volume/UpdateDispatcher.h"

class UpdateDispatcherTest : public QObject{
    Q_OBJECT
private slots:

    fn new_redraw_no_links() -> void;
    fn new_redraw_1_links() -> void;
    fn add_token_redraw_remove_token_redraw() -> void;
    fn multiple_tokens() -> void;

};


#endif //VIS_PROJECT_UPDATEDISPATCHERTEST_H
