//
// Created by janek on 21.02.22.
//

#include <QObject>
#include <QString>
#include <QTest>
#include <QHash>

#include "../src/oxidize.h"
#include "example_tests.h"
#include "UpdateDispatcherTest.h"
#include "TransferFunctionSamplerTest.h"

#define ADD_TEST(testName, className,...) allTests.insert(#testName, [](){ return new className{ __VA_ARGS__ };})
#define ADD_SIMPLE_TEST(className,...) ADD_TEST(className, className, __VA_ARGS__)

int main(int argc, char *argv[]) {

    QHash<QString, std::function<QObject*()>> allTests{};
    ADD_SIMPLE_TEST(SimpleTest);
    ADD_SIMPLE_TEST(UpdateDispatcherTest);
    ADD_SIMPLE_TEST(TransferFunctionSamplerTest);
    ADD_TEST(upper, ParameterTest, true);
    ADD_TEST(lower, ParameterTest, false);


    if (argc >= 2) {
        QString name{ argv[1] };
        if (name.startsWith("-T")) {
            let newArgc{ argc - 1 };
            let newArgv{ new char*[newArgc] };
            newArgv[0] = argv[0];
            for (auto i{ 1 }; i < newArgc; ++i) {
                newArgv[i] = argv[i+1];
            }
            let testName{ name.sliced(2) };
            if (allTests.contains(testName)) {
                return QTest::qExec(allTests[testName](), newArgc, newArgv);
            }
            qCritical() << "no test named" << testName;
            return 1;
        }
    }

    for (let& item : allTests) {
        if (let ret{ QTest::qExec(item(), argc, argv) }; ret != 0) {
            return ret;
        }
    }
    return 0;
}
