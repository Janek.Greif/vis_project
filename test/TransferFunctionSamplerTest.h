//
// Created by janek on 20.03.22.
//

#ifndef VIS_PROJECT_TRANSFERFUNCTIONSAMPLERTEST_H
#define VIS_PROJECT_TRANSFERFUNCTIONSAMPLERTEST_H

#include <QObject>
#include "../src/oxidize.h"

class TransferFunctionSamplerTest : public QObject{
    Q_OBJECT
private slots:
    fn sample_empty() -> void;
    fn sample_one_element() -> void;
    fn sample_two_element() -> void;
    fn sample_multiple_element() -> void;

    fn sample_empty_multi_seg() -> void;
    fn sample_one_element_multi_seg() -> void;
    fn sample_two_element_multi_seg() -> void;

    fn sample_multiple_element_multi_seg_overlapping() -> void;
};


#endif //VIS_PROJECT_TRANSFERFUNCTIONSAMPLERTEST_H
