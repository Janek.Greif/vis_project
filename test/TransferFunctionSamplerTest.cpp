//
// Created by janek on 20.03.22.
//

#include <range/v3/view/iota.hpp>
#include <qtestcase.h>
#include "TransferFunctionSamplerTest.h"
#include "../src/volume/TransferFunction.h"

#define QCOMPAREFUZZY_I(actual, expected, fuzz, i) QVERIFY2(std::abs(expected - actual) < fuzz, qPrintable(QString{"expected %1 got %2 at %3"}.arg(expected).arg(actual).arg(i)))
#define QCOMPAREFUZZY(actual, expected, fuzz) QVERIFY2(std::abs(expected - actual) < fuzz, qPrintable(QString{"expected %1 got %2"}.arg(expected).arg(actual)))

fn TransferFunctionSamplerTest::sample_empty() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    TransferFunction transFn{volTex, geoEnv, shaderEnv, 100, 100};
    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnData{};
    transFn.sample(transFnData);
    for (let i : ranges::views::ints(0, TRANS_FN_TEX_SIZE)) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
}

fn TransferFunctionSamplerTest::sample_one_element() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    TransferFunction transFn{volTex, geoEnv, shaderEnv, 100, 100};

    transFn.segments.emplace_back();
    transFn.segments[0].points.push_back({0.4, 0.2, 0});

    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnData{};
    transFn.sample(transFnData);
    for (let i : ranges::views::ints(0, TRANS_FN_TEX_SIZE)) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
}

fn TransferFunctionSamplerTest::sample_two_element() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    TransferFunction transFn{volTex, geoEnv, shaderEnv, 100, 100};

    transFn.segments.emplace_back();
    transFn.segments[0].points.push_back({0.25, 0.5, 0});
    transFn.segments[0].points.push_back({0.75, 0.5, 1});

    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnData{};
    transFn.sample(transFnData);
    for (let i : ranges::views::ints(0, static_cast<i32>(std::floor(0.25 * TRANS_FN_TEX_SIZE_F)))) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
    for (let i : ranges::views::ints(static_cast<i32>(std::ceil(0.25 * TRANS_FN_TEX_SIZE_F)), static_cast<i32>(std::floor(0.75 * TRANS_FN_TEX_SIZE_F)))) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.5);
    }
    for (let i : ranges::views::ints(static_cast<i32>(std::ceil(0.75 * TRANS_FN_TEX_SIZE_F)), TRANS_FN_TEX_SIZE)) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
}

fn TransferFunctionSamplerTest::sample_multiple_element() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    TransferFunction transFn{volTex, geoEnv, shaderEnv, 100, 100};

    transFn.segments.emplace_back();
    transFn.segments[0].color = QColor{255, 128, 0};
    transFn.segments[0].points.push_back({0.1, 0.0, 0});
    transFn.segments[0].points.push_back({0.4, 0.5, 1});
    transFn.segments[0].points.push_back({0.6, 0.6, 2});
    transFn.segments[0].points.push_back({0.9, 0.0, 3});

    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnData{};
    transFn.sample(transFnData);
    for (let i : ranges::views::ints(0, static_cast<i32>(std::floor(0.1 * TRANS_FN_TEX_SIZE_F)))) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
    auto start{ static_cast<i32>(std::ceil(0.1 * TRANS_FN_TEX_SIZE_F)) };
    auto end{ static_cast<i32>(std::floor(0.4 * TRANS_FN_TEX_SIZE_F)) };
    for (let i : ranges::views::ints(start, end)) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.0f, 0.5f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 0.5, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][3], alpha, 0.005);
    }
    start = static_cast<i32>(std::ceil(0.4 * TRANS_FN_TEX_SIZE_F));
    end = static_cast<i32>(std::floor(0.6 * TRANS_FN_TEX_SIZE_F));
    for (let i : ranges::views::ints(start, end)) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.5f, 0.6f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 0.5, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][3], alpha, 0.005);
    }
    start = static_cast<i32>(std::ceil(0.6 * TRANS_FN_TEX_SIZE_F));
    end = static_cast<i32>(std::floor(0.9 * TRANS_FN_TEX_SIZE_F));
    for (let i : ranges::views::ints(start, end)) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.6f, 0.0f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 0.5, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][3], alpha, 0.005);
    }
    for (let i : ranges::views::ints(static_cast<i32>(std::ceil(0.9 * TRANS_FN_TEX_SIZE_F)), TRANS_FN_TEX_SIZE)) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
}

// --------------------------------------------------------------------------------------------------------------

fn TransferFunctionSamplerTest::sample_empty_multi_seg() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    TransferFunction transFn{volTex, geoEnv, shaderEnv, 100, 100};
    transFn.segments.emplace_back();
    transFn.segments.emplace_back();
    transFn.segments.emplace_back();

    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnData{};
    transFn.sample(transFnData);
    for (let i : ranges::views::ints(0, TRANS_FN_TEX_SIZE)) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
}

fn TransferFunctionSamplerTest::sample_one_element_multi_seg() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    TransferFunction transFn{volTex, geoEnv, shaderEnv, 100, 100};

    transFn.segments.emplace_back();
    transFn.segments.emplace_back();
    transFn.segments.emplace_back();
    transFn.segments[0].points.push_back({0.4, 0.2, 0});
    transFn.segments[1].points.push_back({0.7, 0.6, 0});
    transFn.segments[2].points.push_back({0.4, 0.1, 0});

    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnData{};
    transFn.sample(transFnData);
    for (let i : ranges::views::ints(0, TRANS_FN_TEX_SIZE)) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
}

fn TransferFunctionSamplerTest::sample_two_element_multi_seg() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    TransferFunction transFn{volTex, geoEnv, shaderEnv, 100, 100};

    transFn.segments.emplace_back();
    transFn.segments.emplace_back();
    transFn.segments[0].color = QColor{0, 128, 255};
    transFn.segments[0].points.push_back({0.12, 0.2, 0});
    transFn.segments[0].points.push_back({0.34, 0.8, 1});
    transFn.segments[0].points.push_back({0.4, 0.0, 2});
    transFn.segments[1].color = QColor{0, 255, 128};
    transFn.segments[1].points.push_back({0.62, 0.4, 0});
    transFn.segments[1].points.push_back({0.84, 0.9, 1});

    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnData{};
    transFn.sample(transFnData);

    for (let i : ranges::views::ints(0, static_cast<i32>(std::floor(0.12 * TRANS_FN_TEX_SIZE_F)))) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
    auto start{ static_cast<i32>(std::ceil(0.12 * TRANS_FN_TEX_SIZE_F)) };
    auto end{ static_cast<i32>(std::floor(0.34 * TRANS_FN_TEX_SIZE_F)) };
    for (let i : ranges::views::ints(start, end)) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.2f, 0.8f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 0.5, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][3], alpha, 0.005);
    }
    start = static_cast<i32>(std::ceil(0.34 * TRANS_FN_TEX_SIZE_F)) ;
    end = static_cast<i32>(std::floor(0.4 * TRANS_FN_TEX_SIZE_F)) ;
    for (let i : ranges::views::ints(start, end)) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.8f, 0.0f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 0.5, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][3], alpha, 0.05); //TODO: why is it so inaccurate
    }
    for (let i : ranges::views::ints(static_cast<i32>(std::ceil(0.4 * TRANS_FN_TEX_SIZE_F)), static_cast<i32>(std::floor(0.62 * TRANS_FN_TEX_SIZE_F)))) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
    start = static_cast<i32>(std::ceil(0.62 * TRANS_FN_TEX_SIZE_F)) ;
    end = static_cast<i32>(std::floor(0.84 * TRANS_FN_TEX_SIZE_F)) ;
    for (let i : ranges::views::ints(start, end)) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.4f, 0.9f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 0.5, 0.005);
        QCOMPAREFUZZY(transFnData[i][3], alpha, 0.005);
    }
    for (let i : ranges::views::ints(static_cast<i32>(std::ceil(0.84 * TRANS_FN_TEX_SIZE_F)), TRANS_FN_TEX_SIZE)) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
}

fn TransferFunctionSamplerTest::sample_multiple_element_multi_seg_overlapping() -> void {
    let shaderEnv{  std::make_shared<ShaderEnvironment>() };
    auto volTex{ std::make_shared<VolumeTexture>(shaderEnv) };
    let geoEnv{  std::make_shared<GeometryEnvironment>() };
    TransferFunction transFn{volTex, geoEnv, shaderEnv, 100, 100};

    transFn.segments.emplace_back();
    transFn.segments.emplace_back();
    transFn.segments[0].color = QColor{0, 128, 255};
    transFn.segments[0].points.push_back({0.12, 0.2, 0});
    transFn.segments[0].points.push_back({0.34, 0.8, 1});
    transFn.segments[0].points.push_back({0.4, 0.0, 2});
    transFn.segments[1].color = QColor{255, 0, 128};
    transFn.segments[1].points.push_back({0.3, 0.1, 0});
    transFn.segments[1].points.push_back({0.5, 0.4, 1});
    transFn.segments[1].points.push_back({0.8, 0.9, 2});

    // segments 0...0.12...0.3...0.34...0.4...0.5...0.8...1.0

    let toStart{ [](f32 x){ return static_cast<i32>(std::ceil(x * TRANS_FN_TEX_SIZE_F)); } };
    let toEnd{ [](f32 x){ return static_cast<i32>(std::floor(x * TRANS_FN_TEX_SIZE_F)); } };

    std::array<std::array<f32, 4>, TRANS_FN_TEX_SIZE> transFnData{};
    transFn.sample(transFnData);
    for (let i : ranges::views::ints(0, static_cast<i32>(std::floor(0.12 * TRANS_FN_TEX_SIZE_F)))) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
    auto start{ toStart(0.12) };
    auto end{ toEnd(0.34) };
    for (let i : ranges::views::ints(toStart(0.12), toEnd(0.3))) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.2f, 0.8f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 0.5, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][3], alpha, 0.005);
    }
    start = toStart(0.12);
    end = toEnd(0.34);
    auto start2{ toStart(0.3) };
    auto end2{ toStart(0.5) };
    for (let i : ranges::views::ints(toStart(0.3), toEnd(0.34))) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let start2F{ static_cast<f32>(start2) };
        let end2F{ static_cast<f32>(end2) };
        let iF{ static_cast<f32>(i) };
        let alpha1{ std::lerp(0.2f, 0.8f, (iF - startF)/(endF - startF)) };
        let alpha2{ std::lerp(0.1f, 0.4f, (iF - start2F)/(end2F - start2F)) };
        f32 r1{0.0}, g1{0.5}, b1{1.0};
        f32 r2{1.0}, g2{0.0}, b2{0.5};
        QCOMPAREFUZZY(transFnData[i][0], (r1*alpha1 + r2*alpha2)/(alpha1+alpha2), 0.005);
        QCOMPAREFUZZY(transFnData[i][1], (g1*alpha1 + g2*alpha2)/(alpha1+alpha2), 0.005);
        QCOMPAREFUZZY(transFnData[i][2], (b1*alpha1 + b2*alpha2)/(alpha1+alpha2), 0.005);
        QCOMPAREFUZZY(transFnData[i][3], std::max(alpha1, alpha2), 0.005);
    }
    // TODO:
    start = toStart(0.34);
    end = toEnd(0.4);
    start2 = toStart(0.3);
    end2 = toStart(0.5);
    for (let i : ranges::views::ints(toStart(0.34), toEnd(0.4))) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let start2F{ static_cast<f32>(start2) };
        let end2F{ static_cast<f32>(end2) };
        let iF{ static_cast<f32>(i) };
        let alpha1{ std::lerp(0.8f, 0.0f, (iF - startF)/(endF - startF)) };
        let alpha2{ std::lerp(0.1f, 0.4f, (iF - start2F)/(end2F - start2F)) };
        f32 r1{0.0}, g1{0.5}, b1{1.0};
        f32 r2{1.0}, g2{0.0}, b2{0.5};
        QCOMPAREFUZZY(transFnData[i][0], (r1*alpha1 + r2*alpha2)/(alpha1+alpha2), 0.08); //TODO: why is it so inaccurate
        QCOMPAREFUZZY(transFnData[i][1], (g1*alpha1 + g2*alpha2)/(alpha1+alpha2), 0.05); //TODO: why is it so inaccurate
        QCOMPAREFUZZY(transFnData[i][2], (b1*alpha1 + b2*alpha2)/(alpha1+alpha2), 0.05); //TODO: why is it so inaccurate
        QCOMPAREFUZZY(transFnData[i][3], std::max(alpha1, alpha2), 0.05); //TODO: why is it so inaccurate
    }
    start = toStart(0.3);
    end = toEnd(0.5);
    for (let i : ranges::views::ints(toStart(0.4), toEnd(0.5))) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.1f, 0.4f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 0.5, 0.005);
        QCOMPAREFUZZY_I(transFnData[i][3], alpha, 0.005, iF/TRANS_FN_TEX_SIZE_F);
    }
    start = toStart(0.5);
    end = toEnd(0.8);
    for (let i : ranges::views::ints(start, end)) {
        let startF{ static_cast<f32>(start) };
        let endF{ static_cast<f32>(end) };
        let iF{ static_cast<f32>(i) };
        let alpha{ std::lerp(0.4f, 0.9f, (iF - startF)/(endF - startF)) };
        QCOMPAREFUZZY(transFnData[i][0], 1.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][1], 0.0, 0.005);
        QCOMPAREFUZZY(transFnData[i][2], 0.5, 0.005);
        QCOMPAREFUZZY(transFnData[i][3], alpha, 0.005);
    }
    for (let i : ranges::views::ints(static_cast<i32>(std::ceil(0.8 * TRANS_FN_TEX_SIZE_F)), TRANS_FN_TEX_SIZE)) {
        QCOMPARE(transFnData[i][0], 0.0);
        QCOMPARE(transFnData[i][1], 0.0);
        QCOMPARE(transFnData[i][2], 0.0);
        QCOMPARE(transFnData[i][3], 0.0);
    }
}
