//
// Created by janek on 21.02.22.
//

#ifndef VIS_PROJECT_EXAMPLE_TESTS_H
#define VIS_PROJECT_EXAMPLE_TESTS_H

#include <QObject>
#include <QTest>
#include "../src/oxidize.h"

class ParameterTest : public QObject{
    Q_OBJECT
public:
    ParameterTest(bool upper);

private slots:
    fn doSomething() -> void;

private:
    bool upper;
};

class SimpleTest : public QObject {
    Q_OBJECT

private slots:
    fn add() -> void;
};

#endif //VIS_PROJECT_EXAMPLE_TESTS_H
