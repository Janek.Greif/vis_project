#version 450

out vec4 fragmentColor;
in vec2 texCoord;
uniform mat4 viewProjection;
uniform mat4 inversViewProjection;
uniform sampler3D volumeTexture;
uniform vec3 volumeDimensions;
uniform vec3 volumeSpacing;

uniform sampler2D skipWhitespaceStart;
uniform sampler2D skipWhitespaceEnd;
uniform sampler1D transferFunction;
uniform float colorDensity = 10.0;
uniform float quality;

float calcDepth(vec3 pos) {
    vec4 clip_space_pos = viewProjection * vec4(pos, 1.0);
    float ndc_depth = clip_space_pos.z / clip_space_pos.w;
    return ndc_depth / 2.0 + 0.5;
}

void main() {
    float whitespaceStart = texture(skipWhitespaceStart, texCoord*0.5 + 0.5).r;
    float whitespaceEnd = texture(skipWhitespaceEnd, texCoord*0.5 + 0.5).r;
    if (whitespaceStart - whitespaceEnd > 0.0) {
        discard;
    }

    whitespaceStart = whitespaceStart*2.0-1.0;
    whitespaceEnd = whitespaceEnd*2.0-1.0;

    vec3 volSize = volumeSpacing * volumeDimensions;
    float m = max(max(volSize.x, volSize.y), volSize.z);
    vec3 normalizedVolSize = volSize / m;

    vec4 whitespaceStartPos_raw = inversViewProjection * vec4(texCoord, whitespaceStart, 1.0);
    vec3 whitespaceStartPos = whitespaceStartPos_raw.xyz / whitespaceStartPos_raw.w;
    vec4 whitespaceEndPos_raw = inversViewProjection * vec4(texCoord, whitespaceEnd, 1.0);
    vec3 whitespaceEndPos = whitespaceEndPos_raw.xyz / whitespaceEndPos_raw.w;

    vec3 whitespaceStartVolCoord = (whitespaceStartPos / normalizedVolSize / 2.0 + 0.5) * volumeDimensions;
    vec3 whitespaceEndVolCoord = (whitespaceEndPos / normalizedVolSize / 2.0 + 0.5) * volumeDimensions;

    vec3 ray_direction = whitespaceEndVolCoord-whitespaceStartVolCoord;
    vec3 step_direction = normalize(ray_direction) * quality;
    float alphaValue = 0.0;
    vec3 colorValue = vec3(0.0);

    vec3 ray_pos = whitespaceStartVolCoord;

    float nonWhitespacedistance = distance(whitespaceStartVolCoord, whitespaceEndVolCoord);

    vec3 maxValuePos = whitespaceStartVolCoord;

    uint c = 0;

    bool saturated = false;

    do {
        float rawValue = texture(volumeTexture, ray_pos / volumeDimensions).r;
        vec4 color = texture(transferFunction, rawValue);

        float alpha = color.a;
        vec3 preMultipliedColor = alpha * color.rgb;

        float correctedAlpha;
        if (alpha < 1.0) {
            correctedAlpha = 1.0 - pow(1.0 - alpha, length(step_direction) / colorDensity);
        } else {
            correctedAlpha = 1.0;
        }

        if (alpha >= 0.00000001) {
            preMultipliedColor = preMultipliedColor * (correctedAlpha / alpha);
        }
        alpha = correctedAlpha;


        float newAlphaValue = alphaValue + (1.0 - alphaValue)*alpha;
        if (newAlphaValue >= 1.0) {
            saturated = true;
            break;
        }
        colorValue = colorValue + (1.0 - alphaValue)*preMultipliedColor;
        alphaValue = newAlphaValue;
        ray_pos += step_direction;
        c++;
    } while(nonWhitespacedistance > distance(whitespaceStartVolCoord, ray_pos) && c < 3000 / quality);
    if (colorValue == vec3(0.0)) {
        discard;
    }

    fragmentColor = vec4(colorValue, alphaValue);
    gl_FragDepth = calcDepth((maxValuePos / volumeDimensions - 0.5) * 2.0 * normalizedVolSize);
}
