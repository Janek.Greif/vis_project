#version 450

out vec4 fragmentColor;
in vec2 texCoord;
uniform sampler2D screenTexture;
uniform sampler2D depthTexture;

void main() {
    fragmentColor = texture(screenTexture, texCoord);
    gl_FragDepth = texture(depthTexture, texCoord).r;
}
