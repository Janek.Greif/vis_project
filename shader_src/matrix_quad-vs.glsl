#version 450

in vec4 vertexPosition;
out vec2 texCoord;

uniform mat4 matrix;

void main() {
    gl_Position = matrix * vertexPosition;
    texCoord = (vertexPosition.xy+1.0)/2.0;
}
