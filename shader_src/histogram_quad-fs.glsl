#version 450

out vec4 fragmentColor;
in vec2 texCoord;
uniform sampler1D histogram;
uniform vec4 color;

void main() {
    float h = texture(histogram, texCoord.x).r;
    float y = texCoord.y;
    y *= y * y;
    if (y < h) {
        fragmentColor = color;
    } else {
        discard;
    }
}
