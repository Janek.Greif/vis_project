#version 450

out vec4 fragmentColor;
in vec3 texCoord;

uniform sampler3D volumeTexture;
uniform sampler1D transferFunction;

void main() {
    if (texCoord.x < 0.0 || texCoord.x >= 1.0 || texCoord.y < 0.0 || texCoord.y >= 1.0) {
        discard;
    }
    float value = texture(volumeTexture, texCoord).r;
    vec4 color = texture(transferFunction, value);
    if (color.a < 0.0001) {
        discard;
    }
    fragmentColor = vec4(color.rgb * color.a, color.a);
}
