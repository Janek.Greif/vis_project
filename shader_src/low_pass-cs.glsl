#version 450
layout(local_size_x=1, local_size_y=1, local_size_z=1) in;

layout(rgba32f, binding = 0) uniform image3D inputVolumeImage;
layout(rgba32f, binding = 1) uniform image3D outputVolumeImage;

const float[27] kernel = {
    0.037037, 0.037037, 0.037037,
    0.037037, 0.037037, 0.037037,
    0.037037, 0.037037, 0.037037,

    0.037037, 0.037037, 0.037037,
    0.037037, 0.037037, 0.037037,
    0.037037, 0.037037, 0.037037,

    0.037037, 0.037037, 0.037037,
    0.037037, 0.037037, 0.037037,
    0.037037, 0.037037, 0.037037
};

ivec3 offset(ivec3 p, ivec3 o) {
    return clamp(p + (o-ivec3(4)), ivec3(0,0,0), imageSize(inputVolumeImage)-1);
}

vec4 blur(ivec3 coord) {
    vec4 v = vec4(0.0);
    for (int z = 0; z < 7; ++z) {
        for (int y = 0; y < 7; ++y) {
            for (int x = 0; x < 7; ++x) {
                vec4 rawValue = imageLoad(inputVolumeImage, coord + ivec3(x,y,z) - 3);
                v += 0.0029154518950437317 * rawValue;
            }
        }
    }
    return v;
}

void main() {
    ivec3 target = ivec3(gl_GlobalInvocationID);

    if (target.x >= imageSize(outputVolumeImage).x || target.y >= imageSize(outputVolumeImage).y || target.z >= imageSize(outputVolumeImage).z) {
        return;
    }

    // the global invocation id points to the target image (which is reduces in size by 3 in all dimensions)
    // now the coord refers to the original image
    ivec3 coord = target *  3;

    vec4[27] values = {
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
        vec4(0.0),
    };

    for (int z = 0; z < 9 ; ++z) {
        for (int y = 0; y < 9 ; ++y) {
            for (int x = 0; x < 9 ; ++x) {

                vec4 value = 0.0029154518950437317 * imageLoad(inputVolumeImage, offset(coord, ivec3(x,y,z)));
                if (z == 0 && y == 0 && x == 0) {
                    values[0] += value;
                } else if (z == 0 && y == 0 && x == 8) {
                    values[2] += value;
                } else if (z == 0 && y == 8 && x == 0) {
                    values[3*2] += value;
                } else if (z == 8 && y == 0 && x == 0) {
                    values[9*2] += value;
                } else if (z == 0 && y == 8 && x == 8) {
                    values[3*2 + 2] += value;
                } else if (z == 8 && y == 0 && x == 8) {
                    values[9*2 + 2] += value;
                } else if (z == 8 && y == 8 && x == 0) {
                    values[9*2 + 3*2] += value;
                } else if (z == 8 && y == 8 && x == 8) {
                    values[9*2 + 3*2 + 2] += value;

                } else if (z == 0 && y == 0) {
                    values[0] += value;
                    values[1] += value;
                    values[2] += value;
                } else if (z == 0 && x == 0) {
                    values[3*0] += value;
                    values[3*1] += value;
                    values[3*2] += value;
                } else if (y == 0 && x == 0) {
                    values[9*0] += value;
                    values[9*1] += value;
                    values[9*2] += value;
                } else if (z == 8 && y == 8) {
                    values[9*2 + 3*2 + 0] += value;
                    values[9*2 + 3*2 + 1] += value;
                    values[9*2 + 3*2 + 2] += value;
                } else if (z == 8 && x == 8) {
                    values[9*2 + 3*0 + 2] += value;
                    values[9*2 + 3*1 + 2] += value;
                    values[9*2 + 3*2 + 2] += value;
                } else if (y == 8 && x == 8) {
                    values[9*0 + 3*2 + 2] += value;
                    values[9*1 + 3*2 + 2] += value;
                    values[9*2 + 3*2 + 2] += value;

                } else if (z == 0 && y == 8) {
                    values[3*2 + 0] += value;
                    values[3*2 + 1] += value;
                    values[3*2 + 2] += value;
                } else if (z == 0 && x == 8) {
                    values[3*0 + 2] += value;
                    values[3*1 + 2] += value;
                    values[3*2 + 2] += value;
                } else if (y == 0 && x == 8) {
                    values[9*0 + 2] += value;
                    values[9*1 + 2] += value;
                    values[9*2 + 2] += value;
                } else if (y == 0 && z == 8) {
                    values[9*2 + 0] += value;
                    values[9*2 + 1] += value;
                    values[9*2 + 2] += value;
                } else if (x == 0 && y == 8) {
                    values[9*0 + 3*2] += value;
                    values[9*1 + 3*2] += value;
                    values[9*2 + 3*2] += value;
                } else if (x == 0 && z == 8) {
                    values[9*2 + 3*0] += value;
                    values[9*2 + 3*1] += value;
                    values[9*2 + 3*2] += value;

                } else if (z == 0) {
                    values[3*0 + 0] += value;
                    values[3*0 + 1] += value;
                    values[3*0 + 2] += value;

                    values[3*1 + 0] += value;
                    values[3*1 + 1] += value;
                    values[3*1 + 2] += value;

                    values[3*2 + 0] += value;
                    values[3*2 + 1] += value;
                    values[3*2 + 2] += value;
                } else if (y == 0) {
                    values[9*0 + 0] += value;
                    values[9*0 + 1] += value;
                    values[9*0 + 2] += value;

                    values[9*1 + 0] += value;
                    values[9*1 + 1] += value;
                    values[9*1 + 2] += value;

                    values[9*2 + 0] += value;
                    values[9*2 + 1] += value;
                    values[9*2 + 2] += value;
                } else if (x == 0) {
                    values[9*0 + 3*0] += value;
                    values[9*0 + 3*1] += value;
                    values[9*0 + 3*2] += value;

                    values[9*1 + 3*0] += value;
                    values[9*1 + 3*1] += value;
                    values[9*1 + 3*2] += value;

                    values[9*2 + 3*0] += value;
                    values[9*2 + 3*1] += value;
                    values[9*2 + 3*2] += value;
                } else if (z == 8) {
                    values[9*2 + 3*0 + 0] += value;
                    values[9*2 + 3*0 + 1] += value;
                    values[9*2 + 3*0 + 2] += value;

                    values[9*2 + 3*1 + 0] += value;
                    values[9*2 + 3*1 + 1] += value;
                    values[9*2 + 3*1 + 2] += value;

                    values[9*2 + 3*2 + 0] += value;
                    values[9*2 + 3*2 + 1] += value;
                    values[9*2 + 3*2 + 2] += value;
                } else if (y == 8) {
                    values[9*0 + 3*2 + 0] += value;
                    values[9*0 + 3*2 + 1] += value;
                    values[9*0 + 3*2 + 2] += value;

                    values[9*1 + 3*2 + 0] += value;
                    values[9*1 + 3*2 + 1] += value;
                    values[9*1 + 3*2 + 2] += value;

                    values[9*2 + 3*2 + 0] += value;
                    values[9*2 + 3*2 + 1] += value;
                    values[9*2 + 3*2 + 2] += value;
                } else if (x == 8) {
                    values[9*0 + 3*0 + 2] += value;
                    values[9*0 + 3*1 + 2] += value;
                    values[9*0 + 3*2 + 2] += value;

                    values[9*1 + 3*0 + 2] += value;
                    values[9*1 + 3*1 + 2] += value;
                    values[9*1 + 3*2 + 2] += value;

                    values[9*2 + 3*0 + 2] += value;
                    values[9*2 + 3*1 + 2] += value;
                    values[9*2 + 3*2 + 2] += value;
                } else {
                    for (int i = 0; i < 27; ++i) {
                        values[i] += value;
                    }
                }
            }
        }
    }


    vec4 v = vec4(0.0);
    for (int i = 0; i < 27; ++i) {
        if (values[i].a > v.a) {
            v = values[i];
        }
    }
    if (v.a < 0.01) {
        imageStore(outputVolumeImage, target, vec4(0.0, 0.0, 0.0, v.a));
    } else {
        imageStore(outputVolumeImage, target, v);
    }
}
