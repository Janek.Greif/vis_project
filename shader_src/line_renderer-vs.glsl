#version 450

in vec4 vertexPosition;
uniform mat4 viewProjection;

uniform vec3 volSize;

void main() {
    gl_Position = viewProjection * (vertexPosition * vec4(volSize, 1.0));
}
