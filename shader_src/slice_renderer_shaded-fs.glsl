#version 450

out vec4 fragmentColor;
in vec3 texCoord;

uniform sampler3D volumeTexture;
uniform sampler3D gradientTexture;
uniform sampler1D transferFunction;
uniform vec3 volumeDimensions;
uniform vec3 volumeSpacing;

uniform vec3 cameraPos;
uniform vec3 lightPos;
uniform float lightPower;
uniform float ambientF;
uniform float diffuseF;
uniform float specularF;
uniform float shininess;

vec3 shadeColor(vec4 gradient, vec4 v, vec3 pos, vec3 normalizedVolSize) {
    if (gradient.a > 0.005) {
        vec3 normal = normalize(gradient.xyz);

        vec3 currentCoord = (pos / volumeDimensions - 0.5) * 2.0 * normalizedVolSize;
        vec3 lightDir = normalize(lightPos - currentCoord);
        float distance = distance(lightPos, currentCoord);
        distance = distance * distance;

        float lambertian = max(dot(lightDir, normal), 0.0);
        vec3 diffuse = v.rgb * v.a;
        vec3 specularColor = v.rgb;
        vec3 halfDir = normalize(lightDir + normalize(-cameraPos));
        float specAngle = max(dot(halfDir, normal), 0.0);
        float specular = pow(specAngle, shininess);

        vec3 color = diffuse * ambientF + diffuse * lambertian * diffuseF * lightPower / distance + specularColor * specular * specularF * lightPower / distance;

        return color;
    }
    return vec3(0.0);
}

void main() {
    if (texCoord.x < 0.0 || texCoord.x >= 1.0 || texCoord.y < 0.0 || texCoord.y >= 1.0) {
        discard;
    }
    float value = texture(volumeTexture, texCoord).r;
    vec4 color = texture(transferFunction, value);
    if (color.a < 0.0001) {
        discard;
    }

    vec3 volSize = volumeSpacing * volumeDimensions;
    float m = max(max(volSize.x, volSize.y), volSize.z);
    vec3 normalizedVolSize = volSize / m;

    vec4 gradient = texture(gradientTexture, texCoord);
    fragmentColor = vec4(shadeColor(gradient, color, gl_FragCoord.xyz, normalizedVolSize), color.a);
//    fragmentColor = vec4(color.rgb * color.a, color.a);
}
