#version 450

out vec4 fragmentColor;
in vec2 texCoord;
uniform mat4 viewProjection;
uniform mat4 inversViewProjection;
uniform sampler3D volumeTexture;
uniform sampler3D gradientTexture;
uniform vec3 volumeDimensions;
uniform vec3 volumeSpacing;

uniform sampler2D skipWhitespaceStart;
uniform sampler2D skipWhitespaceEnd;
uniform sampler1D transferFunction;
uniform float quality;

uniform vec3 cameraPos;
uniform vec3 lightPos;
uniform float lightPower;
uniform float ambientF;
uniform float diffuseF;
uniform float specularF;
uniform float shininess;

float calcDepth(vec3 pos) {
    vec4 clip_space_pos = viewProjection * vec4(pos, 1.0);
    float ndc_depth = clip_space_pos.z / clip_space_pos.w;
    return ndc_depth / 2.0 + 0.5;
}

vec3 shadeColor(vec4 gradient, vec4 v, vec3 pos, vec3 normalizedVolSize) {
    if (gradient.a > 0.005) {
        vec3 normal = normalize(gradient.xyz);

        vec3 currentCoord = (pos / volumeDimensions - 0.5) * 2.0 * normalizedVolSize;
        vec3 lightDir = normalize(lightPos - currentCoord);
        float distance = distance(lightPos, currentCoord);
        distance = distance * distance;

        float lambertian = max(dot(lightDir, normal), 0.0);
        vec3 diffuse = v.rgb * v.a;
        vec3 specularColor = v.rgb;
        vec3 halfDir = normalize(lightDir + normalize(-cameraPos));
        float specAngle = max(dot(halfDir, normal), 0.0);
        float specular = pow(specAngle, shininess);

        vec3 color = diffuse * ambientF + diffuse * lambertian * diffuseF * lightPower / distance + specularColor * specular * specularF * lightPower / distance;

        return color;
    }
    return vec3(0.0);
}

void main() {

    float whitespaceStart = texture(skipWhitespaceStart, texCoord*0.5 + 0.5).r;
    float whitespaceEnd = texture(skipWhitespaceEnd, texCoord*0.5 + 0.5).r;
    if (whitespaceStart - whitespaceEnd > 0.0) {
        discard;
    }

    whitespaceStart = whitespaceStart*2.0-1.0;
    whitespaceEnd = whitespaceEnd*2.0-1.0;

    vec3 volSize = volumeSpacing * volumeDimensions;
    float m = max(max(volSize.x, volSize.y), volSize.z);
    vec3 normalizedVolSize = volSize / m;

    vec4 whitespaceStartPos_raw = inversViewProjection * vec4(texCoord, whitespaceStart, 1.0);
    vec3 whitespaceStartPos = whitespaceStartPos_raw.xyz / whitespaceStartPos_raw.w;
    vec4 whitespaceEndPos_raw = inversViewProjection * vec4(texCoord, whitespaceEnd, 1.0);
    vec3 whitespaceEndPos = whitespaceEndPos_raw.xyz / whitespaceEndPos_raw.w;

    vec3 whitespaceStartVolCoord = (whitespaceStartPos / normalizedVolSize / 2.0 + 0.5) * volumeDimensions;
    vec3 whitespaceEndVolCoord = (whitespaceEndPos / normalizedVolSize / 2.0 + 0.5) * volumeDimensions;

    vec3 ray_direction = whitespaceEndVolCoord-whitespaceStartVolCoord;
    vec3 step_direction = normalize(ray_direction) * quality;

    vec3 ray_pos = whitespaceStartVolCoord;

    float nonWhitespacedistance = distance(whitespaceStartVolCoord, whitespaceEndVolCoord);

    vec3 maxValuePos = whitespaceStartVolCoord;

    vec3 value = vec3(0.0);
    uint sample_count = 0u;

    uint c = 0;

    do {
        float rawValue = texture(volumeTexture, ray_pos / volumeDimensions).r;
        vec4 v = texture(transferFunction, rawValue);
        if (v.a > 0.02) {
            vec4 gradient = texture(gradientTexture, clamp(ray_pos / volumeDimensions, 0.0, 1.0));
            value += shadeColor(gradient, v, ray_pos, normalizedVolSize);
            sample_count++;
        }
        ray_pos += step_direction;
        c++;
    } while(nonWhitespacedistance > distance(whitespaceStartVolCoord, ray_pos) && c < 3000 / quality);
    if (value == vec3(0.0)) {
        discard;
    }

    fragmentColor = vec4(vec3(value/float(sample_count)), 1.0);
    gl_FragDepth = calcDepth((maxValuePos / volumeDimensions - 0.5) * 2.0 * normalizedVolSize);
}
