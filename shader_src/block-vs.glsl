#version 450

layout(rgba32f, binding = 0) uniform image1D blockPosition;

uniform mat4 modelViewProjectionMatrix;
uniform vec3 blockSize;
uniform vec3 volumeSpacing;
in vec4 vertexPosition;

void main(void)
{
    vec3 bP = imageLoad(blockPosition, gl_InstanceID).xyz * volumeSpacing;
    vec3 blockCoordinates = bP+(vertexPosition.xyz+vec3(1.0))*vec3(0.5)*blockSize;
    gl_Position = modelViewProjectionMatrix*vec4(blockCoordinates,1.0);
}

