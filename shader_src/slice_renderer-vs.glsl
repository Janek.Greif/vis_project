#version 450

in vec4 vertexPosition;
out vec3 texCoord;

uniform mat4 viewProjection;
uniform vec3 volSize;
uniform vec3 center;
uniform vec3 globalCenter;
uniform vec3 scale;


void main() {
    gl_Position = viewProjection * (vertexPosition * vec4(volSize, 1.0));
    texCoord = (vertexPosition.xyz + globalCenter / volSize) / scale * 0.5 + 0.5;
}
