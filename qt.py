import gdb
import re
import time
import math

class FunctionLookup:

    def __init__(self, gdb, pretty_printers_dict):
        self.gdb = gdb
        self.pretty_printers_dict = pretty_printers_dict

    def __call__(self, val):
        """Look-up and return a pretty-printer that can print val."""

        # Get the type.
        type = val.type

        # If it points to a reference, get the reference.
        if type.code == self.gdb.TYPE_CODE_REF:
            type = type.target()

        # Get the unqualified type, stripped of typedefs.
        type = type.unqualified().strip_typedefs()

        # Get the type name.
        typename = type.tag
        if typename is None:
            typename = str(type)

        # Iterate over local dictionary of types to determine
        # if a printer is registered for that type.  Return an
        # instantiation of the printer if found.
        for function in self.pretty_printers_dict:
            if function.search(typename):
                return self.pretty_printers_dict[function](val)

        # Cannot find a pretty printer.  Return None.
        return None


# END

class QStringPrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        size = self.val['d']['size']
        ret = ""

        # The QString object might be not yet initialized. In this case size is a bogus value
        # and the following 2 lines might throw memory access error. Hence the try/catch.
        try:
            data_ptr = (self.val['d']['ptr']).cast(gdb.lookup_type("char").pointer())
            ret = data_ptr.string(encoding='UTF-16', length=size * 2)
        except Exception as e:
            # swallow the exception and return empty string
            print(str(e))
            ret = "-[[error looking up string]]-"
            pass
        return ret

    def display_hint(self):
        return 'string'


class QByteArrayPrinter:
    def __init__(self, val):
        self.val = val
        self.size = self.val['d']['size']
        self.ptr = self.val['d']['ptr'].cast(gdb.lookup_type("char").pointer())
        self.list = list(map(lambda i: (self.ptr + i).dereference(), range(0, min(self.size, 30))))

        def children():
            return map(lambda v: ('[%d]' % v[0], v[1]), enumerate(self.list))

        if self.size > 30:
            setattr(self, 'children', children)

    def to_string(self):
        return " ".join(list(map(lambda i: i.format_string(format='x')[2:], self.list)))

    def display_hint(self):
        return 'string'


class QListPrinter:
    """Print a QList"""

    class _iterator:
        def __init__(self, node_type, d):
            self.node_type = node_type
            self.d = d
            self.count = 0

            # from QTypeInfo::isLarge
            is_large = self.node_type.sizeof > gdb.lookup_type('void').pointer().sizeof

            is_pointer = self.node_type.code == gdb.TYPE_CODE_PTR

            # unfortunately we can't use QTypeInfo<T>::isStatic as it's all inlined, so use
            # this list of types that use Q_DECLARE_TYPEINFO(T, Q_MOVABLE_TYPE)
            # (obviously it won't work for custom types)
            movable_types = ['QRect', 'QRectF', 'QString', 'QMargins', 'QLocale', 'QChar', 'QDate', 'QTime', 'QDateTime',
                            'QVector',
                            'QRegExpr', 'QPoint', 'QPointF', 'QByteArray', 'QSize', 'QSizeF', 'QBitArray', 'QLine',
                            'QLineF', 'QModelIndex', 'QPersitentModelIndex',
                            'QVariant', 'QFileInfo', 'QUrl', 'QXmlStreamAttribute', 'QXmlStreamNamespaceDeclaration',
                            'QXmlStreamNotationDeclaration',
                            'QXmlStreamEntityDeclaration', 'QPair<int, int>']
            # this list of types that use Q_DECLARE_TYPEINFO(T, Q_PRIMITIVE_TYPE) (from qglobal.h)
            primitive_types = ['bool', 'char', 'signed char', 'unsigned char', 'short', 'unsigned short', 'int',
                              'unsigned int', 'long', 'unsigned long', 'long long', 'unsigned long long', 'float',
                              'double']

            if movable_types.count(self.node_type.tag) or primitive_types.count(str(self.node_type)):
                is_static = False
            else:
                is_static = not is_pointer

            self.externalStorage = is_large or is_static  # see QList::Node::t()

        def __iter__(self):
            return self

        def __next__(self):
            if self.count >= self.d['size']:
                raise StopIteration
            count = self.count
            array = self.d['ptr'] + count
            # if self.externalStorage:
            #     value = array.cast(self.nodetype)
            # else:
            #     value = array
            self.count = self.count + 1
            return '[%d]' % count, array.cast(self.node_type.pointer()).dereference()

    def __init__(self, val, container, itype):
        self.d = val['d']
        self.container = container
        self.size = self.d['size']
        if itype is None:
            self.itype = val.type.template_argument(0)
        else:
            self.itype = gdb.lookup_type(itype)

    def children(self):
        return self._iterator(self.itype, self.d)

    def to_string(self):
        return "%s<%s> (size = %s)" % (self.container, self.itype, self.size)


class QMapPrinter:
    "Print a QMap"

    def __init__(self, val, container):
        self.v = val
        self.val = val['d']['d']
        self.con = container

    def children(self):
        return [("", self.val['m'])]

    def to_string(self):
        return "%s<%s, %s>" % (self.con, self.v.type.template_argument(0), self.v.type.template_argument(1))

    def display_hint(self):
        return 'array'


class QHashPrinter:
    "Print a QHash"
    NEntries = 128
    LocalBucketMask = NEntries - 1
    UnusedEntry = 255

    def __init__(self, val, container, node_ptr_type):
        self.val = val
        self.ktype = self.val.type.template_argument(0)
        self.vtype = self.val.type.template_argument(1)
        self.container = container
        self.node_ptr_str = node_ptr_type

    def iter_nodes(self):
        node_ptr = gdb.lookup_type(self.node_ptr_str % (self.ktype, self.vtype)).pointer()

        d = self.val['d']
        num_buckets = int(d['numBuckets'])
        data_ptr = d.cast(gdb.lookup_type('%s<%s, %s>::Data' % (self.container, self.ktype, self.vtype)).pointer())
        spans = data_ptr['spans']

        count = 0
        for s in range(0, math.floor((num_buckets + QHashPrinter.LocalBucketMask) / QHashPrinter.NEntries)):
            span = spans[s]
            span_start = span['offsets']
            entries_start = span['entries']
            for o in range(0, QHashPrinter.NEntries):
                offset = int(span_start[o])
                if offset != QHashPrinter.UnusedEntry:
                    storage = entries_start[offset]['storage']
                    real_entry = storage.address.reinterpret_cast(node_ptr).dereference()
                    key = real_entry['key']
                    value = real_entry['value']
                    idx1 = '[%d]' % count
                    idx2 = '[%d]' % (count + 1)
                    yield idx1, key
                    yield idx2, value
                    count += 2

    def children(self):
        return self.iter_nodes()

    def to_string(self):
        size = self.val['d']['size']
        return "%s<%s, %s> (size = %s)" % (
            self.container, self.val.type.template_argument(0), self.val.type.template_argument(1), size)

    def display_hint(self):
        return 'map'


class QMultiNodeChainPrinter:
    def __init__(self, val):
        self.val = val

    def children(self):
        c = []
        value = self.val
        n = value['next']
        c.append(("[0]", value['value']))
        count = 1
        while int(n) != 0:
            value = n
            n = value['next']
            c.append(('[%d]' % count, value['value']))
            count += 1
        return c

    def display_hint(self):
        return 'array'


class QDatePrinter:
    def __init__(self, val):
        self.val = val

    def to_string(self):
        julianDay = self.val['jd']

        if julianDay == 0:
            return "invalid QDate"

        # Copied from Qt sources
        if julianDay >= 2299161:
            # Gregorian calendar starting from October 15, 1582
            # This algorithm is from Henry F. Fliegel and Thomas C. Van Flandern
            ell = julianDay + 68569
            n = (4 * ell) / 146097
            ell = ell - (146097 * n + 3) / 4
            i = (4000 * (ell + 1)) / 1461001
            ell = ell - (1461 * i) / 4 + 31
            j = (80 * ell) / 2447
            d = ell - (2447 * j) / 80
            ell = j / 11
            m = j + 2 - (12 * ell)
            y = 100 * (n - 49) + i + ell
        else:
            # Julian calendar until October 4, 1582
            # Algorithm from Frequently Asked Questions about Calendars by Claus Toendering
            julianDay += 32082
            dd = (4 * julianDay + 3) / 1461
            ee = julianDay - (1461 * dd) / 4
            mm = ((5 * ee) + 2) / 153
            d = ee - (153 * mm + 2) / 5 + 1
            m = mm + 3 - 12 * (mm / 10)
            y = dd - 4800 + (mm / 10)
            if y <= 0:
                y -= 1  # --y does nothing in py, since i assume it was copied form c++ i changed to to y -= 1
        return "%d-%02d-%02d" % (y, m, d)


class QTimePrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        ds = self.val['mds']

        if ds == -1:
            return "invalid QTime"

        MSECS_PER_HOUR = 3600000
        SECS_PER_MIN = 60
        MSECS_PER_MIN = 60000

        hour = ds / MSECS_PER_HOUR
        minute = (ds % MSECS_PER_HOUR) / MSECS_PER_MIN
        second = (ds / 1000) % SECS_PER_MIN
        msec = ds % 1000
        return "%02d:%02d:%02d.%03d" % (hour, minute, second, msec)


class QDateTimePrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        time_t = gdb.parse_and_eval("reinterpret_cast<const QDateTime*>(%s)->toSecsSinceEpoch()" % self.val.address)
        return time.ctime(int(time_t))


class QUrlPrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        # first try to access the Qt 5 data
        try:
            int_type = gdb.lookup_type('int')
            string_type = gdb.lookup_type('QString')
            string_pointer = string_type.pointer()

            addr = self.val['d'].cast(gdb.lookup_type('char').pointer())
            if not addr:
                return "<invalid>"
            # skip QAtomicInt ref
            addr += int_type.sizeof
            # handle int port
            port = addr.cast(int_type.pointer()).dereference()
            addr += int_type.sizeof
            # handle QString scheme
            scheme = QStringPrinter(addr.cast(string_pointer).dereference()).to_string()
            addr += string_type.sizeof
            # handle QString username
            username = QStringPrinter(addr.cast(string_pointer).dereference()).to_string()
            addr += string_type.sizeof
            # skip QString password
            addr += string_type.sizeof
            # handle QString host
            host = QStringPrinter(addr.cast(string_pointer).dereference()).to_string()
            addr += string_type.sizeof
            # handle QString path
            path = QStringPrinter(addr.cast(string_pointer).dereference()).to_string()
            addr += string_type.sizeof
            # handle QString query
            query = QStringPrinter(addr.cast(string_pointer).dereference()).to_string()
            addr += string_type.sizeof
            # handle QString fragment
            fragment = QStringPrinter(addr.cast(string_pointer).dereference()).to_string()

            url = ""
            if len(scheme) > 0:
                # always adding // is apparently not compliant in all cases
                url += scheme + "://"
            if len(host) > 0:
                if len(username) > 0:
                    url += username + "@"
                url += host
                if port != -1:
                    url += ":" + str(port)
            url += path
            if len(query) > 0:
                url += "?" + query
            if len(fragment) > 0:
                url += "#" + fragment

            return url
        except:
            pass
        # then try to print directly, but that might lead to issues (see http://sourceware-org.1504.n7.nabble.com/help-Calling-malloc-from-a-Python-pretty-printer-td284031.html)
        try:
            return gdb.parse_and_eval(
                "reinterpret_cast<const QUrl*>(%s)->toString((QUrl::FormattingOptions)QUrl::PrettyDecoded)" % self.val.address)
        except:
            pass
        # if everything fails, maybe we deal with Qt 4 code
        try:
            return self.val['d']['encodedOriginal']
        except RuntimeError:
            # if no debug information is available for Qt, try guessing the correct address for encodedOriginal
            # problem with this is that if QUrlPrivate members get changed, this fails
            offset = gdb.lookup_type('int').sizeof
            offset += offset % gdb.lookup_type('void').pointer().sizeof  # alignment
            offset += gdb.lookup_type('QString').sizeof * 6
            offset += gdb.lookup_type('QByteArray').sizeof
            encodedOriginal = self.val['d'].cast(gdb.lookup_type('char').pointer());
            encodedOriginal += offset
            encodedOriginal = encodedOriginal.cast(gdb.lookup_type('QByteArray').pointer()).dereference();
            encodedOriginal = encodedOriginal['d']['data'].string()
            return encodedOriginal


class QSetPrinter:
    "Print a QSet"

    def __init__(self, val):
        self.val = val
        self.vtype = self.val.type.template_argument(0)

    def iter_nodes(self):
        node_ptr = gdb.lookup_type("QHashPrivate::Node<%s, QHashDummyValue>" % self.vtype).pointer()

        d = self.val['q_hash']['d']
        num_buckets = int(d['numBuckets'])
        data_ptr = d.cast(gdb.lookup_type('QHash<%s, QHashDummyValue>::Data' % self.vtype).pointer())
        spans = data_ptr['spans']

        count = 0
        for s in range(0, math.floor((num_buckets + QHashPrinter.LocalBucketMask) / QHashPrinter.NEntries)):
            span = spans[s]
            span_start = span['offsets']
            entries_start = span['entries']
            for o in range(0, QHashPrinter.NEntries):
                offset = int(span_start[o])
                if offset != QHashPrinter.UnusedEntry:
                    storage = entries_start[offset]['storage']
                    real_entry = storage.address.reinterpret_cast(node_ptr).dereference()
                    key = real_entry['key']
                    idx = '[%d]' % count
                    count += 1
                    yield idx, key

    def children(self):
        return self.iter_nodes()

    def to_string(self):
        size = self.val['q_hash']['d']['size']

        return "QSet<%s> (size = %s)" % (self.val.type.template_argument(0), size)

    def display_hint(self):
        return "array"


class QCharPrinter:
    def __init__(self, val):
        self.val = val

    def to_string(self):
        return chr(self.val['ucs'])

    def display_hint(self):
        return 'string'


class QUuidPrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        return "QUuid({%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x})" % (
            int(self.val['data1']), int(self.val['data2']), int(self.val['data3']),
            int(self.val['data4'][0]), int(self.val['data4'][1]),
            int(self.val['data4'][2]), int(self.val['data4'][3]),
            int(self.val['data4'][4]), int(self.val['data4'][5]),
            int(self.val['data4'][6]), int(self.val['data4'][7]))

    def display_hint(self):
        return 'string'


class QVariantPrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        d = self.val['d']

        if d['is_null']:
            return "QVariant(NULL)"

        data_type = d['type']
        type_str = ("type = %d" % data_type)
        try:
            typeAsCharPointer = (
                gdb.parse_and_eval("QVariant::typeToName(%d)" % data_type).cast(gdb.lookup_type("char").pointer()))
            if typeAsCharPointer:
                type_str = typeAsCharPointer.string(encoding='UTF-8')
        except Exception as e:
            pass

        data = d['data']
        is_shared = d['is_shared']
        value_str = ""
        if is_shared:
            private_shared = data['shared'].dereference()
            value_str = "PrivateShared(%s)" % hex(private_shared['ptr'])
        elif type_str.startswith("type = "):
            value_str = str(data['ptr'])
        else:
            type_obj = None
            try:
                type_obj = gdb.lookup_type(type_str)
            except Exception as e:
                value_str = str(data['ptr'])
            if type_obj:
                if type_obj.sizeof > type_obj.pointer().sizeof:
                    value_ptr = data['ptr'].reinterpret_cast(type_obj.const().pointer())
                    value_str = str(value_ptr.dereference())
                else:
                    value_ptr = data['c'].address.reinterpret_cast(type_obj.const().pointer())
                    value_str = str(value_ptr.dereference())

        return "QVariant(%s, %s)" % (type_str, value_str)


pretty_printers_dict = {}


def register_qt_printers():
    gdb.pretty_printers.append(FunctionLookup(gdb, pretty_printers_dict))


def build_dictionary():
    pretty_printers_dict[re.compile('^QString$')] = lambda val: QStringPrinter(val)
    pretty_printers_dict[re.compile('^QByteArray$')] = lambda val: QByteArrayPrinter(val)
    pretty_printers_dict[re.compile('^QList<.*>$')] = lambda val: QListPrinter(val, 'QList', None)
    pretty_printers_dict[re.compile('^QStringList$')] = lambda val: QListPrinter(val, 'QStringList', 'QString')
    pretty_printers_dict[re.compile('^QQueue$')] = lambda val: QListPrinter(val, 'QQueue', None)
    pretty_printers_dict[re.compile('^QVector<.*>$')] = lambda val: QListPrinter(val, 'QVector', None)
    pretty_printers_dict[re.compile('^QStack<.*>$')] = lambda val: QListPrinter(val, 'QStack', None)
    pretty_printers_dict[re.compile('^QMap<.*>$')] = lambda val: QMapPrinter(val, 'QMap')
    # pretty_printers_dict[re.compile('^QMap<.*>$')] = lambda val: val['d']['d']
    pretty_printers_dict[re.compile('^QMultiMap<.*>$')] = lambda val: QMapPrinter(val, 'QMultiMap')
    pretty_printers_dict[re.compile('^QHash<.*>$')] = lambda val: QHashPrinter(val, 'QHash',
                                                                               'QHashPrivate::Node<%s, %s>')
    pretty_printers_dict[re.compile('^QHashPrivate::MultiNode<.*>::Chain\\s?\\*?$')] = lambda \
            val: QMultiNodeChainPrinter(val)
    pretty_printers_dict[re.compile('^QMultiHash<.*>$')] = lambda val: QHashPrinter(val, 'QMultiHash',
                                                                                    'QHashPrivate::MultiNode<%s, %s>')
    pretty_printers_dict[re.compile('^QDate$')] = lambda val: QDatePrinter(val)
    pretty_printers_dict[re.compile('^QTime$')] = lambda val: QTimePrinter(val)
    pretty_printers_dict[re.compile('^QDateTime$')] = lambda val: QDateTimePrinter(val)
    pretty_printers_dict[re.compile('^QUrl$')] = lambda val: QUrlPrinter(val)
    pretty_printers_dict[re.compile('^QSet<.*>$')] = lambda val: QSetPrinter(val)
    pretty_printers_dict[re.compile('^QChar$')] = lambda val: QCharPrinter(val)
    pretty_printers_dict[re.compile('^QUuid')] = lambda val: QUuidPrinter(val)
    # pretty_printers_dict[re.compile('^QVariant')] = lambda val: QVariantPrinter(val)


build_dictionary()
register_qt_printers()
